/*8629*/
//don't include this header, only configmanager-revision.cpp should do this.
#ifndef AUTOREVISION_H
#define AUTOREVISION_H


#include <wx/string.h>

namespace autorevision
{
	const unsigned int svn_revision = 1;
	const wxString svnRevision(_T("130314"));      /* emIDE change [JL] */
	const wxString svnDate(_T(__DATE__));    /* emIDE change [JL] */
}



#endif
