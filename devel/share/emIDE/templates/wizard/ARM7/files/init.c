/*********************************************************************
----------------------------------------------------------------------
File    : init.c
Purpose : Sample initialisation for ARM7
--------- END-OF-HEADER ----------------------------------------------
*/

/*********************************************************************
*
*       Local defines
*
**********************************************************************
*/
#define OS_INTERWORK
#define OS_U32 unsigned
/*      Watchdog */
#define _WDT_BASE_ADDR     (0xFFFFFD40)
#define _WDT_CR       (*(volatile OS_U32*) (_WDT_BASE_ADDR + 0x00))
#define _WDT_MR       (*(volatile OS_U32*) (_WDT_BASE_ADDR + 0x04))
#define _WDT_SR       (*(volatile OS_U32*) (_WDT_BASE_ADDR + 0x08))
#define WDT_WDFIEN    (1 << 12) /* Watchdog interrupt enable flag in mode register */
#define WDT_WDERR     (1 <<  1) /* Watchdog error status flag                      */
#define WDT_WDUNF     (1 <<  0) /* Watchdog underflow status flag                  */

/*********************************************************************
*
*       __low_level_init()
*
*       Initialize memory controller, clock generation and pll
*
*       Has to be modified, if another CPU clock frequency should be
*       used. This function is called during startup and
*       has to return 1 to perform segment initialization
*/
#ifdef __cplusplus
extern "C" {
#endif
OS_INTERWORK int __low_level_init(void);  // Avoid "no ptototype" warning
#ifdef __cplusplus
  }
#endif
OS_INTERWORK int __low_level_init(void) {
  _WDT_MR = (1 << 15);                                           /* Initially disable watchdog */
  return 1;
}
