/*********************************************************************
----------------------------------------------------------------------
File    : BSP.h
Purpose : BSP (Board support package)
--------  END-OF-HEADER  ---------------------------------------------
*/

#ifndef BSP_H                           /* avoid multiple inclusion */
#define BSP_H

/*********************************************************************
*
*       Functions
*
**********************************************************************
*/
void     BSP_Init       (void);
void     BSP_SetLED     (int Index);
void     BSP_ClrLED     (int Index);
void     BSP_ToggleLED  (int Index);

#endif                                  /* avoid multiple inclusion */

/*************************** End of file ****************************/

