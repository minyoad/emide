/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: srfdlg.h
**  Purpose:  Headerfile for SFR window
**
********************************************************************/

#ifndef SFRDLG_H
#define SFRDLG_H

#include <wx/panel.h>
#include <cbdebugger_interfaces.h>

class wxListCtrl;

class SfrDlg : public wxPanel, public cbLocalsDlg
{
  public:
    SfrDlg(wxWindow* parent);

    wxWindow* GetWindow() { return this; }
    void EnableWindow(bool enable);

    void UpdateSFRs();
    void UpdateSFR();

  private:


    DECLARE_EVENT_TABLE();

    wxListCtrl *m_pList;
};

#endif // SFRDLG_H
