/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: swodlg.cpp
**  Purpose:
**
********************************************************************/
#if SWO
#include "sdk.h"

#ifndef CB_PRECOMP
    #include <wx/listctrl.h>
    #include <wx/menu.h>
    #include <wx/sizer.h>

    #include "cbplugin.h"
    #include "debuggermanager.h"
#endif

#include "swodlg.h"
#include "debuggermanager.h"

namespace
{
    const int idTxt = wxNewId();
}

BEGIN_EVENT_TABLE(SwoDlg, wxPanel)

END_EVENT_TABLE()

SwoDlg::SwoDlg(wxWindow* parent) : wxPanel(parent) {
  wxBoxSizer* BoxSizer = new wxBoxSizer(wxVERTICAL);
  m_pText = new wxListCtrl(this, idTxt, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL);
  BoxSizer->Add(m_pText, 1, wxEXPAND | wxALL);
  SetAutoLayout(true);
  SetSizer(BoxSizer);

  wxFont font(8, wxMODERN, wxNORMAL, wxNORMAL);
  m_pText->SetFont(font);

  m_pText->InsertColumn(0, _("Variable"), wxLIST_FORMAT_LEFT, 64);
  m_pText->InsertColumn(1, _("Value"), wxLIST_FORMAT_LEFT, 64);
}

void SwoDlg::EnableWindow(bool enable) {

}
#endif
