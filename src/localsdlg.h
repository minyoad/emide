/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: localsdlg.h
**  Purpose:  Headerfile for locals window
**
********************************************************************/

#ifndef LOCALSDLG_H
#define LOCALSDLG_H

#include <wx/panel.h>
#include <cbdebugger_interfaces.h>

class wxListCtrl;
class wxPropertyGrid;
class wxPGProperty;

class LocalsDlg : public wxPanel, public cbLocalsDlg
{
  public:
    LocalsDlg(wxWindow* parent);

    wxWindow* GetWindow() { return this; }
    void EnableWindow(bool enable);

    void AddLocal(const wxString& VarName, const wxString& Value, bool isLocal = true);
    void Begin();
    void End();
    void Clear();

  private:


    DECLARE_EVENT_TABLE();

    wxPropertyGrid* m_pList;
    wxPGProperty* m_pLocals;
    wxPGProperty* m_pArgs;
};

#endif // LOCALSDLG_H
