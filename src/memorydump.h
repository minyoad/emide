/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: memorydump.h
**  Purpose:  Interactive memory dump window in debug view
**
********************************************************************/

#ifndef JLMEMDUMP_H
#define JLMEMDUMP_H

#include <wx/panel.h>
#include <cbdebugger_interfaces.h>

class wxTextCtrl;

class JLMemoryDump : public wxPanel, public cbExamineMemoryDlg
{
    public:
        JLMemoryDump(wxWindow* parent);

        wxWindow* GetWindow() { return this; }

        void Begin();
        void End();
        void Clear();
        void ClearReal();

        wxString GetBaseAddress();
        int GetBytes();
        wxString GetUnitSize();
        void AddError(const wxString& err);
        void AddHexByte(const wxString& addr, const wxString& hexbyte);
        void EnableWindow(bool enable);
        void IncrementAddr();
    protected:
        void OnScrollDown(wxCommandEvent& event);
        void OnScrollUp(wxCommandEvent& event);
        void OnUpdate(wxCommandEvent& event);
        void OnScroll(wxMouseEvent& event);
        void OnScrollText(wxMouseEvent& event);
        void Scroll(int rotation, int lines);
        void OnMouse(wxMouseEvent& event);
        void OnChoice(wxCommandEvent& event);
        void OnGoToAddress(wxCommandEvent& event);
        void GetPanelFocus(wxChildFocusEvent& event);

    private:
        wxTextCtrl* m_pText;
        wxListCtrl* m_pList;
        wxStopWatch m_StopWatch;
        long m_NextScroll; //Time of the next handled scroll
        long m_Bytes;
        long m_ByteStep;
        unsigned long m_TempAddress;
        unsigned long m_FirstAddress;
        unsigned long m_LastAddress;
        int m_Direction;
        int m_errorCount;


        int m_UnitSize;
        wxString m_UnitChar;
        size_t m_ByteCounter;
        wxString m_LineChars; // 16*3 "7F " + 3 "   " + 16 "."
        wxString m_LineBytes;


    private:
        DECLARE_EVENT_TABLE()
};

#endif // JLMEMDUMP_H
