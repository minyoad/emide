/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: swodlg.cpp
**  Purpose:
**
********************************************************************/
#if SWO
#ifndef SWODLG_H
#define SWODLG_H

#include <wx/panel.h>
#include <cbdebugger_interfaces.h>

class wxListCtrl;

class SwoDlg : public wxPanel, public cbSwoDlg
{
  public:
    SwoDlg(wxWindow* parent);

    wxWindow* GetWindow() { return this; }

    void EnableWindow(bool enable);
  protected:

  private:
    DECLARE_EVENT_TABLE();
  private:
    wxListCtrl* m_pText;
};

#endif // SWODLG_H
#endif
