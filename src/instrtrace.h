/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: instrtrace.h
**  Purpose:  Instruction Trace window for debugging
**
********************************************************************/

#ifndef INSTRTRACE_H
#define INSTRTRACE_H

#include <wx/panel.h>
#include <cbdebugger_interfaces.h>

class wxTextCtrl;

class InstrtraceDlg : public wxPanel, public cbInstrtraceDlg
{
    public:
        InstrtraceDlg(wxWindow* parent);

        wxWindow* GetWindow() { return this; }

        void Begin();
        void End();
        void Clear();
        void ClearReal();

        void EnableWindow(bool enable);
    protected:
        void OnScroll(wxMouseEvent& event);

    private:
        wxTextCtrl* m_pText;
        wxStopWatch m_StopWatch;


    private:
        DECLARE_EVENT_TABLE()
};

#endif // INSTRTRACE_H
