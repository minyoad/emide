/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: appglobals.h
**
** WARNING: This file is autogenerated. Do not modify.
**
********************************************************************/

#ifndef APPGLOBALS_H
#define APPGLOBALS_H

#include <wx/string.h>
#include <wx/intl.h>

#define VERSION_NUMBER  22000 // ABBCC A: Major; B: Minor; C: Revision;

#if (((VERSION_NUMBER/100)%100)%2)
  #define SVN_BUILD       1     // Set to 1 for Beta versions (odd Minor versions)
#else
  #define SVN_BUILD       0
#endif

namespace appglobals
{
    typedef struct { int locale_code; wxString name; } Localisation;

    extern const wxString AppVendor;
    extern const wxString AppName;
    extern const wxString AppVersion;
    extern const wxString AppActualVersionVerb;
    extern const wxString AppActualVersion;
    extern const wxString AppSDKVersion;
    extern const wxString AppUrl;
    extern const wxString AppContactEmail;
    extern const wxString AppPlatform;
    extern const wxString AppWXAnsiUnicode;
    extern const wxString AppBuildTimestamp;

    extern const wxString DefaultBatchBuildArgs;
};

#endif // APPGLOBALS_H
