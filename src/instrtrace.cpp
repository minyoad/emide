/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: instrtrace.cpp
**  Purpose:
**
********************************************************************/


/********************************************************************
**      INCLUDES
**/
#include "sdk.h"

#ifndef CB_PRECOMP
#include <wx/button.h>
#include <wx/combobox.h>
#include <wx/intl.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/xrc/xmlres.h>

#include "cbplugin.h"
#endif

#include "instrtrace.h"
#include "debuggermanager.h"

/********************************************************************
**      EVENTS
**/
BEGIN_EVENT_TABLE(InstrtraceDlg, wxPanel)
  EVT_MOUSEWHEEL(InstrtraceDlg::OnScroll)
END_EVENT_TABLE()

/********************************************************************
**      CLASSFUNCTIONS
**/

InstrtraceDlg::InstrtraceDlg(wxWindow* parent) {
  //ctor
  if (!wxXmlResource::Get()->LoadPanel(this, parent, _T("InstrtracePanel")))
    return;
  m_pText = XRCCTRL(*this, "txtBacktrace", wxTextCtrl);


  wxFont font(8, wxMODERN, wxNORMAL, wxNORMAL);
  m_pText->SetFont(font);

  ClearReal();

  m_pText->AppendText(_T("0x00000000: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));

  m_StopWatch.Start();
}

/********************************************************************
**  InstrtraceDlg::Begin()
*/
void InstrtraceDlg::Begin()
{
  m_pText->Freeze();
}

/********************************************************************
**  InstrtraceDlg::End()
*/
void InstrtraceDlg::End()
{
  m_pText->Thaw();
}

/********************************************************************
**  InstrtraceDlg::Clear()
**    May be a dummy to clear the memory window
********************************************************************/
void InstrtraceDlg::Clear()
{
  m_pText->Clear();
}

/********************************************************************
**  InstrtraceDlg::ClearReal()
**    Really clear the memory window
********************************************************************/
void InstrtraceDlg::ClearReal()
{
  m_pText->Clear();

}

/********************************************************************
**  InstrtraceDlg::OnScroll()
**    Handles Mousewheelscrolling in Memorydump window
********************************************************************/
void InstrtraceDlg::OnScroll(wxMouseEvent& event) {

}


/********************************************************************
**  InstrtraceDlg::EnableWindow()
*/
void InstrtraceDlg::EnableWindow(bool enable)
{
  Enable(enable);
}
