/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: localsdlg.cpp
**  Purpose:  Class for locals window
**
********************************************************************/

#include "sdk.h"

#ifndef CB_PRECOMP
  #include <wx/listctrl.h>
  #include <wx/sizer.h>
  #include "cbplugin.h"
#endif

#include "localsdlg.h"
#include "debuggermanager.h"
#include <wx/propgrid/propgrid.h>

namespace {
  const int idList  = wxNewId();
  const long idGrid = wxNewId();
}

BEGIN_EVENT_TABLE(LocalsDlg, wxPanel)

END_EVENT_TABLE()

/********************************************************************
**  LocalsDlg::LocalsDlg()
**    Constructor, creates panel elements (List)
*/
LocalsDlg::LocalsDlg(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize) {
  wxBoxSizer *bs = new wxBoxSizer(wxVERTICAL);
  m_pList = new wxPropertyGrid(this, idGrid, wxDefaultPosition, wxDefaultSize,
                              wxPG_SPLITTER_AUTO_CENTER | wxTAB_TRAVERSAL /*| wxWANTS_CHARS*/);

  m_pList->SetExtraStyle(wxPG_EX_DISABLE_TLP_TRACKING | wxPG_EX_HELP_AS_TOOLTIPS);
  m_pList->SetColumnCount(2);

  // append category
  m_pArgs = m_pList->Append( new wxPropertyCategory(_("Arguments")) );
  m_pLocals = m_pList->Append( new wxPropertyCategory(_("Locals")) );

  bs->Add(m_pList, 1, wxEXPAND | wxALL);
  SetAutoLayout(TRUE);
  SetSizer(bs);

  m_pList->SetColumnProportion(0, 30);
  m_pList->SetColumnProportion(1, 70);

  Layout();
}

/********************************************************************
**  LocalsDlg::AddLocal()
**    Add a new Item to the List
*/
void LocalsDlg::AddLocal(const wxString& VarName, const wxString& Value, bool isLocal) {

  unsigned long int HexValue;
  wxString StrValue;
  wxPGId id;
  //
  // Generate Hex value from Value
  //
  if(Value.ToULong(&HexValue, 10)) {
    StrValue = wxString::Format(wxT("0x%08x"), HexValue);
  } else {
    StrValue = Value;
  }
  //m_pList->Append( new wxStringProperty(VarName, wxPG_LABEL, StrValue) );
  if (isLocal) {
    id = m_pList->AppendIn(m_pLocals, new wxStringProperty(VarName, wxPG_LABEL, StrValue));
    id->SetFlag(wxPG_PROP_READONLY);
  } else {
    id = m_pList->AppendIn(m_pArgs, new wxStringProperty(VarName, wxPG_LABEL, StrValue));
    id->SetFlag(wxPG_PROP_READONLY);
  }
}

/********************************************************************
**  LocalsDlg::Begin()
*/
void LocalsDlg::Begin() {
  m_pList->Freeze();
}

/********************************************************************
**  LocalsDlg::Clear()
*/
void LocalsDlg::Clear() {
  m_pArgs->DeleteChildren();
  m_pLocals->DeleteChildren();
}

/********************************************************************
**  LocalsDlg::End()
*/
void LocalsDlg::End() {

  m_pList->Refresh();
  m_pList->Thaw();

}

/********************************************************************
**  LocalsDlg::EnableWindow()
*/
void LocalsDlg::EnableWindow(bool enable) {
  m_pList->Enable(enable);
}

/* EOF */
