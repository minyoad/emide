/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 *
 * $Revision$
 * $Id$
 * $HeadURL$
 */

#include "sdk.h"

#ifndef CB_PRECOMP
    #ifdef __WXMAC__
        #include <wx/font.h>
    #endif //__WXMAC__
    #include <wx/image.h>    // wxImage
    #include <wx/intl.h>
    #include <wx/stattext.h>
    #include <wx/string.h>
    #include <wx/textctrl.h>
    #include <wx/xrc/xmlres.h>

    #include "licenses.h"
    #include "configmanager.h"
#endif

#include <wx/bitmap.h>
#include <wx/dcmemory.h>    // wxMemoryDC
#include <wx/statbmp.h>

#include "appglobals.h"
#include "dlgabout.h" // class's header file
#include "configmanager.h"

// class constructor

dlgAbout::dlgAbout(wxWindow* parent)
{
    if (!wxXmlResource::Get()->LoadObject(this, parent, _T("dlgAbout"), _T("wxScrollingDialog")))
    {
        cbMessageBox(_("There was an error loading the \"About\" dialog from XRC file."),
                     _("Information"), wxICON_EXCLAMATION);
        return;
    }

    const wxString description = _("Welcome to ") + appglobals::AppName + _T(" ") +
                                 appglobals::AppVersion + _T("!\n") + appglobals::AppName +
                                 _(" is a full-featured IDE (Integrated Development Environment) "
                                   "giving you all you need for embedded programming, "
                                   "from the editor to the compiler and the (remote-)debugger.\n"
                                   "Its structure and functions are designed to be straightforward and used intuitively.\n");

    wxString file = ConfigManager::ReadDataPath() + _T("/images/Logo_white.png");
    wxImage im;
    im.LoadFile(file, wxBITMAP_TYPE_PNG); im.ConvertAlphaToMask();
    wxBitmap bmp(im);
    wxMemoryDC dc;
    dc.SelectObject(bmp);

    {  // keep this (kind of) in sync with splashscreen.cpp!
        static const wxString release(wxString::Format(_("%d.%d%c"), (VERSION_NUMBER/10000), ((VERSION_NUMBER/100)%100), ((VERSION_NUMBER%100) ? ((VERSION_NUMBER%100) + 'a' - 1) : ' ') ));
        static const wxString revision = ConfigManager::GetRevisionString();

        wxFont largeFont(16, wxSWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        wxFont smallFont(9,  wxSWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);

        wxCoord lf_width, lf_heigth, sf_width, sf_heigth, sm_width, sm_height;

        dc.GetTextExtent(release,  &lf_width, &lf_heigth, 0, 0, &largeFont);
        dc.GetTextExtent(revision, &sf_width, &sf_heigth, 0, 0, &smallFont);

        dc.GetTextExtent(_("SAFE MODE"), &sm_width, &sm_height, 0, 0, &largeFont);

        dc.SetTextForeground(*wxBLACK);

        dc.SetFont(largeFont);
    }

    wxStaticBitmap *bmpControl = XRCCTRL(*this, "lblTitle", wxStaticBitmap);
    bmpControl->SetSize(im.GetWidth(),im.GetHeight());
    bmpControl->SetBitmap(bmp);

    XRCCTRL(*this, "lblBuildTimestamp", wxStaticText)->SetLabel(wxString(_("Build: ")) + appglobals::AppBuildTimestamp);
    XRCCTRL(*this, "txtDescription",    wxTextCtrl)->SetValue(description);
    XRCCTRL(*this, "txtThanksTo",       wxTextCtrl)->SetValue(_(
        "Code::Blocks and the whole Code::Blocks team\n"
        "for the great work for the base of emIDE.\n"
        "\n"
        "SEGGER and their developers\n"
        "for the JLink plugin and the help to integrate it."));
    XRCCTRL(*this, "txtLicense", wxTextCtrl)->SetValue(LICENSE_GPL);

    XRCCTRL(*this, "lblName",    wxStaticText)->SetLabel(appglobals::AppName);
    XRCCTRL(*this, "lblVersion", wxStaticText)->SetLabel(appglobals::AppActualVersionVerb);
    XRCCTRL(*this, "lblSDK",     wxStaticText)->SetLabel(appglobals::AppSDKVersion);
    XRCCTRL(*this, "lblAuthor",  wxStaticText)->SetLabel(_("Johannes Lask"));
    XRCCTRL(*this, "lblEmail",   wxStaticText)->SetLabel(appglobals::AppContactEmail);
    XRCCTRL(*this, "lblWebsite", wxStaticText)->SetLabel(appglobals::AppUrl);

#ifdef __WXMAC__
    // Courier 8 point is not readable on Mac OS X, increase font size:
    wxFont font1 = XRCCTRL(*this, "txtThanksTo", wxTextCtrl)->GetFont();
    font1.SetPointSize(10);
    XRCCTRL(*this, "txtThanksTo", wxTextCtrl)->SetFont(font1);

    wxFont font2 = XRCCTRL(*this, "txtLicense", wxTextCtrl)->GetFont();
    font2.SetPointSize(10);
    XRCCTRL(*this, "txtLicense", wxTextCtrl)->SetFont(font2);
#endif
    Fit();
    CentreOnParent();
}

// class destructor
dlgAbout::~dlgAbout()
{
    // insert your code here
}
