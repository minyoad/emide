/********************************************************************
**  This file is part of the emIDE
**
**  File: MMR.cpp
**  Purpose: Memory Mapped Register panel functions
**           Parse emRD (emIDE Register Definition file)
**           Show and update MMR panel
**
********************************************************************/

/********************************************************************
**      INCLUDES
**/
#include <sdk.h> // Code::Blocks SDK

#ifndef CB_PRECOMP
    #include <wx/confbase.h>
    #include <wx/fileconf.h>
    #include <wx/intl.h>
    #include <wx/filename.h>
    #include <wx/msgdlg.h>
    #include <wx/stopwatch.h>
    #include "manager.h"
    #include "configmanager.h"
    #include "projectmanager.h"
    #include "logmanager.h"
    #include "macrosmanager.h"
    #include "cbproject.h"
    #include "compilerfactory.h"
    #include "globals.h"

    #include <wx/app.h>
    #include <wx/dnd.h>
    #include <wx/fontutil.h>
    #include <wx/menu.h>
    #include <wx/settings.h>
    #include <wx/sizer.h>

    #include "cbexception.h"
    #include "cbplugin.h"
    #include "scrollingdialog.h"
#endif

#include <numeric>
#include <map>
#include <wx/propgrid/propgrid.h>
#include <wx/progdlg.h>

#include <wx/arrimpl.cpp>
#include "tinyxml/tinywxuni.h"
#include "MMRdlg.h"

/*********************************************************************
*
*       Globals
*
**********************************************************************
*/
namespace {
  const long idGrid       = wxNewId();
  const long idMenuUpdate = wxNewId();
  const long idMenuExpand = wxNewId();
}

/*********************************************************************
*
*       Statics
*
**********************************************************************
*/
//
//  The format of a register definition is:
//
//  sfr = "name", "zone", address, size, base=radix [, bitRange=range]
//
//  name
//    The name of the register. A dot . in the name is used for separating the register
//    name and the optional bit name.
//  zone
//    The memory zone of the register. The only available zone is Memory.
//  address
//    The address of the register.
//  size
//    The size in bytes of the register. Use 1, 2 or 4.
//  radix
//    The radix used for displaying the numeric value of the register. Use 10 for
//    decimal and 16 for hexadecimal.
//  range
//    An optional bit range definition which can be a single bit number or a range of
//    bits of the form i-j. For example 0-2. As an alternative bitRange can be
//    replaced with bitMask to specify the bit mask for the bit field.
//

//sfr = "BAUD",         "Memory", 0xf0001600, 4, base=16
static wxRegEx regRegister(_T("sfr = \"([A-Za-z0-9_]+)\"[ \t,]+\"Memory\",[ \t,]+(0[xX][0-9A-Fa-f]+)[ \t,]+([124])[ \t,]+base=([1][06])"));
//
// 1: Register Name
// 2: Register Address
// 3: Register Size
// 4: Radix
//

//sfr = "CONTROL.TXEN", "Memory", 0xf0001604, 1, base=16, bitRange=1
//sfr = "CONTROL.CHAR", "Memory", 0xf0001604, 1, base=16, bitRange=2-3
static wxRegEx regField1(_T("sfr = \"([A-Za-z0-9_]+)\\.([A-Za-z0-9_]+)\"[ \t,]+\"Memory\"[ \t,]+(0[xX][0-9A-Fa-f]+)[ \t,]+([124])[ \t,]+base=([1][06])[ \t,]+bitRange=([0-9A-Fa-f]+)-?([0-9A-Fa-f]+)?"));
//
// 1: Register Name
// 2: Field Name
// 3: Register Address
// 4: Register Size
// 5: Radix
// 6: Field Start bit
// 7: Field End bit
//

// sfr = "CONTROL.CHAR", "Memory", 0xf0001604, 1, base=16, bitMask=0x0c
static wxRegEx regField2(_T("sfr = \"([A-Za-z0-9_]+)\\.([A-Z0-9_]+)\"[ \t,]+\"Memory\"[ \t,]+(0[xX][0-9A-Fa-f]+)[ \t,]+([124])[ \t,]+base=([1][06])[ \t,]+bitMask=(0x[0-9A-Fa-f]+)"));
//
// 1: Register Name
// 2: Field Name
// 3: Register Address
// 4: Register Size
// 5: Radix
// 6: Field bit mask
//
/*********************************************************************
*
*       Array implementation
*
**********************************************************************
*/
WX_DEFINE_OBJARRAY(SubRegisterArray);
WX_DEFINE_OBJARRAY(RegisterArray);
WX_DEFINE_OBJARRAY(RegisterGroupArray);
WX_DEFINE_OBJARRAY(MMRProjectsArray);
WX_DEFINE_OBJARRAY(MMRDevicesArray);
/*********************************************************************
*
*       Events
*
**********************************************************************
*/
BEGIN_EVENT_TABLE(MMRDlg, wxPanel)
  EVT_PG_ITEM_EXPANDED(idGrid, MMRDlg::OnExpand)
  EVT_PG_RIGHT_CLICK(idGrid, MMRDlg::OnPropertyRightClick)
  EVT_PG_CHANGED(idGrid, MMRDlg::OnPropertyChanged)
  EVT_PG_CHANGING(idGrid, MMRDlg::OnPropertyChanging)

  EVT_MENU(idMenuUpdate, MMRDlg::OnMenuUpdate)
  EVT_MENU(idMenuExpand,      MMRDlg::OnMenuExpand)
END_EVENT_TABLE()
/*********************************************************************
*
*       Public functions
*
**********************************************************************
*/
/********************************************************************
**  MMRDlg::MMRDlg()
**    Create MMRDlg Window
*/
MMRDlg::MMRDlg(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL | wxNO_BORDER, wxT("PeripheralRegs")),
  m_aRegisterGroup(0) {
  m_UpdateCount = 0;
  wxBoxSizer *bs = new wxBoxSizer(wxVERTICAL);
  m_PropGrid = new wxPropertyGrid(this, idGrid, wxDefaultPosition, wxDefaultSize,
                              wxPG_SPLITTER_AUTO_CENTER | wxTAB_TRAVERSAL /*| wxWANTS_CHARS*/);

  m_PropGrid->SetExtraStyle(wxPG_EX_DISABLE_TLP_TRACKING | wxPG_EX_HELP_AS_TOOLTIPS);
  m_PropGrid->SetColumnCount(2);
  bs->Add(m_PropGrid, 1, wxEXPAND | wxALL);
  SetAutoLayout(TRUE);
  SetSizer(bs);

  m_PropGrid->SetColumnProportion(0, 60);
  m_PropGrid->SetColumnProportion(1, 40);
  Layout();
}

/********************************************************************
**  MMRDlg::EnableWindow()
*/
void MMRDlg::EnableWindow(bool enable) {

//  if (enable && !(IsWindowReallyShown(GetWindow()))) {
//    CodeBlocksDockEvent evt(cbEVT_SHOW_DOCK_WINDOW);
//    evt.pWindow = this;
//    Manager::Get()->ProcessEvent(evt);
//  } else if (!enable && (IsWindowReallyShown(GetWindow()))) {
//    CodeBlocksDockEvent evt(cbEVT_HIDE_DOCK_WINDOW);
//    evt.pWindow = this;
//    Manager::Get()->ProcessEvent(evt);
//  }
m_PropGrid->Enable(enable);
}

static wxColor level1 = wxColor(255, 128, 128);
static wxColor level2 = wxColor(255, 176, 176);
static wxColor level3 = wxColor(255, 224, 224);

/********************************************************************
**  MMRDlg::SetMMRValue()
**    Set the value of an MMR
*/
bool MMRDlg::SetMMRValue(unsigned long Address, unsigned long Value) {
  wxPGProperty  prop;
  wxString      sValue;
  unsigned int  GroupCount;
  unsigned int  RegisterCount;
  unsigned int  FieldCount;
  unsigned int  i;
  unsigned int  j;
  unsigned int  k;
  unsigned long FlagMask;
  unsigned long FlagValue;
  wxColor       Background;
  //
  // Find register with Address in array, update value and get Name
  //
  if (!m_aRegisterGroup) {
    return false;
  }
  GroupCount = m_aRegisterGroup->Count();
  for (i = 0; i < GroupCount; i++) {
    RegisterCount = m_aRegisterGroup->Item(i).aRegister.Count();
    for (j = 0; j < RegisterCount; j++) {
      if (m_aRegisterGroup->Item(i).aRegister[j].Address == Address) {      // Register found
        if (m_aRegisterGroup->Item(i).aRegister[j].Value != Value) {        // Value has changed
          m_aRegisterGroup->Item(i).aRegister[j].Value = Value;
          sValue = wxString::Format(wxT("0x%.8X"),Value);
          m_aRegisterGroup->Item(i).aRegister[j].rID->SetValueFromString(sValue);
          m_PropGrid->SetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].rID, level1, 0);
          if (m_aRegisterGroup->Item(i).aRegister[j].Access != 2) {
            FieldCount = m_aRegisterGroup->Item(i).aRegister[j].aSubRegister.Count();
            for (k = 0; k < FieldCount; k++) {
              FlagMask = m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].BitMask;
              FlagValue = Value;
              while ((FlagMask & 1) == 0) {
                FlagMask = FlagMask >> 1;
                FlagValue = FlagValue >> 1;
              }
              if (m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].Value != (FlagValue & FlagMask)) { // Flag has changed
                m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].Value = FlagValue & FlagMask;
                sValue = wxString::Format(wxT("%X"), (FlagValue & FlagMask));
                m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID->SetValueFromString(sValue);
                m_PropGrid->SetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID, level1, 0);
              } else {
                Background = m_PropGrid->GetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID);
                if (Background == level1) {
                  Background = level2;
                } else if (Background == level2) {
                  Background = level3;
                } else  {
                  Background = wxColor(255, 255, 255);
                }
                m_PropGrid->SetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID, Background, 0);
              }
            }
          }
          return true;
        } else {                                                      // Value has not changed, change background of register and flags
          Background = m_PropGrid->GetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].rID);
          if (Background == level1) {
            Background = level2;
          } else if (Background == level2) {
            Background = level3;
          } else  {
            Background = wxColor(255, 255, 255);
          }
          m_PropGrid->SetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].rID, Background, 0);
          if (m_aRegisterGroup->Item(i).aRegister[j].Access != 2) {
            FieldCount = m_aRegisterGroup->Item(i).aRegister[j].aSubRegister.Count();
            for (k = 0; k < FieldCount; k++) {
              Background = m_PropGrid->GetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID);
              if (Background == level1) {
                Background = level2;
              } else if (Background == level2) {
                Background = level3;
              } else  {
                Background = wxColor(255, 255, 255);
              }
              m_PropGrid->SetPropertyBackgroundColour(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID, Background, 0);
            }
          }
          return true;
        }
      }
    }
  }
  if (i == GroupCount || j == RegisterCount) {
    return false;
  }
  return false;
}
/********************************************************************
**  MMRDlg::ToggleWindow()
**    Show/hide MMR Window
*/
void MMRDlg::ToggleWindow() {
  if(!(IsWindowReallyShown(GetWindow()))) {
    CodeBlocksDockEvent evt(cbEVT_SHOW_DOCK_WINDOW);
    evt.pWindow = this;
    Manager::Get()->ProcessEvent(evt);
  } else {
    CodeBlocksDockEvent evt(cbEVT_HIDE_DOCK_WINDOW);
    evt.pWindow = this;
    Manager::Get()->ProcessEvent(evt);
  }
}
/********************************************************************
**  MMRDlg::InsertMMRs()
**    Insert all Registers to the window
*/
void MMRDlg::InsertMMRs(void) {
  int     NumGroups;
  int     NumRegisters;
  int     NumFlags;
  long    RegValue;
  long    FlagValue;
  long    FlagMask;
  wxPGId  gId, rId, fId;
  int     i, j, k;

  unsigned int ProjectId;
  wxString ProjectTitle;
  wxString BuildTarget;

  cbProject* curProject = Manager::Get()->GetProjectManager()->GetActiveProject();
  if(curProject == 0) {
    return;
  }
  ProjectTitle = curProject->GetTitle();
  BuildTarget = curProject->GetActiveBuildTarget();
  for (ProjectId = 0; ProjectId < m_aProjects.GetCount(); ProjectId++) {
    if (ProjectTitle.CmpNoCase(m_aProjects.Item(ProjectId).ProjectTitle) == 0 && BuildTarget.CmpNoCase(m_aProjects.Item(ProjectId).BuildTarget) == 0) {
      break;
    }
  }
  if (ProjectId >= m_aProjects.GetCount()) {
    return;
  }
  m_aRegisterGroup = &m_aProjects.Item(ProjectId).aRegisterGroups;

  NumGroups = m_aRegisterGroup->GetCount();
  for (i = 0; i < NumGroups; i++) {
    gId = m_PropGrid->Append( new wxPropertyCategory(m_aRegisterGroup->Item(i).sName));
    m_aRegisterGroup->Item(i).gId = gId;
    NumRegisters = m_aRegisterGroup->Item(i).aRegister.GetCount();
    for (j = 0; j < NumRegisters; j++) {
      if (m_aRegisterGroup->Item(i).aRegister[j].Access == 2) {
        rId = m_PropGrid->AppendIn(gId, new wxStringProperty(m_aRegisterGroup->Item(i).aRegister[j].sName, wxPG_LABEL, _("<Click to write register>")));
      } else {
        RegValue = m_aRegisterGroup->Item(i).aRegister[j].Value;
        rId = m_PropGrid->AppendIn(gId, new wxStringProperty(m_aRegisterGroup->Item(i).aRegister[j].sName, wxPG_LABEL, wxString::Format(_("0x%08X"), RegValue)));
        if ((m_aRegisterGroup->Item(i).aRegister[j].Access & 2) == 0) {
          rId->SetFlag(wxPG_PROP_READONLY);
          m_PropGrid->SetPropertyTextColour(rId, wxColor(0x55, 0x55, 0x55), 0);
        }
      }
      rId->SetHelpString(m_aRegisterGroup->Item(i).aRegister[j].sDescription);
      m_aRegisterGroup->Item(i).aRegister[j].rID = rId;
      if (m_aRegisterGroup->Item(i).aRegister[j].Access != 2) {
        NumFlags = m_aRegisterGroup->Item(i).aRegister[j].aSubRegister.GetCount();
        for (k = 0; k < NumFlags; k++) {
          FlagMask = m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].BitMask;
          FlagValue = RegValue;
          while ((FlagMask & 1) == 0) {
            FlagMask = FlagMask >> 1;
            FlagValue = FlagValue >> 1;
          }
          FlagValue &= FlagMask;
          fId = rId->AppendChild(new wxStringProperty(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].sName, wxPG_LABEL, wxString::Format(_("%X"), FlagValue)));
          if ((m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].Access & 2) == 0) {
            fId->SetFlag(wxPG_PROP_READONLY);
            m_PropGrid->SetPropertyTextColour(fId, wxColor(0x55, 0x55, 0x55), 0);
          }
          fId->SetHelpString(m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].sDescription);
          m_aRegisterGroup->Item(i).aRegister[j].aSubRegister[k].fID = fId;
        }
      }
    }
  }
  m_PropGrid->CollapseAll();
  m_UpdateCount = 0;
}
/********************************************************************
**  MMRDlg::ClearMMRs()
**    Clears the window
*/
void MMRDlg::ClearMMRs(void) {
  m_PropGrid->Clear();
}
/********************************************************************
**  MMRDlg::ClearRD()
**    Clears the register definition array
*/
void MMRDlg::ClearRD(void) {
  if (m_aRegisterGroup) {
    m_aRegisterGroup->Clear();
  }
}
/********************************************************************
**  MMRDlg::Start()
**    Freezes the window
*/
void MMRDlg::Start(void) {
  m_PropGrid->Freeze();
  m_UpdateCount++;
}
/********************************************************************
**  MMRDlg::End()
**    Freezes the window
*/
void MMRDlg::End(void) {
  m_PropGrid->Thaw();
}


/********************************************************************
**  MMRDlg::GetUpdateString()
**    Create the GDB command to get the MMR values from target
*/
wxString MMRDlg::GetUpdateString(int i) {
  int           j;
  int           NumRegisters;
  unsigned long Address;
  unsigned long Size;
  wxString      cmd;

  cmd = wxEmptyString;
  if (!m_aRegisterGroup) {
    return wxEmptyString;
  }
  if (i >= (int)m_aRegisterGroup->GetCount()) {
    return cmd;
  }
  m_aRegisterGroup->Item(i).UpdateCount = m_UpdateCount;
  cmd = _T("if 1\n");
  NumRegisters = m_aRegisterGroup->Item(i).aRegister.GetCount();
  for (j = 0; j < NumRegisters; j++) {
    if (m_aRegisterGroup->Item(i).aRegister[j].rID && (m_aRegisterGroup->Item(i).aRegister[j].Access != 2)) {
      //
      // Add command to get register value
      //
      Address = m_aRegisterGroup->Item(i).aRegister[j].Address;
      Size = m_aRegisterGroup->Item(i).aRegister[j].Size;
      cmd.Append(wxString::Format(_T("monitor memU%d 0x%X\n"), Size, Address));
    }
  }
  cmd.Append(_T("end"));
  return cmd;
}

/********************************************************************
**  MMRDlg::GetUpdateStringRegister()
**    Create the GDB command to get the MMR value for one MMR
*/
wxString  MMRDlg::GetUpdateStringRegister(int g, int r) {
  unsigned long Address;
  unsigned long Size;
  wxString      cmd;

  cmd = wxEmptyString;
  if (!m_aRegisterGroup) {
    return cmd;
  }
  if (g >= (int)m_aRegisterGroup->GetCount()) {
    return cmd;
  }
  if (r >= (int)m_aRegisterGroup->Item(g).aRegister.GetCount()) {
    return cmd;
  }
  //
  // Add command to get register value
  //
  if (m_aRegisterGroup->Item(g).aRegister[r].Access != 2) {
    Address = m_aRegisterGroup->Item(g).aRegister[r].Address;
    Size = m_aRegisterGroup->Item(g).aRegister[r].Size;
    cmd.Append(wxString::Format(_T("monitor memU%d 0x%X"), Size, Address));
  }
  return cmd;
}

/********************************************************************
**  MMRDlg::GetExpandedGroup()
**
*/
int MMRDlg::GetExpandedGroup(int PrevGroup) {
  int NumGroups;
  int i;
  if (m_aRegisterGroup) {
    NumGroups = m_aRegisterGroup->GetCount();
    for (i = PrevGroup+1; i < NumGroups; i++) {
      if (m_aRegisterGroup->Item(i).gId->IsExpanded()) {
        return i;
      }
    }
  }
  return -1;
}

/********************************************************************
**  MMRDlg::InsertRD()
**
*/
int MMRDlg::InsertRD(const wxString& ProjectTitle, const wxString& BuildTarget, const wxString& FileName, bool SkipKnown) {
  unsigned int ProjectId;
  //
  // Search for register definition of project+build target
  //
  for (ProjectId = 0; ProjectId < m_aProjects.GetCount(); ProjectId++) {
    if (ProjectTitle.CmpNoCase(m_aProjects.Item(ProjectId).ProjectTitle) == 0 && BuildTarget.CmpNoCase(m_aProjects.Item(ProjectId).BuildTarget) == 0) {
      if(SkipKnown) {
        return 0;
      } else {
        break;
      }
    }
  }
  //
  // Register definition found. Has the file changed?
  //
  if (ProjectId != m_aProjects.GetCount()) {
    if (FileName.CmpNoCase(m_aProjects.Item(ProjectId).FileName.GetFullPath()) == 0) {
      return 0; //File already parsed and inserted
    }
  }
  //
  // Register definition not found or changed file.
  // Insert
  //
  clProjects Project;
  Project.ProjectTitle = ProjectTitle;
  Project.FileName.Assign(FileName);
  Project.BuildTarget = BuildTarget;
  m_aProjects.Add(Project);
  m_aRegisterGroup = &m_aProjects.Last().aRegisterGroups;
  return 1;
}

/********************************************************************
**  MMRDlg::InsertDeviceRD()
**
*/
int MMRDlg::InsertDeviceRD(const wxString& ProjectTitle, const wxString& BuildTarget, const wxString& DeviceName, bool SkipKnown) {
  unsigned int ProjectId;
  unsigned int DeviceId;
  wxString SVDFileName;
  //
  // Is it already inserted?
  // If yes
  //
  for (ProjectId = 0; ProjectId < m_aProjects.GetCount(); ProjectId++) {
    if (ProjectTitle.CmpNoCase(m_aProjects.Item(ProjectId).ProjectTitle) == 0 && BuildTarget.CmpNoCase(m_aProjects.Item(ProjectId).BuildTarget) == 0) {
      if(SkipKnown) {
        return 0;
      } else {
        break;
      }
    }
  }
  //
  // Update device list if neccesary
  //
  if (m_aDevices.GetCount() == 0) {
    ParseDevices();
    if (m_aDevices.GetCount() == 0) {
      LogManager* pMsg = Manager::Get()->GetLogManager();
      pMsg->DebugLog(_T("No SVD devices found!"));
      return 0;     // No devices found
    }
  }
  //
  // Search for device SVD file
  //
  for (DeviceId = 0; DeviceId < m_aDevices.GetCount(); DeviceId++) {
    if (DeviceName.CmpNoCase(m_aDevices.Item(DeviceId).DeviceName) == 0) {
      SVDFileName = m_aDevices.Item(DeviceId).SVDFile.GetFullPath();
      break;
    }
  }
  if (m_aDevices.GetCount() == DeviceId) {
    return 0;   //Device file not found
  }
  if (ProjectId != m_aProjects.GetCount()) {
    if (SVDFileName.CmpNoCase(m_aProjects.Item(ProjectId).FileName.GetFullPath()) == 0) {
      return 0; //File already parsed and inserted
    }
  }

  clProjects Project;
  Project.ProjectTitle = ProjectTitle;
  Project.FileName.Assign(SVDFileName);
  Project.BuildTarget = BuildTarget;
  m_aProjects.Add(Project);
  m_aRegisterGroup = &m_aProjects.Last().aRegisterGroups;
  return 1;
}

/********************************************************************
**  MMRDlg::OpenRD()
**    Opens and parses the Register definition file
*/
bool MMRDlg::OpenRD(void) {
  wxFileName    rdFile;
  wxString      rdExt;

  LogManager* pMsg = Manager::Get()->GetLogManager();
  pMsg->DebugLog(_T("Loading and parsing register definitions."));
  pMsg->Log(_T("Loading and parsing peripheral register definitions."));
  //
  // Load the definition file
  //
//  rdFile.Assign(filename);
//  if (!rdFile.IsAbsolute()) {
//    rdFile.MakeAbsolute(basePath);
//  }


  rdFile = m_aProjects.Last().FileName;

  if (!rdFile.IsFileReadable()) {
    pMsg->LogError(_T("Failed to load peripheral registers file. File does not exist or is not readable."));
    return false;
  }
  rdExt = rdFile.GetExt();
  if(rdExt.CmpNoCase(_("svd")) == 0 || rdExt.CmpNoCase(_("xml")) == 0) {
    return OpenSVD(rdFile);
  } else if (rdExt.CmpNoCase(_("ddf")) == 0) {
    return OpenDDF(rdFile);
  } else {
    pMsg->DebugLog(_T("Failed to load peripheral registers file. Wrong file-extension."));
    pMsg->LogError(_T("Failed to load peripheral registers file. Wrong file-extension."));
    return false;
  }


}
/*********************************************************************
*
*       Protected functions
*
**********************************************************************
*/

/********************************************************************
**  MMRDlg::OpenSVD()
**
*/
bool MMRDlg::OpenSVD(wxFileName FileName) {
  TiXmlDocument Doc;
  TiXmlElement* Root;
  TiXmlElement* Peripherals;
  TiXmlElement* Peripheral;
  TiXmlElement* Registers;
  TiXmlElement* Register;
  TiXmlElement* Fields;
  TiXmlElement* Field;

  unsigned long FlagBitMask;
  unsigned long FlagValue;
  unsigned int  Skip;
  unsigned int  FieldCount;
  unsigned int  FieldSkip;
  unsigned int  RegisterCount;
  unsigned int  RegisterSkip;
  int           BitOffset;
  unsigned long Offset;
  unsigned long ResetMask;

  unsigned int  stdAccess;
  unsigned long stdResetValue;
  unsigned long stdResetMask;
  unsigned long stdSize;

  int           i;
  int           Count;

  wxString sRegName;
  wxString sSubRegName;
  wxString sDerivedFrom;
  wxString ValueText;
  wxString Access;
  wxString FilePath;

  wxStopWatch StopWatch;

  StopWatch.Start(0);

  FieldCount = 0;
  FieldSkip = 0;
  RegisterCount = 0;
  RegisterSkip = 0;
  Skip = 0;

  LogManager* pMsg = Manager::Get()->GetLogManager();
  FilePath = FileName.GetFullPath();
  if (!TinyXML::LoadDocument(FilePath, &Doc)) {
    pMsg->DebugLog(_T("  Failed to load file ")+FilePath);
    pMsg->LogError(_T("  Failed to load file ")+FilePath);
    return false;
  } else {
    pMsg->DebugLog(_T("  File ") + FilePath + _T(" loaded successfully."));
    pMsg->Log(_T("  File ") + FilePath + _T(" loaded successfully."));
  }
  //
  // Check if it is a register definition file
  //
  Root = Doc.FirstChildElement("device");
  if (!Root) {
    pMsg->DebugLog(_T("  This is not a valid SVD register definition file \n(no 'Register_definition_file') found in file."));
    pMsg->Log(_T("  This is not a valid SVD register definition file."));
    return false;
  }
  //
  // Set Standard values
  //
  if (Root->FirstChildElement("size")) {
    ValueText = wxString::FromUTF8(Root->FirstChildElement("size")->GetText());
    ValueText.ToULong(&stdSize, 0);
  } else {
    stdSize = 0;
  }
  if (Root->FirstChildElement("access")) {
    Access = wxString::FromUTF8(Root->FirstChildElement("access")->GetText());
    if (Access.Cmp(_("read-write")) == 0) {
      stdAccess = 3;
    } else if (Access.Cmp(_("read-only")) == 0) {
      stdAccess = 1;
    } else if (Access.Cmp(_("write-only")) == 0) {
      stdAccess = 2;
    } else {
      stdAccess = 1;
    }
  } else {
    stdAccess = 1;
  }
  if (Root->FirstChildElement("resetValue")) {
    ValueText = wxString::FromUTF8(Root->FirstChildElement("resetValue")->GetText());
    ValueText.ToULong(&stdResetValue, 16);
  } else {
    stdResetValue = 0;
  }
  if (Root->FirstChildElement("resetMask")) {
    ValueText = wxString::FromUTF8(Root->FirstChildElement("resetMask")->GetText());
    ValueText.ToULong(&stdResetMask, 16);
  } else {
    stdResetMask = 0xFFFFFFFF;
  }

  Peripherals = Root->FirstChildElement("peripherals");
  if (!Peripherals) {
      pMsg->DebugLog(_T("  No 'peripherals' element in file...Abort"));
      pMsg->DebugLog(_T("Failed to parse register definitions."));
      pMsg->Log(_T("Failed to parse register definitions."));
      return false;
  }

  Peripheral = Peripherals->FirstChildElement("peripheral");
  if (!Peripheral) {
      pMsg->DebugLog(_T("  No 'peripheral' element in file...Abort"));
      pMsg->DebugLog(_T("Failed to parse register definitions."));
      pMsg->Log(_T("Failed to parse register definitions."));
      return false;
  }
  //
  // Parse every peripheral (register group)
  //
  Peripheral = Peripherals->FirstChildElement("peripheral");
  while (Peripheral) {
    if(!Peripheral->FirstChildElement("name")->GetText()) {
      pMsg->DebugLog(_T("  No groupname name found for a group. Skipping."));
      Peripheral = Peripheral->NextSiblingElement("peripheral");                 // Get next register group
      Skip++;
      continue;
    }
    clRegisterGroup RegisterGroupElement;
    //
    // Check if peripheral is derived from another one
    //
    if (Peripheral->Attribute("derivedFrom")) {
      sDerivedFrom = cbC2U(Peripheral->Attribute("derivedFrom"));
      Count = m_aRegisterGroup->Count();
      for (i = 0; i < Count; i++) {
        if (m_aRegisterGroup->Item(i).sName == sDerivedFrom) {
          break;
        }
      }
      if (i >= Count) {
        pMsg->DebugLog(_T("  Did not find derivative for peripheral. Skipping."));
        Peripheral = Peripheral->NextSiblingElement("peripheral");                 // Get next register group
        Skip++;
        continue;
      }
      RegisterGroupElement.sName = cbC2U(Peripheral->FirstChildElement("name")->GetText());
      if (Peripheral->FirstChildElement("description")) {
        RegisterGroupElement.sDescription = cbC2U(Peripheral->FirstChildElement("description")->GetText());
        RegisterGroupElement.sDescription << _T("\n");
      } else {
        RegisterGroupElement.sDescription = wxEmptyString;
      }

      ValueText = wxString::FromUTF8(Peripheral->FirstChildElement("baseAddress")->GetText());
      RegisterGroupElement.sDescription << _T("Base address: ");
      RegisterGroupElement.sDescription << ValueText;
      ValueText.ToULong(&RegisterGroupElement.BaseAddress, 16);
      Offset = RegisterGroupElement.BaseAddress - m_aRegisterGroup->Item(i).BaseAddress;
      RegisterGroupElement.aRegister = m_aRegisterGroup->Item(i).aRegister;
      Count = RegisterGroupElement.aRegister.Count();
      for (i = 0; i < Count; i++) {
        RegisterGroupElement.aRegister[i].Address += Offset;
      }

    } else {
      Registers = Peripheral->FirstChildElement("registers");
      if(!Registers) {
        pMsg->DebugLog(_T("  Peripheral does not have any registers. Skipping."));
        Peripheral = Peripheral->NextSiblingElement("peripheral");                 // Get next register group
        Skip++;
        continue;
      }
      RegisterGroupElement.sName = cbC2U(Peripheral->FirstChildElement("name")->GetText());
      if (Peripheral->FirstChildElement("description")) {
        RegisterGroupElement.sDescription = cbC2U(Peripheral->FirstChildElement("description")->GetText());
        RegisterGroupElement.sDescription << _T("\n");
      } else {
        RegisterGroupElement.sDescription = wxEmptyString;
      }
      ValueText = wxString::FromUTF8(Peripheral->FirstChildElement("baseAddress")->GetText());
      RegisterGroupElement.sDescription << _T("Base address: ");
      RegisterGroupElement.sDescription << ValueText;
      ValueText.ToULong(&(RegisterGroupElement.BaseAddress), 16);
      //
      // Parse registers of the group
      //
      Register = Registers->FirstChildElement("register");
      while (Register) {
        //
        // Check for Name, Address, Size and Value of the register
        //
        if (!Register->FirstChildElement("name")) {
          pMsg->DebugLog(_T("  No register name found for a register. Skipping."));
          Register = Register->NextSiblingElement("register");                    // Get next register
          RegisterSkip++;
          continue;
        }
        if(!(Register->FirstChildElement("addressOffset")) || (!(Register->FirstChildElement("size")) && stdSize == 0)) {
          pMsg->DebugLog(_T("  Register options are missing for register ") + RegisterGroupElement.sName + _T(".") + cbC2U(Register->FirstChildElement("name")->GetText()) + _T(". Skipping."));
          Register = Register->NextSiblingElement("register");                    // Get next register
          RegisterSkip++;
          continue;
        }
        //
        // Fill Register element with infos
        //
        clRegister RegisterElement;
        if (Register->FirstChildElement("displayName")) {
          RegisterElement.sName = cbC2U(Register->FirstChildElement("displayName")->GetText());
        } else {
          RegisterElement.sName = cbC2U(Register->FirstChildElement("name")->GetText());
        }
        if (Register->FirstChildElement("description")) {
          RegisterElement.sDescription = cbC2U(Register->FirstChildElement("description")->GetText());
          RegisterElement.sDescription << _T("\n");
        } else {
          RegisterElement.sDescription = wxEmptyString;
        }
        if (Register->FirstChildElement("access")) {
          Access = wxString::FromUTF8(Register->FirstChildElement("access")->GetText());
          if (Access.Cmp(_("read-write")) == 0) {
            RegisterElement.Access = 3;
          } else if (Access.Cmp(_("read-only")) == 0) {
            RegisterElement.Access = 1;
          } else if (Access.Cmp(_("write-only")) == 0) {
#if 0
            //
            // Skip if register is write-only, because we cannot display them anyway.
            //
            pMsg->DebugLog(_T("  Write-only registers are currently not supported (") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T("). Skipping."));
            Register = Register->NextSiblingElement("register");                    // Get next register
            RegisterSkip++;
            continue;
#else
            RegisterElement.Access = 2;
#endif
          } else {
            pMsg->DebugLog(_T("  Access options are incorrect for register ") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(". Skipping."));
            Register = Register->NextSiblingElement("register");                    // Get next register
            RegisterSkip++;
            continue;
          }
#if 0
        } else if ((stdAccess & 1) != 1) {
          pMsg->DebugLog(_T("  Write-only registers are currently not supported (") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T("). Skipping."));
          Register = Register->NextSiblingElement("register");                    // Get next register
          RegisterSkip++;
          continue;
#endif
        } else {
          RegisterElement.Access = stdAccess;
        }
        cbC2U(Register->FirstChildElement("addressOffset")->GetText()).ToULong(&RegisterElement.Address, 16);
        RegisterElement.Address += RegisterGroupElement.BaseAddress;
        RegisterElement.sDescription << wxString::Format(_T("Address: 0x%.8X"), RegisterElement.Address);
        if (Register->FirstChildElement("size")) {
          ValueText = wxString::FromUTF8(Register->FirstChildElement("size")->GetText());
          ValueText.ToULong(&RegisterElement.Size, 0);
        } else {
          RegisterElement.Size = stdSize;
        }
        if(Register->FirstChildElement("resetValue")) {
          ValueText = wxString::FromUTF8(Register->FirstChildElement("resetValue")->GetText());
          ValueText.ToULong(&RegisterElement.Value, 16);
        } else {
          RegisterElement.Value = stdResetValue;
        }
        if (Register->FirstChildElement("resetMask")) {
          ValueText = wxString::FromUTF8(Register->FirstChildElement("resetMask")->GetText());
          ValueText.ToULong(&ResetMask, 16);
          RegisterElement.Value &= ResetMask;
        } else if (stdResetMask != 0xFFFFFFFF) {
          RegisterElement.Value &= stdResetMask;
        }
        //
        // Parse subregisters of the register
        //
        Fields = Register->FirstChildElement("fields");
        if(Fields) {
          Field = Fields->FirstChildElement("field");
          while(Field) {
            if (!Field->FirstChildElement("name")) {
              pMsg->DebugLog(_T("  No field name found for a field. Skipping."));
              Field = Fields->NextSiblingElement("field");                    // Get next subregister
              FieldSkip++;
              continue;
            }
            clSubRegister SubRegisterElement;
            //
            // Get options for the subregister
            //
            SubRegisterElement.sName = cbC2U(Field->FirstChildElement("name")->GetText());
            if (Field->FirstChildElement("description")) {
              SubRegisterElement.sDescription = cbC2U(Field->FirstChildElement("description")->GetText());
              SubRegisterElement.sDescription << _T("\n");
            } else {
              SubRegisterElement.sDescription = wxEmptyString;
            }

            if (Field->FirstChildElement("access")) {
              Access = wxString::FromUTF8(Field->FirstChildElement("access")->GetText());
              if (Access.Cmp(_("read-write")) == 0) {
                SubRegisterElement.Access = 3;
                SubRegisterElement.sDescription << _T("Access: Read/Write\n");
              } else if (Access.Cmp(_("read-only")) == 0) {
                SubRegisterElement.Access = 1;
                SubRegisterElement.sDescription << _T("Access: Read\n");
              } else if (Access.Cmp(_("write-only")) == 0) {
                SubRegisterElement.Access = 2;
                SubRegisterElement.sDescription << _T("Access: Write\n");
#if 0
                //
                // Skip if field is write-only, because we cannot display them anyway.
                //
                pMsg->DebugLog(_T("  Write-only fields are currently not supported (") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T("). Skipping."));
                Field = Fields->NextSiblingElement("field");                    // Get next subregister
                FieldSkip++;
                continue;
#endif
              } else {
                pMsg->DebugLog(_T("  Access options are incorrect for field ") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T(". Skipping."));
                Field = Fields->NextSiblingElement("field");                    // Get next subregister
                FieldSkip++;
                continue;
              }
#if 0
            } else if ((stdAccess & 1) != 1) {
              pMsg->DebugLog(_T("  Write-only fields are currently not supported (") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T("). Skipping."));
              Field = Fields->NextSiblingElement("field");                    // Get next subregister
              FieldSkip++;
              continue;
#endif
            } else {
              SubRegisterElement.Access = stdAccess;
            }

            if ((Field->FirstChildElement("lsb")) && (Field->FirstChildElement("msb"))) {
              FlagBitMask = 0;
              for (i = atoi(Field->FirstChildElement("lsb")->GetText()); i < atoi(Field->FirstChildElement("lsb")->GetText())+1; i++) {
                FlagBitMask += 1 << i;
              }
              if (FlagBitMask == 0) {
                pMsg->DebugLog(_T("  LSB and MSB are not correct for field ") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T(". Skipping."));
                Field = Fields->NextSiblingElement("field");                    // Get next subregister
                FieldSkip++;
                continue;
              }
              SubRegisterElement.BitMask = FlagBitMask;
              SubRegisterElement.sDescription << wxString::Format(_T("Mask: 0x%X"), FlagBitMask);
            } else if ((Field->FirstChildElement("bitOffset")) && (Field->FirstChildElement("bitWidth"))) {
              BitOffset = atoi(Field->FirstChildElement("bitOffset")->GetText());
              FlagBitMask = 0;

              for (i = 0; i < atoi(Field->FirstChildElement("bitWidth")->GetText()); i++) {
                FlagBitMask += 1 << (BitOffset + i);
              }
              if (FlagBitMask == 0) {
                pMsg->DebugLog(_T("  bitOffset and bitWidth are not correct for field ") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T(". Skipping."));
                Field = Fields->NextSiblingElement("field");                    // Get next subregister
                FieldSkip++;
                continue;
              }
              SubRegisterElement.BitMask = FlagBitMask;
              SubRegisterElement.sDescription << wxString::Format(_T("Mask: 0x%X"), FlagBitMask);
            } else {
              pMsg->DebugLog(_T("  Field options are missing for a field ") + RegisterGroupElement.sName + _T(".") + RegisterElement.sName + _T(".") + SubRegisterElement.sName + _T(". Skipping."));
              Field = Fields->NextSiblingElement("field");                    // Get next subregister
              FieldSkip++;
              continue;
            }

            FlagValue = RegisterElement.Value;
            while ((FlagBitMask & 1) == 0) {
              FlagBitMask = FlagBitMask >> 1;
              FlagValue = FlagValue >> 1;
            }
            FlagValue &= FlagBitMask;

            SubRegisterElement.Value =  FlagValue;
            //
            // Add subregister to SubRegisterArray
            //
            RegisterElement.aSubRegister.Add(SubRegisterElement);
            FieldCount++;
            Field = Field->NextSiblingElement("field");                    // Get next subregister
          }
        }
        //
        // Add register to RegisterArray
        //
        RegisterGroupElement.aRegister.Add(RegisterElement);
        RegisterCount++;
        Register = Register->NextSiblingElement("register");                    // Get next register
      }
    }
    //
    // Add register group to RegisterGroupArray
    //
    m_aRegisterGroup->Add(RegisterGroupElement);
    //
    // Get next register group
    //
    Peripheral = Peripheral->NextSiblingElement("peripheral");
  }

  Count = m_aRegisterGroup->Count();
  StopWatch.Pause();

  pMsg->DebugLog(wxString::Format(_T("Register definitions succesfully parsed. %d fields, in %d registers, in %d register groups added. (%d/%d/%d skipped.)"), FieldCount, RegisterCount, Count, FieldSkip, RegisterSkip, Skip));
  pMsg->Log(wxString::Format(_T("Register definitions succesfully parsed. %d fields, in %d registers, in %d register groups added. (%d/%d/%d skipped.)"), FieldCount, RegisterCount, Count, FieldSkip, RegisterSkip, Skip));
  pMsg->DebugLog(wxString::Format(_("Register definitions successfully paresd in %dms."), StopWatch.Time()));
  return true;
}

/********************************************************************
**  MMRDlg::OpenDDF()
**
*/
bool MMRDlg::OpenDDF(wxFileName FileName) {
  unsigned long FlagBitMask;
  unsigned int  GroupSkip;
  unsigned int  FieldCount;
  unsigned int  FieldSkip;
  unsigned int  RegisterCount;
  unsigned int  RegisterSkip;
  unsigned int  GroupCount;

  wxString      sRegName;
  wxString      sSubRegName;
  wxString      sDerivedFrom;
  wxString      ValueText;
  wxString      Access;

  int           CurLine;
  unsigned int  j, k;
  int           Count;
  int           CurrentSection;   // 1: SFRs; 2: SFRGroups; else: undefined
  int           ProgCounter;
  int           ProgMaxCount;

  wxString      FilePath;
  wxString      FileContent;
  wxFile        DDFFile;
  wxArrayString DDFArray;
  wxArrayString GroupArray;
  wxString      sIncludeFileName;
  wxFileName    IncludeFileName;
  wxArrayString IncludeArray;
  wxString      CurrentSfr;

  SubRegisterArray  UnsortedSubRegisters;
  RegisterArray     UnsortedRegisters;

  FieldCount = 0;
  FieldSkip = 0;
  RegisterCount = 0;
  RegisterSkip = 0;
  GroupSkip = 0;
  GroupCount = 0;

  LogManager* pMsg = Manager::Get()->GetLogManager();
  FilePath = FileName.GetFullPath();

  if(!(DDFFile.Open(FilePath))) {
    pMsg->DebugLog(_T("  Failed to load file ")+FilePath);
    pMsg->LogError(_T("  Failed to load file ")+FilePath);
    return false;
  } else {
    pMsg->DebugLog(_T("  File ") + FilePath + _T(" loaded successfully."));
    pMsg->DebugLog(wxString::Format(_("  File size is: %d Byte"), DDFFile.Length()));
    pMsg->Log(_T("  File ") + FilePath + _T(" loaded successfully."));
  }

  ProgMaxCount = 1000000;
  wxProgressDialog* progress = new wxProgressDialog(_("Peripheral Registers"),
                                                  _("Reading register definition file..."),
                                                  ProgMaxCount,
                                                  Manager::Get()->GetAppWindow(),
                                                  wxPD_AUTO_HIDE | wxPD_APP_MODAL);
  progress->Pulse();
  FileContent = cbReadFileContents(DDFFile);
  ProgCounter = ProgMaxCount/10;
  progress->Update(ProgCounter, _("Parsing register definition file..."));
  pMsg->DebugLog(wxString::Format(_("  File length is: %d chars"), FileContent.Len()));

  DDFArray = GetArrayFromString(FileContent, _("\n"));
  ProgCounter += ProgMaxCount/10;
  progress->Update(ProgCounter, _("Parsing register definition file..."));

  Count = DDFArray.GetCount();
  CurrentSection = 0;
  //
  // Search for register definition or include file
  //
  for (CurLine = 0; CurLine < Count; CurLine++) {
    ProgCounter = ProgMaxCount/Count*CurLine;
    progress->Update(ProgCounter);

    if (CurrentSection == 0) {  // We are in no section
      if (DDFArray.Item(CurLine).CmpNoCase(_("[SfrInclude]")) == 0) {
        //
        // Include file found
        // Open it, read it, insert it into DDFArray
        //
        CurLine++;
        if(DDFArray.Item(CurLine).StartsWith(_("File = "), &sIncludeFileName)) {
          pMsg->DebugLog(_("  Found [SfrInclude]. File: ") + sIncludeFileName);
          pMsg->Log(_("  Found [SfrInclude]. File: ") + sIncludeFileName);
          pMsg->LogWarning(_("SfrInclude is currently not supported."));
        } else {
          pMsg->DebugLog(_("  Found [SfrInclude]. File is missing in next line."));
          pMsg->Log(_("  Found [SfrInclude]. File is missing in next line."));
          continue;
        }
      } else if (DDFArray.Item(CurLine).CmpNoCase(_("[Sfr]")) == 0) {
        //
        // Sfr definition starts
        //
        pMsg->DebugLog(wxString::Format(_("  Line %d started sfr section."), CurLine+1));
        CurrentSection = 1;
        continue;
      } else if (DDFArray.Item(CurLine).CmpNoCase(_("[SfrGroupInfo]")) == 0) {
        //
        // Sfr group definition starts
        //
        pMsg->DebugLog(wxString::Format(_("  Line %d started sfr group section."), CurLine+1));
        CurrentSection = 2;
        continue;
      }
    } else if (CurrentSection == 1) { // We are in SFR section
      //
      //sfr = "SYSTICKCSR"                          , "Memory", 0xE000E010,        4, base=16
      //sfr = "SYSTICKCSR.ENABLE"                   , "Memory", 0xE000E010,        4, base=16,    bitRange=0-0
      //sfr = "SYSTICKCSR.TICKINT"                  , "Memory", 0xE000E010,        4, base=16,    bitRange=1-1
      //sfr = "SYSTICKCSR.CLKSOURCE"                , "Memory", 0xE000E010,        4, base=16,    bitRange=2-2
      //sfr = "SYSTICKCSR.COUNTFLAG"                , "Memory", 0xE000E010,        4, base=16,    bitRange=16-16
      //; alternate definition:
      //; sfr = "CONTROL.CHAR", "Memory", 0xf0001604, 1, base=16, bitMask=0x0c
      //
      do {
        CurLine++;
        if (CurLine >= Count) {
          pMsg->DebugLog(wxString::Format(_("  Line %d ended sfr section: eof"), Count));
          break;
        }
        ProgCounter = ProgMaxCount/Count*CurLine;
        progress->Update(ProgCounter, _("Parsing... register section..."));
        if (DDFArray.Item(CurLine).StartsWith(_(";")) || DDFArray.Item(CurLine).IsEmpty()) {
          continue;
        } else if (DDFArray.Item(CurLine).StartsWith(_("sfr = "), &CurrentSfr)) {
          //
          // Parse the current sfr
          //
          if (regRegister.Matches(DDFArray.Item(CurLine))) {
            //
            // Register
            //
            clRegister RegisterElement;

            RegisterCount++;

            RegisterElement.sName = regRegister.GetMatch(DDFArray.Item(CurLine), 1);
            regRegister.GetMatch(DDFArray.Item(CurLine), 2).ToULong(&RegisterElement.Address, 16);
            regRegister.GetMatch(DDFArray.Item(CurLine), 3).ToULong(&RegisterElement.Size, 16);
            RegisterElement.Size *= 8;
            RegisterElement.Base = wxAtoi(regRegister.GetMatch(DDFArray.Item(CurLine), 4));

            RegisterElement.sDescription << wxString::Format(_T("Address: 0x%.8X"), RegisterElement.Address);
            //
            // Standard values, not supported by ddf
            //
            RegisterElement.Access = 3;
            RegisterElement.Value = 0;

            UnsortedRegisters.Add(RegisterElement);
          } else if (regField1.Matches(DDFArray.Item(CurLine))) {
            //
            // Subregister with bitRange=X-Y
            //
            unsigned long FieldStartBit;
            unsigned long FieldEndBit;
            wxString      RegisterName;

            clSubRegister SubRegisterElement;

            FieldCount++;

            RegisterName = regField1.GetMatch(DDFArray.Item(CurLine), 1);
            SubRegisterElement.sName = regField1.GetMatch(DDFArray.Item(CurLine), 2);
            SubRegisterElement.Base = wxAtoi(regField1.GetMatch(DDFArray.Item(CurLine), 5));
            regField1.GetMatch(DDFArray.Item(CurLine), 6).ToULong(&FieldStartBit, 10);
            regField1.GetMatch(DDFArray.Item(CurLine), 7).ToULong(&FieldEndBit, 10);
            FlagBitMask = 0;
            do {
              FlagBitMask += 1 << FieldStartBit;
              FieldStartBit++;
            } while (FieldStartBit <= FieldEndBit);
            SubRegisterElement.BitMask = FlagBitMask;

            SubRegisterElement.sDescription << wxString::Format(_T("Address: ") + regField1.GetMatch(DDFArray.Item(CurLine), 3));
            SubRegisterElement.sDescription << wxString::Format(_T("\nMask: 0x%X"), FlagBitMask);
            //
            // Standard values
            //
            SubRegisterElement.Access = 3;
            SubRegisterElement.Value = 0;

            //
            // Insert into matching parent register
            //
            for (j = 0; j < UnsortedRegisters.GetCount(); j++) {
              if (RegisterName.CmpNoCase(UnsortedRegisters.Item(j).sName) == 0) {
                UnsortedRegisters.Item(j).aSubRegister.Add(SubRegisterElement);
                break;
              }
            }
            if (j == UnsortedRegisters.GetCount()) {
              UnsortedSubRegisters.Add(SubRegisterElement);
            }
          } else if (regField2.Matches(DDFArray.Item(CurLine))) {
            FieldCount++;
          } else {
            RegisterSkip++;
            pMsg->DebugLog(wxString::Format(_("  Line %d does not match sfr definition: "), CurLine+1) + DDFArray.Item(CurLine) + _(". Skipping"));
            continue;
          }

        } else {
          //
          // No sfr line
          //
          pMsg->DebugLog(wxString::Format(_("  Line %d ended sfr section: "), CurLine+1) + DDFArray.Item(CurLine));
          pMsg->DebugLog(wxString::Format(_("  %d unsorted registers, %d unsorted subregisters"), UnsortedRegisters.GetCount(), UnsortedSubRegisters.GetCount()));
          CurrentSection = 0;
          CurLine--;
          break;
        }
      } while (CurLine < Count && CurrentSection == 1);
      CurrentSection = 0;
    } else if (CurrentSection == 2) { // We are in SFRGroup section
      //
      //group = "DBG","DBGMCU_IDCODE","DBGMCU_CR","DBGMCU_APB1_FZ","DBGMCU_APB2_FZ"
      //
      CurLine--;
      do {
        CurLine++;
        if (CurLine >= Count) {
          pMsg->DebugLog(wxString::Format(_("  Line %d ended group section: eof"), Count));
          break;
        }
        ProgCounter = ProgMaxCount/Count*CurLine;
        progress->Update(ProgCounter, _("Parsing ... group section..."));

        if (DDFArray.Item(CurLine).StartsWith(_(";")) || DDFArray.Item(CurLine).IsEmpty()) {
          continue;
        } else if (DDFArray.Item(CurLine).StartsWith(_("group = "), &CurrentSfr)) {
          clRegisterGroup RegisterGroupElement;

          GroupCount++;
          //
          // Add Unsorted registers to Group
          //
          CurrentSfr.Replace(_("\""), wxEmptyString, true);
          GroupArray = GetArrayFromString(CurrentSfr, _(","));

          if(GroupArray.GetCount() <= 1) {
            GroupSkip++;
            pMsg->DebugLog(_("  Empty group: ") + CurrentSfr + _(" Skipping."));
            continue;
          }
          //
          // Fill Group element's name
          //
          RegisterGroupElement.sName = GroupArray.Item(0);
          //
          // Count of registers for the group
          //
          for (j = 1; j < GroupArray.GetCount(); j++) {
            //
            // Find register in unsorted list of registers
            //
            for (k = 0; k < UnsortedRegisters.GetCount(); k++) {
              //
              //
              //
              if (GroupArray.Item(j).CmpNoCase(UnsortedRegisters.Item(k).sName) == 0) {
                RegisterGroupElement.aRegister.Add(UnsortedRegisters.Item(k));
                UnsortedRegisters.RemoveAt(k);
                break;
              }
            }
            if (k == UnsortedRegisters.GetCount() && !UnsortedRegisters.IsEmpty()) {
              pMsg->DebugLog(_("  Register not found ") + GroupArray.Item(j) + _(" Skipping."));
              RegisterSkip++;
            }
          }
          m_aRegisterGroup->Add(RegisterGroupElement);
        } else {
          //
          // No group line
          //
          pMsg->DebugLog(wxString::Format(_("  Line %d ended group section: "), CurLine+1) + DDFArray.Item(CurLine));
          CurrentSection = 0;
          CurLine--;
          break;
        }
      } while (CurLine < Count && CurrentSection == 2);
      CurrentSection = 0;
    }
  }
  //
  // Done parsing file
  // Check if everything is filled
  //
  if (UnsortedRegisters.GetCount() > 0) {
    pMsg->DebugLog(wxString::Format(_("  Still %d unsorted registers. "), UnsortedRegisters.GetCount()));
  }
  if (UnsortedSubRegisters.GetCount() > 0) {
    pMsg->DebugLog(wxString::Format(_("  Still %d unsorted subregisters. "), UnsortedSubRegisters.GetCount()));
  }

  ProgCounter = ProgMaxCount;
  progress->Update(ProgCounter);

  delete progress;
  if (DDFFile.IsOpened()) {
    DDFFile.Close();
  }
  pMsg->DebugLog(wxString::Format(_T("Register definitions succesfully parsed. %d fields, in %d registers, in %d register groups added. (%d/%d/%d skipped.)"), FieldCount, RegisterCount, GroupCount, FieldSkip, RegisterSkip, GroupSkip));
  pMsg->Log(wxString::Format(_T("Register definitions succesfully parsed. %d fields, in %d registers, in %d register groups added. (%d/%d/%d skipped.)"), FieldCount, RegisterCount, GroupCount, FieldSkip, RegisterSkip, GroupSkip));
  return true;
}

/********************************************************************
**  MMRDlg::ParseDevices()
**
*/
void MMRDlg::ParseDevices(void) {
  TiXmlDocument Doc;
  TiXmlElement* Root;
  TiXmlElement* DeviceNode;
  clDevices     Device;
  wxString      DeviceListFile;
  wxString      FileRoot;

  LogManager* pMsg = Manager::Get()->GetLogManager();

  //
  // Load device list file
  //
  FileRoot = ConfigManager::GetExecutableFolder() + wxFILE_SEP_PATH + _T("share") + wxFILE_SEP_PATH + _T("emIDE") + wxFILE_SEP_PATH;
  DeviceListFile = FileRoot + _T("DeviceList.emDat");
  if (!TinyXML::LoadDocument(DeviceListFile, &Doc)) {
    pMsg->DebugLog(_T("Failed to load file ")+FileRoot + _T("DeviceList.emDat"));
    return;
  } else {
    pMsg->DebugLog(_T("File ") + FileRoot + _T("DeviceList.emDat") + _T(" loaded successfully."));
  }
  //
  // Check if it is the device list file
  //
  Root = Doc.FirstChildElement("devicelist");
  if (!Root) {
    pMsg->DebugLog(_T("  Failed to verify DeviceList file. \"<devicelist>\" not found."));
    pMsg->LogError(_T("Failed to verify DeviceList file. \"<devicelist>\" not found."));
    pMsg->LogError(_T("Please report this to info@emIDE.org."));
    return;
  }
  //
  // Parse all devices
  //
  DeviceNode = Root->FirstChildElement("device");
  while (DeviceNode)  {
    //
    // Get device infos
    //
    Device.DeviceName = cbC2U(DeviceNode->FirstChildElement("name")->GetText());
    Device.SVDFile.Assign(cbC2U(DeviceNode->FirstChildElement("svdfile")->GetText()));
    Device.SVDFile.MakeAbsolute(FileRoot);
    if (Device.DeviceName.IsEmpty()) {
      pMsg->DebugLog(_T("  No device name found for device in devicelist."));
      DeviceNode = DeviceNode->NextSiblingElement("device");
      continue;
    }
    if (!Device.SVDFile.FileExists()) {
      pMsg->DebugLog(_T("  SVD file for ") + Device.DeviceName + _T(" (") + Device.SVDFile.GetFullPath() + _T(") does not exist."));
      pMsg->LogError(_T("SVD file for ") + Device.DeviceName + _T(" (") + Device.SVDFile.GetFullPath() + _T(") does not exist."));
      pMsg->LogError(_T("Please report this to info@emIDE.org."));
      DeviceNode = DeviceNode->NextSiblingElement("device");
      continue;
    }
    m_aDevices.Add(Device);
    DeviceNode = DeviceNode->NextSiblingElement("device");
  }
  pMsg->DebugLog(wxString::Format(_T("  %d devices with svd files found."), m_aDevices.GetCount()));
}
/********************************************************************
**  MMRDlg::OnExpand()
**
*/
void MMRDlg::OnExpand(wxPropertyGridEvent &event) {
  wxPGId prop;

  prop = event.GetProperty();
  if (prop->IsCategory()) {
    //
    // Is group
    //
    OnCategoryExpand(prop);
  } else {
    //
    // Is register or flag
    //
  }
}

/********************************************************************
**  MMRDlg::OnCategoryExpand()
**
*/
void MMRDlg::OnCategoryExpand(wxPGId category) {
  int Group;
  cbDebuggerPlugin* plugin;

  plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {
    for (Group = 0; Group < (int)m_aRegisterGroup->GetCount(); Group++) {
      if (m_aRegisterGroup->Item(Group).gId == category) {
        if (m_aRegisterGroup->Item(Group).UpdateCount != m_UpdateCount) {
          m_aRegisterGroup->Item(Group).UpdateCount = m_UpdateCount;
          plugin->UpdateMMRGroup(Group);
          break;
        } else {
          break;
        }
      }
    }
  }
}
/********************************************************************
**  MMRDlg::OnPropertyRightClick()
**
*/
void MMRDlg::OnPropertyRightClick(wxPropertyGridEvent &event) {
  wxPGId property;
  wxMenu menu;
  cbDebuggerPlugin* plugin;

  property = event.GetProperty();
  if (property->IsCategory()) {
    plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();

    menu.Append(idMenuUpdate, _("Update Group"), _("Update the registers of this group"));
    if(plugin && plugin->IsRunning() && plugin->IsStopped()) {
      menu.Enable(idMenuUpdate, true);
    } else {
      menu.Enable(idMenuUpdate, false);
    }
    menu.Append(idMenuExpand, _("Expand/Collapse"), _("Expand/Collapse the group"));
    menu.Enable(idMenuExpand, true);

    PopupMenu(&menu);
  } else if (property->GetChildCount() > 0) {
    plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();

    menu.Append(idMenuUpdate, _("Update Register"), _("Update the register"));
    if(plugin && plugin->IsRunning() && plugin->IsStopped()) {
      menu.Enable(idMenuUpdate, true);
    } else {
      menu.Enable(idMenuUpdate, false);
    }
    menu.Append(idMenuExpand, _("Expand/Collapse"), _("Expand/Collapse the register"));
    menu.Enable(idMenuExpand, true);
    PopupMenu(&menu);
  } else {

  }
}

/********************************************************************
**  MMRDlg::OnPropertyChanged()
**
*/
void MMRDlg::OnPropertyChanged(wxPropertyGridEvent &event) {
  wxPGId            propRegister;
  wxPGId            propGroup;
  wxPGId            propFlag;
  cbDebuggerPlugin* plugin;
  wxString          ValueText;
  unsigned long     RegisterValue;
  unsigned long     Value;
  unsigned long     Address;
  unsigned long     Size;
  unsigned long     BitMask;
  unsigned long     Shift;
  int               GroupItem;
  int               RegisterItem;
  int               FlagItem;

  propRegister = event.GetProperty();

  propGroup = propRegister->GetParent();
  if (propGroup->IsCategory()) {
    //
    // Changed a register
    // Find Register in Array
    //
    propFlag = 0;
    for (GroupItem = 0; GroupItem < (int)m_aRegisterGroup->GetCount(); GroupItem++) {
      if (m_aRegisterGroup->Item(GroupItem).gId == propGroup) {
        for (RegisterItem = 0; RegisterItem < (int)m_aRegisterGroup->Item(GroupItem).aRegister.GetCount(); RegisterItem++) {
          if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).rID == propRegister) {
            break;
          }
        }
        break;
      }
    }
    if (GroupItem == (int)m_aRegisterGroup->GetCount() || RegisterItem == (int)m_aRegisterGroup->Item(GroupItem).aRegister.GetCount()) {
      //
      // Error, register not found
      //
      return;
    }
    ValueText = propRegister->GetValueAsString();
  } else {
    //
    // Changed a flag
    //
    propFlag = propRegister;
    propRegister = propGroup;
    propGroup = propRegister->GetParent();
    if (!(propGroup->IsCategory())) {
      return;
    }
    for (GroupItem = 0; GroupItem < (int)m_aRegisterGroup->GetCount(); GroupItem++) {
      if (m_aRegisterGroup->Item(GroupItem).gId == propGroup) {
        for (RegisterItem = 0; RegisterItem < (int)m_aRegisterGroup->Item(GroupItem).aRegister.GetCount(); RegisterItem++) {
          if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).rID == propRegister) {
            for (FlagItem = 0; FlagItem < (int)m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.GetCount(); FlagItem++) {
              if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.Item(FlagItem).fID == propFlag) {
                break;
              }
            }
            break;
          }
        }
        break;
      }
    }
    if (GroupItem == (int)m_aRegisterGroup->GetCount() || RegisterItem == (int)m_aRegisterGroup->Item(GroupItem).aRegister.GetCount() || FlagItem == (int)m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.GetCount()) {
      //
      // Error, register/flag not found
      //
      return;
    }
    ValueText = propFlag->GetValueAsString();
  }

  plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if(plugin && plugin->IsRunning() && plugin->IsStopped()) {
    if(!ValueText.ToULong(&Value, 16)) {
      //
      // Reset Value
      //
      if (propFlag) {
        Value = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.Item(FlagItem).Value;
        ValueText = wxString::Format(wxT("%X"),Value);
        propFlag->SetValueFromString(ValueText);
      } else if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Access == 2) {
        propRegister->SetValueFromString(_("<Click to write register>"));
      } else {
        Value = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Value;
        ValueText = wxString::Format(wxT("0x%.8X"),Value);
        propRegister->SetValueFromString(ValueText);
      }
    } else {
      //
      // Update saved value
      //
      if (propFlag) {
        //
        // Calculate Value of register with changed flag
        //
        RegisterValue = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Value;
        BitMask = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.Item(FlagItem).BitMask;
        for (Shift = 0; Shift < m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Size; Shift++) {
          if ((BitMask >> Shift) & 1) {
            break;
          }
        }
        Value = (RegisterValue & (~BitMask)) | ((Value << Shift) & BitMask);
      }
      Address = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Address;
      Size  = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Size;
      plugin->ChangePeripheralValue(Address, Value, Size);
      if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Access == 2) {
        propRegister->SetValueFromString(_("<Click to write register>"));
      } else {
        SetMMRValue(Address, Value);
      }
    }
  } else {
    //
    // Reset Value
    //
    if (propFlag) {
      Value = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).aSubRegister.Item(FlagItem).Value;
      ValueText = wxString::Format(wxT("%X"),Value);
      propFlag->SetValueFromString(ValueText);
    } else if (m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Access == 2) {
      propRegister->SetValueFromString(_("<Click to write register>"));
    } else {
      Value = m_aRegisterGroup->Item(GroupItem).aRegister.Item(RegisterItem).Value;
      ValueText = wxString::Format(wxT("0x%.8X"),Value);
      propRegister->SetValueFromString(ValueText);
    }
  }
}

/********************************************************************
**  MMRDlg::OnPropertyChanged()
**
*/
void MMRDlg::OnPropertyChanging(wxPropertyGridEvent &event) {
  wxPGId            propRegister;

  propRegister = event.GetProperty();
  propRegister->SetValue(wxEmptyString);
}

/********************************************************************
**  MMRDlg::OnMenuUpdate()
**
*/
void MMRDlg::OnMenuUpdate(wxCommandEvent &event) {
  wxPGId selected;
  wxPGId parent;
  cbDebuggerPlugin* plugin;
  int Group;
  int Register;

  selected = m_PropGrid->GetSelection();
  if (selected->IsCategory()) {
    plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
    if (plugin) {
      for (Group = 0; Group < (int)m_aRegisterGroup->GetCount(); Group++) {
        if (m_aRegisterGroup->Item(Group).gId == selected) {
          m_aRegisterGroup->Item(Group).UpdateCount = m_UpdateCount;
          plugin->UpdateMMRGroup(Group);
          break;
        }
      }
    }
  } else {
    parent = selected->GetParent();
    if (parent) {
      plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
      if (plugin) {
        for (Group = 0; Group < (int)m_aRegisterGroup->GetCount(); Group++) {
          if (m_aRegisterGroup->Item(Group).gId == parent) {
            for (Register = 0; Register < (int)m_aRegisterGroup->Item(Group).aRegister.GetCount(); Register++) {
              if (m_aRegisterGroup->Item(Group).aRegister[Register].rID == selected) {
                plugin->UpdateMMR(Group, Register);
                break;
              }
            }
            break; // Not Found
          }
        }
      }
    }
  }
}
/********************************************************************
**  MMRDlg::OnMenuExpand()
**
*/
void MMRDlg::OnMenuExpand(wxCommandEvent &event) {
  wxPGId selected;

  selected = m_PropGrid->GetSelection();
  if(selected->IsExpanded()) {
    m_PropGrid->Collapse(selected);
  } else {
    m_PropGrid->Expand(selected);
  }
}




