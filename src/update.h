#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#  include <wx/wx.h>
#endif

class UpdateThread : public wxThread {
public:
                UpdateThread  (bool AlwaysInfo) {
                  m_AlwaysInfo = AlwaysInfo;
                }
  virtual void* Entry         (void) {
                  CheckForUpdate(m_AlwaysInfo);
                  return NULL;
                }
  static  void  CheckForUpdate(bool AlwaysInfo = false);
private:
          bool  m_AlwaysInfo;
};
