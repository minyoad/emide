#ifndef JLINKEDITREGISTER_H
#define JLINKEDITREGISTER_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(JLinkEditRegister)
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	#include <wx/dialog.h>
	//*)
#endif
//(*Headers(JLinkEditRegister)
//*)

class JLinkEditRegister: public wxDialog
{
	public:

		JLinkEditRegister(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~JLinkEditRegister();

		//(*Declarations(JLinkEditRegister)
		wxStaticText* StaticText2;
		wxTextCtrl* txtRegValue;
		wxStaticText* txtRegName;
		//*)

	protected:

		//(*Identifiers(JLinkEditRegister)
		static const long ID_STATICTEXT2;
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL1;
		//*)

	private:

		//(*Handlers(JLinkEditRegister)
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
