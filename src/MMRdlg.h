/********************************************************************
**  This file is part of the JLink plugin for emIDE
**
**  (c) 2012-2013 SEGGER Microcontroller Systeme GmbH
**  Internet: www.segger.com
**
**  File: MMR.h
**  Purpose:
**
********************************************************************/
#ifndef MMR_H_INCLUDED
#define MMR_H_INCLUDED

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <cbdebugger_interfaces.h>
#include <wx/propgrid/propgrid.h>

/********************************************************************
**      CLASSES FORWARD DECLARATION
**/
class clSubRegister;
class clRegister;
class clRegisterGroup;
class clProjects;
class clDevices;
class wxPropertyGrid;
class wxPropertyGridEvent;

/********************************************************************
**      ARRAYS
**/
WX_DECLARE_OBJARRAY(clSubRegister,   SubRegisterArray);
WX_DECLARE_OBJARRAY(clRegister,      RegisterArray);
WX_DECLARE_OBJARRAY(clRegisterGroup, RegisterGroupArray);
WX_DECLARE_OBJARRAY(clProjects,      MMRProjectsArray);
WX_DECLARE_OBJARRAY(clDevices,       MMRDevicesArray);

/********************************************************************
**      CLASSES
**/
/*********************************************************************
*
*       Class MMR: Memory Mapped Register panel
*/
class MMRDlg : public wxPanel, public cbMMRDlg {
public:
            MMRDlg(wxWindow* parent);
  void      ToggleWindow(void);

  wxWindow* GetWindow() { return this; }
  void EnableWindow(bool enable);

  bool      SetMMRValue(unsigned long Address, unsigned long Value);
  int       InsertRD(const wxString& ProjectTitle, const wxString& BuildTarget, const wxString& FileName, bool SkipKnown);
  int       InsertDeviceRD(const wxString& ProjectTitle, const wxString& BuildTarget, const wxString& DeviceName, bool SkipKnown);
  bool      OpenRD(void);
  void      ClearMMRs(void);
  void      ClearRD(void);
  void      Start(void);
  void      End(void);
  void      InsertMMRs(void);
  wxString  GetUpdateString(int i);
  wxString  GetUpdateStringRegister(int g, int r);
  int       GetExpandedGroup(int PrevGroup);
protected:
  bool      OpenSVD(wxFileName FileName);
  bool      OpenDDF(wxFileName FileName);
  void      ParseDevices(void);

private:
  void OnExpand             (wxPropertyGridEvent &event);
  void OnCategoryExpand     (wxPGId category);
  void OnPropertyRightClick (wxPropertyGridEvent &event);
  void OnPropertyChanged    (wxPropertyGridEvent &event);
  void OnPropertyChanging   (wxPropertyGridEvent &event);
  void OnMenuUpdate         (wxCommandEvent &event);
  void OnMenuExpand         (wxCommandEvent &event);
//  void OnCollapse(wxPropertyGridEvent &event);
//  void OnPropertySelected(wxPropertyGridEvent &event);

  RegisterGroupArray* m_aRegisterGroup;
  MMRProjectsArray    m_aProjects;
  MMRDevicesArray     m_aDevices;
  wxPropertyGrid*     m_PropGrid;
  int                 m_UpdateCount;

  DECLARE_EVENT_TABLE();
};

/*********************************************************************
*
*       Class clSubRegister: Objects for sub register definitions array
*/
class clSubRegister {
public:
  wxString      sName;
  wxString      sDescription;
  wxPGId        fID;
  unsigned long BitMask;
  unsigned int  Access;  // read-write: 3; read-only: 1; write-only: 2; field-defined: 4;
  int           Base;
  unsigned long Value;
};
/*********************************************************************
*
*       Class clRegister: Objects for register definitions array
*/
class clRegister {
public:
  wxString         sName;
  wxString         sDescription;
  wxPGId           rID;
  unsigned long    Address;
  unsigned long    Size;
  int              Base;
  unsigned long    Value;
  unsigned int     Access;  // read-write: 3; read-only: 1; write-only: 2; field-defined: 4;
  SubRegisterArray aSubRegister;
};
/*********************************************************************
*
*       Class clRegisterGroup: Objects for register group definitions array
*/
class clRegisterGroup {
public:
  wxString      sName;
  wxString      sDescription;
  wxPGId        gId;
  RegisterArray aRegister;
  unsigned long BaseAddress;
  int           UpdateCount;
};

class clProjects {
public:
  wxString            ProjectTitle;
  wxString            BuildTarget;
  wxFileName          FileName;
  RegisterGroupArray  aRegisterGroups;
};

class clDevices {
public:
  wxString            DeviceName;
  wxFileName          SVDFile;
};
#endif
/* EOF */
