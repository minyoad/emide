/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: memorydump.cpp
**  Purpose:  Interactive memory dump window in debug view
**
********************************************************************/


/********************************************************************
**      INCLUDES
**/
#include "sdk.h"

#ifndef CB_PRECOMP
#include <wx/button.h>
#include <wx/combobox.h>
#include <wx/intl.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/xrc/xmlres.h>

#include "cbplugin.h"
#endif

#include "memorydump.h"
#include "debuggermanager.h"

/********************************************************************
**      EVENTS
**/
BEGIN_EVENT_TABLE(JLMemoryDump, wxPanel)
  EVT_BUTTON(XRCID("btnDown"), JLMemoryDump::OnScrollDown)
  EVT_BUTTON(XRCID("btnUp"), JLMemoryDump::OnScrollUp)
  EVT_BUTTON(XRCID("btnUpdate"), JLMemoryDump::OnUpdate)
  EVT_BUTTON(XRCID("btnGoTo"), JLMemoryDump::OnGoToAddress)
  EVT_CHOICE(XRCID("cmbUnitsize"), JLMemoryDump::OnChoice)
  EVT_MOUSEWHEEL(JLMemoryDump::OnScroll)
  EVT_MOUSE_EVENTS(JLMemoryDump::OnMouse)
//  EVT_CHILD_FOCUS(JLMemoryDump::GetPanelFocus)
END_EVENT_TABLE()

/********************************************************************
**      CLASSFUNCTIONS
**/

JLMemoryDump::JLMemoryDump(wxWindow* parent) :
  m_NextScroll(0),
  m_Bytes(512),
  m_ByteStep(128),
  m_FirstAddress(0),
  m_LastAddress(512),
  m_Direction(0),
  m_errorCount(0),
  m_UnitSize(2),
  m_UnitChar(_("b"))

{
  //ctor
  if (!wxXmlResource::Get()->LoadPanel(this, parent, _T("JLMemoryDumpPanel")))
    return;
  m_pText = XRCCTRL(*this, "txtDump", wxTextCtrl);

  m_pText->Connect(wxEVT_MOUSEWHEEL,(wxObjectEventFunction)&JLMemoryDump::OnScrollText);

  wxFont font(8, wxMODERN, wxNORMAL, wxNORMAL);
  m_pText->SetFont(font);

  ClearReal();

  m_pText->AppendText(_T("0x00000000: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000010: -- Please start the debugger for Memory info --    ................\n"));
  m_pText->AppendText(_T("0x00000020: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000030: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000040: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000050: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000060: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000070: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000080: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000090: -- Please start the debugger for Memory info --    ................\n"));
  m_pText->AppendText(_T("0x000000A0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000000B0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000000C0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000000D0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000000E0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000000F0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000100: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000110: -- Please start the debugger for Memory info --    ................\n"));
  m_pText->AppendText(_T("0x00000120: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000130: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000140: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000150: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000160: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000170: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000180: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x00000190: -- Please start the debugger for Memory info --    ................\n"));
  m_pText->AppendText(_T("0x000001A0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000001B0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000001C0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000001D0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000001E0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_pText->AppendText(_T("0x000001F0: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"));
  m_StopWatch.Start();
}

/********************************************************************
**  JLMemoryDump::Begin()
*/
void JLMemoryDump::Begin()
{
  m_pText->Freeze();
}

/********************************************************************
**  JLMemoryDump::End()
*/
void JLMemoryDump::End()
{
  m_pText->Thaw();
  //m_errorCount = 0;
}

/********************************************************************
**  JLMemoryDump::Clear()
**    May be a dummy to clear the memory window
********************************************************************/
void JLMemoryDump::Clear()
{
  m_pText->Clear();
  m_ByteCounter = 0;
  m_LineBytes = wxEmptyString;
  m_LineChars = wxEmptyString;
}

/********************************************************************
**  JLMemoryDump::ClearReal()
**    Really clear the memory window
********************************************************************/
void JLMemoryDump::ClearReal()
{
  m_pText->Clear();
  m_ByteCounter = 0;
  m_LineBytes = wxEmptyString;
  m_LineChars = wxEmptyString;

}

/********************************************************************
**  JLMemoryDump::GetBaseAddress()
**    Get the first address to read and display
********************************************************************/
wxString JLMemoryDump::GetBaseAddress()
{
  return wxString::Format(_T("0x%lx"), m_FirstAddress);
}

/********************************************************************
**  JLMemoryDump::GetBytes()
**    How many bytes to request
********************************************************************/
int JLMemoryDump::GetBytes()
{
  return m_Bytes;// /(m_UnitSize/2);
}

/********************************************************************
**  JLMemoryDump::GetUnitSize()
**    Get The Unit Size
********************************************************************/
wxString JLMemoryDump::GetUnitSize() {
  return m_UnitChar;
}

/********************************************************************
**  JLMemoryDump::AddError()
*/
void JLMemoryDump::AddError(const wxString& err)
{
  m_pText->AppendText(err);
  long pos = m_pText->GetLastPosition();
  m_pText->ShowPosition(pos);
  if(m_errorCount < 1) {
    m_errorCount++;
    cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
    if(plugin) {
      plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
    }
  }
}


/********************************************************************
**  JLMemoryDump::OnScrollDown()
**    Removes 8 Lines per scroll on top and adds 8 lines at the end
********************************************************************/
void JLMemoryDump::OnScrollDown(wxCommandEvent& event)
{
  Begin();

  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+16));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+32));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -Please halt the Target for Memory Info update-    ................\n"), m_LastAddress+48));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+64));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+80));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+96));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+112));
    m_pText->Remove(0, 81*8);
    m_FirstAddress += m_ByteStep;
    m_LastAddress += m_ByteStep;
    m_Direction = 1;
    long pos = m_pText->GetLastPosition();
    m_pText->ShowPosition(pos);
    plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
  }
  End();
}

/********************************************************************
**  JLMemoryDump::OnScrollUp()
**    Removes 8 Lines per scroll on top and adds 8 lines at the end
********************************************************************/
void JLMemoryDump::OnScrollUp(wxCommandEvent& event)
{
  Begin();

  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {
    if(m_FirstAddress >= 16) {
      m_FirstAddress -= m_ByteStep;
      m_LastAddress -= m_ByteStep;
      m_Direction = -1;
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+112));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+96));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+80));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -Please halt the Target for Memory Info update-    ................\n"), m_FirstAddress+64));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+48));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+32));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+16));
      m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress));

      long lastpos = m_pText->GetLastPosition();
      m_pText->Remove(lastpos-81*8, lastpos);
      m_pText->ShowPosition(0);
      plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
    }
  }
  End();
}

/********************************************************************
**  JLMemoryDump::OnScroll()
**    Handles Mousewheelscrolling in Memorydump window
********************************************************************/
void JLMemoryDump::OnScroll(wxMouseEvent& event) {
  if(m_StopWatch.Time() < m_NextScroll) {
    return;
  }
  int rotation = event.GetWheelRotation(); //Amount and direction of rotation
  int lines  = rotation / event.GetWheelDelta() * event.GetLinesPerAction();
  Scroll(rotation, lines);
  m_NextScroll = m_StopWatch.Time() + 200;
}

/********************************************************************
**  JLMemoryDump::OnScrollText()
**    Handles Mousewheelscrolling in txtDump
********************************************************************/
void JLMemoryDump::OnScrollText(wxMouseEvent& event) {
  GetParent()->GetEventHandler()->ProcessEvent(event);
}

/********************************************************************
**  JLMemoryDump::Scroll()
**    Scrolls Memory
********************************************************************/
void JLMemoryDump::Scroll(int rotation, int lines) {
  Begin();
  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {
//    Manager::Get()->GetLogManager()->Log(wxString::Format(_T("Lines: %d"), lines));
    if(lines < 0) { //Scroll down
      int tmplines = lines * (-1);
      for(int i=0; i < tmplines; i++) {
        if(i%3 == 0) {
          m_pText->AppendText(wxString::Format(_T("0x%08lx: -Please halt the Target for Memory Info update-    ................\n"), m_LastAddress+i*16));
        } else {
          m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+i*16));
        }
      }

      m_FirstAddress += 16*tmplines;
      m_LastAddress += 16*tmplines;

      m_pText->Remove(0, 81*tmplines);
      m_Direction = 1;
      long pos = m_pText->GetLastPosition();
      m_pText->ShowPosition(pos);
      plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
    } else { //Scroll up
      if(m_FirstAddress > 0) {
        m_FirstAddress -= 16*lines;
        m_LastAddress -= 16*lines;
        if(m_FirstAddress < 0) {
          m_FirstAddress = 0;
          m_LastAddress = 512;
        }
        for(int i=lines; i != 0; i--) {
          if(i%3 == 0) {
            m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -Please halt the Target for Memory Info update-    ................\n"), m_FirstAddress+16*i));
          } else {
            m_pText->Replace(0, 0, wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_FirstAddress+16*i));
          }
        }
        long lastpos = m_pText->GetLastPosition();
        m_pText->Remove(lastpos-81*lines, lastpos);

      m_Direction = -1;
      m_pText->ShowPosition(0);
      plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
      }

    }
  }
  End();

}

/********************************************************************
**  JLMemoryDump::OnChoice
*/
void JLMemoryDump::OnChoice(wxCommandEvent& event) {
  switch(event.GetSelection()) {
    case 0:
      m_UnitSize = 2;
      m_UnitChar = _("b");
      break;
    case 1:
      m_UnitSize = 4;
      m_UnitChar = _("h");
      break;
    case 2:
      m_UnitSize = 8;
      m_UnitChar = _("w");
      break;
    default:
      m_UnitSize = 2;
      m_UnitChar = _("b");
      break;
  }
  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {
    plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
  }
}

/********************************************************************
**  JLMemoryDump::OnUpdate()
*/
void JLMemoryDump::OnUpdate(wxCommandEvent& event) {
  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if(plugin) {
    plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
  }
}

/********************************************************************
**  JLMemoryDump::OnGoToAddress()
**
********************************************************************/
void JLMemoryDump::OnGoToAddress(wxCommandEvent& event)
{
  wxString NewAddress;
  Begin();
  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if (plugin) {

    NewAddress = XRCCTRL(*this, "txtGoTo", wxTextCtrl)->GetValue();
    if(NewAddress.Contains(wxT("0x")) || NewAddress.Contains(wxT("0X"))) {
      NewAddress.ToULong(&m_FirstAddress, 16);
    } else {
      NewAddress.ToULong(&m_FirstAddress);
    }

    m_LastAddress = m_FirstAddress + m_Bytes;
    ClearReal();

    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+16));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+32));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -Please halt the Target for Memory Info update-    ................\n"), m_LastAddress+48));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+64));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+80));
    m_pText->AppendText(wxString::Format(_T("0x%08lx: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --    ................\n"), m_LastAddress+96));
    m_pText->ShowPosition(0);
    plugin->RequestUpdate(cbDebuggerPlugin::ExamineMemory);
  }
  End();
}

/********************************************************************
**  JLMemoryDump::OnMouse()
*/
void JLMemoryDump::OnMouse(wxMouseEvent& event) {
//  if(event.GetEventType() == wxEVT_RIGHT_DOWN) {
//    Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Rechtsklick!"));
//  } else if(event.GetEventType() == wxEVT_LEFT_DOWN) {
//    Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Linksklick!"));
//  } else if(event.GetEventType() == wxEVT_MOUSEWHEEL || event.GetWheelRotation() != 0) {
//    Manager::Get()->GetLogManager()->Log(wxT("JLMemoryDump | Mousewheel!"));
//  } else if(event.GetEventType() == wxEVT_LEAVE_WINDOW) {
//    Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Window left!"));
//  } else if(event.GetEventType() == wxEVT_ENTER_WINDOW) {
//    Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Window entered!"));
//  } else if(event.GetEventType() == wxEVT_MOTION) {
//    //Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Mouse Motion!"));
//  } else {
//    Manager::Get()->GetLogManager()->Log(_T("JLMemoryDump | Other MouseEvent!"));
//  }
}

/********************************************************************
**  JLMemoryDump::GetPanelFocus()
**    Get the focus to the whole panel
********************************************************************/
void JLMemoryDump::GetPanelFocus(wxChildFocusEvent& event) {
  SetFocusIgnoringChildren();
}
/********************************************************************
**  JLMemoryDump::AddHexByte()
*/
void JLMemoryDump::AddHexByte(const wxString& addr, const wxString& hexbyte)
{
//  int bcmod = m_ByteCounter % 16;
//
//#define HEX_OFFSET(a) (a*3)
#define CHAR_OFFSET(a) (32+32/a+3)

  unsigned long hb;
  hexbyte.ToULong(&hb, 16);

  if(m_ByteCounter == 0) {
    m_TempAddress = m_FirstAddress;
  }

  size_t pos;
  pos = 0;
//  wxString LineBytes;
//  wxString LineText;
  while(pos < hexbyte.Length()) {

    if(pos % m_UnitSize == 0) {
      m_LineBytes << _(" ");
    }
    wxString TempBytes;
    TempBytes << hexbyte[pos];
    TempBytes << hexbyte[pos + 1];

    unsigned long tt;
    TempBytes.ToULong(&tt, 16);

    m_LineBytes << TempBytes;
    if(tt >= 32) {
      m_LineChars << wxChar(tt);
    } else {
      m_LineChars << _T('.');
    }
    pos += 2;
    m_ByteCounter +=2;

    // flush every 16 bytes
    if (m_ByteCounter != 0 && m_ByteCounter % 32 == 0)
    {
      // filled 16 bytes window; append text and reset accumulator array
      //if (m_ByteCounter != 16) // after the first line,
        //m_pText->AppendText(_T('\n')); // prepend a newline
      //unsigned long a;
      //addr.ToULong(&a, 16);
      m_pText->AppendText(wxString::Format(_T("0x%08lx: "), m_TempAddress) + m_LineBytes + wxT("   ") + m_LineChars + wxT("\n"));
      m_TempAddress += 16;
      if(m_Direction == 1) {
        long pos = m_pText->GetLastPosition();
        m_pText->ShowPosition(pos);
      } else if(m_Direction == -1) {
        m_pText->ShowPosition(0);
      }
      m_LineBytes = wxEmptyString;
      m_LineChars = wxEmptyString;
    }

  }
}

/********************************************************************
**  JLMemoryDump::EnableWindow()
*/
void JLMemoryDump::EnableWindow(bool enable)
{
  Enable(enable);
}
