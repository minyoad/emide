/********************************************************************
**  This file is part of emIDE
**  Contains changes to the original C::B source
**  Made by [JL]
********************************************************************/
/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 *
 * $Revision$
 * $Id$
 * $HeadURL$
 */

#include "sdk.h"

#include "cpuregistersdlg.h"
#include "JLinkEditRegister.h"
#include <wx/intl.h>
#include <wx/sizer.h>
#include <wx/listctrl.h>

int ID_LISTREGISTER = wxNewId();

BEGIN_EVENT_TABLE(CPURegistersDlg, wxPanel)
//    EVT_BUTTON(XRCID("btnRefresh"), CPURegistersDlg::OnRefresh)
EVT_LIST_ITEM_ACTIVATED(ID_LISTREGISTER,            CPURegistersDlg::OnEditRegValue)
END_EVENT_TABLE()

CPURegistersDlg::CPURegistersDlg(wxWindow* parent) :
    wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize)
{
    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    m_pList = new wxListCtrl(this, ID_LISTREGISTER, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL);
    sizer->Add(m_pList, 1, wxGROW);
    SetSizer(sizer);
    Layout();

    wxFont font(8, wxMODERN, wxNORMAL, wxNORMAL);
    m_pList->SetFont(font);

    Clear();
}

void CPURegistersDlg::Clear()
{
    m_pList->ClearAll();
    m_pList->Freeze();
    m_pList->DeleteAllItems();
    m_pList->InsertColumn(0, _("Register"), wxLIST_FORMAT_LEFT);
    m_pList->InsertColumn(1, _("Hex"), wxLIST_FORMAT_RIGHT);
    m_pList->InsertColumn(2, _("Flags"), wxLIST_FORMAT_LEFT);
    m_pList->InsertColumn(3, _("Decimal"), wxLIST_FORMAT_RIGHT);
    m_pList->Thaw();
}

int CPURegistersDlg::RegisterIndex(const wxString& reg_name)
{
    for (int i = 0; i < m_pList->GetItemCount(); ++i)
    {
        if (m_pList->GetItemText(i).CmpNoCase(reg_name) == 0)
            return i;
    }
    return -1;
}

/********************************************************************
**  CPURegistersDlg::SetRegisterValue()
**    emIDE change [JL]
**    Set register value, highlight if changed
********************************************************************/
void CPURegistersDlg::SetRegisterValue(const wxString& reg_name, unsigned long int value)
{

    wxColor level1 = wxColor(255, 128, 128);
    wxColor level2 = wxColor(255, 176, 176);
    wxColor level3 = wxColor(255, 224, 224);
    wxString sRegFlags = wxEmptyString;
    // find existing register
    int idx = RegisterIndex(reg_name);
    if (idx == -1)
    {
        // if it doesn't exist, add it
        idx = m_pList->GetItemCount();
        m_pList->InsertItem(idx, reg_name);
    }

/* emIDE change [JL] - Show flags as binary*/
    if(reg_name.Contains(_T("cpsr"))) {
      char Mode = value & 0x1F;
      wxString cpsr_mode, thumb, fiq, irq;
      switch (Mode) {
        case 0x10: cpsr_mode = _T("User mode");   break;
        case 0x11: cpsr_mode = _T("FIQ mode");    break;
        case 0x12: cpsr_mode = _T("IRQ mode");    break;
        case 0x13: cpsr_mode = _T("SVC mode");    break;
        case 0x17: cpsr_mode = _T("ABORT mode");  break;
        case 0x1B: cpsr_mode = _T("UNDEF mode");  break;
        case 0x1F: cpsr_mode = _T("System mode"); break;
        default:
        cpsr_mode = _T("Unknown mode");
      }
      thumb = value & (1 << 5) ? _T(" THUMB") : _T(" ARM");
      fiq = value & (1<< 6) ? _T(" FIQ dis.") : _T("");
      irq = value & (1<< 7) ? _T(" IRQ dis.") : _T("");
      sRegFlags = cpsr_mode + thumb + fiq + irq;
    } else if(reg_name.Contains(_T("xpsr"))) {
      wxChar n, z, c, v, q, t;
      int iciit;
      int ge;
      int exc;
      //
      // Show interpreted XPSR
      //
      n      = ((value >> 31) & 1) ? _T('N') : _T('n');
      z      = ((value >> 30) & 1) ? _T('Z') : _T('z');
      c      = ((value >> 29) & 1) ? _T('C') : _T('c');
      v      = ((value >> 28) & 1) ? _T('V') : _T('v');
      q      = ((value >> 27) & 1) ? _T('Q') : _T('q');
      t      = ((value >> 24) & 1) ? _T('T') : _T('t');
      ge     = (value >> 16) & 15;
      iciit  = (value >> 25) & 3;
      iciit |= (value >>  8) & 0xFC;
      exc    = (value >>  0) & 0x1FF;
      sRegFlags = wxString::Format(wxT("%c%c%c%c%c, GE=0x%.1X, EXC=%d, %c, ICIIT=0x%.2X"), n, z, c, v, q, ge, exc, t, iciit);
    }

    wxListItem info;
    info.m_itemId = idx;
    info.m_mask = wxLIST_MASK_TEXT;

    info.m_col = 1;
    m_pList->GetItem(info);
    wxString temphex = info.m_text.c_str();
//      Manager::Get()->GetLogManager()->Log(temphex + wxString::Format(_T(" : 0x%x"), (size_t)value));
    if(temphex != wxString::Format(_T("0x%x"), (size_t)value) && idx != -1) {
      m_pList->SetItemBackgroundColour(idx, level1);
    } else if(m_pList->GetItemBackgroundColour(idx) == level1) {
      m_pList->SetItemBackgroundColour(idx, level2);
    } else if(m_pList->GetItemBackgroundColour(idx) == level2) {
      m_pList->SetItemBackgroundColour(idx, level3);
    } else {
      m_pList->SetItemBackgroundColour(idx, wxColor(255, 255, 255));
    }
    wxString fmt;
    fmt.Printf(_T("0x%x"), (size_t)value);
    m_pList->SetItem(idx, 1, fmt);
    fmt.Printf(_T("%lu"), value);
    m_pList->SetItem(idx, 3, fmt);
    if(sRegFlags != wxEmptyString) {
      m_pList->SetItem(idx, 2, sRegFlags);
    }

    for (int i = 0; i < 4; ++i)
    {
        m_pList->SetColumnWidth(i, wxLIST_AUTOSIZE);
    }
}

void CPURegistersDlg::OnEditRegValue(wxListEvent& event) {
  wxListItem info;
  info.m_itemId = event.GetIndex();
  info.m_mask = wxLIST_MASK_TEXT;

  info.m_col = 0;
  m_pList->GetItem(info);
  wxString RegName = info.m_text;

  info.m_col = 1;
  m_pList->GetItem(info);
  wxString RegValueHex = info.m_text;
  wxString* EditValueHex = &RegValueHex;

  info.m_col = 3;
  m_pList->GetItem(info);
  wxString RegValueDec = info.m_text;
  wxString* EditValueDec = &RegValueDec;

  if(EditRegisterDlg(RegName, EditValueHex, EditValueDec) != 0) {
    return;
  }

  cbDebuggerPlugin *plugin = Manager::Get()->GetDebuggerManager()->GetActiveDebugger();
  if(plugin) {
    plugin->ChangeRegisterValue(RegName, RegValueHex);
  }

  m_pList->SetItem(event.GetIndex(), 1, RegValueHex);
  m_pList->SetItem(event.GetIndex(), 3, RegValueDec);
  m_pList->SetColumnWidth(1, wxLIST_AUTOSIZE);
  m_pList->SetColumnWidth(3, wxLIST_AUTOSIZE);

}

int CPURegistersDlg::EditRegisterDlg(wxString RegName, wxString* RegValueHex, wxString* RegValueDec) {
  unsigned long temp;
  wxString EditValue = *RegValueHex;
  wxString NewRegValue;
  JLinkEditRegister dialog(this);
  dialog.txtRegName->SetLabel(RegName);
  dialog.txtRegValue->SetLabel(EditValue);
  if(dialog.ShowModal() == wxID_OK) {
    NewRegValue = dialog.txtRegValue->GetValue();
    if(NewRegValue.StartsWith(wxT("0x")) || NewRegValue.StartsWith(wxT("0X"))) {
      if(NewRegValue.ToULong(&temp, 16)) {
        *RegValueHex = NewRegValue;
        *RegValueDec = wxString::Format(wxT("%lu"), temp);
      } else {
        wxMessageBox(*RegValueHex + wxT(" is not a valid hexadecimal Value!"));
        return 1;
      }
    } else {
      if(NewRegValue.ToULong(&temp, 10)) {
        *RegValueDec = NewRegValue;
        *RegValueHex = wxString::Format(wxT("0x%x"), temp);
      } else {
        wxMessageBox(*RegValueDec + wxT(" is not a valid decimal Value!"));
        return 1;
      }
    }

    return 0;
  } else {
    return 1;
  }
}

void CPURegistersDlg::EnableWindow(bool enable)
{
    m_pList->Enable(enable);
}
