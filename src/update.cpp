#include "update.h"
#include <wx/fs_inet.h>
#include <manager.h>
#include "configmanager.h"
#include "appglobals.h"

void UpdateThread::CheckForUpdate(bool AlwaysInfo) {
  wxFileSystem*   pFileSystem;
  wxFSFile*       pFile;
  wxInputStream*  pStream;
  size_t          StreamSize;
  char*           pBuffer;
  wxString        sText;
  bool            CheckUpdates;
  int             LastCheck;
  int             TodayCheck;
  int             LastVersion;
  int             NewVersion;
  wxTimeSpan      ts;

  wxDateTime refDate(12, wxDateTime::Sep, 2013, 0, 0, 0, 0); // Reference date for timespan between last update check
  wxDateTime now(wxDateTime::Now());

  CheckUpdates = false;
  if (!(Manager::Get()->GetConfigManager(_T("app"))->Read(_T("/environment/check_updates"), &CheckUpdates))) {
    //
    // Check updates not found.
    // By default enable it.
    //
    //if (cbMessageBox(_("Do you want to allow emIDE to regularly check for updates?"), _("emIDE Update Check"), wxICON_QUESTION | wxYES_NO) == wxID_YES) {
        CheckUpdates = true;
    //}
    Manager::Get()->GetConfigManager(_T("app"))->Write(_T("/environment/check_updates"), CheckUpdates);
  }

  //
  // If allowed to check for updates
  // Check if the last update was at least 7 days ago
  // If yes, get current version.dat from emide.org
  // Read version number and compare it with last fetched version number
  // If newer, inform user about new version
  //
  if (CheckUpdates || AlwaysInfo) {
    ts = now.Subtract(refDate);
    TodayCheck = ts.GetDays();

    LastCheck = 0;
    Manager::Get()->GetConfigManager(_T("app"))->Read(_T("/environment/last_update_check"), &LastCheck);
    if (((TodayCheck-7) >= LastCheck) || AlwaysInfo == true) {
      pFileSystem = new wxFileSystem;
//#warning "MAKE SURE TO CHANGE LOCAL PATH TO URL FOR VERSION.DAT"
      pFile = pFileSystem->OpenFile(_("http://emide.org/version.dat"));
      if (pFile) {
        pStream = pFile->GetStream();
        StreamSize = pStream->GetSize();
        pBuffer = new char[StreamSize + 1];
        pStream->Read((void*) pBuffer, StreamSize);
        pBuffer[StreamSize] = 0;
        sText = wxString::FromAscii(pBuffer);
        delete[] pBuffer;
        delete pFile;
        delete pFileSystem;

        NewVersion = wxAtoi(sText);
        if (NewVersion > 0) {
          LastVersion = VERSION_NUMBER;
          if (AlwaysInfo == false) {
            Manager::Get()->GetConfigManager(_T("app"))->Read(_T("/environment/last_update_version"), &LastVersion);
          }
          if (NewVersion > LastVersion) {
            //
            // New Version available.
            // Inform User
            //
            wxString sMessage;
            int Major;
            int Minor;
            char Revision;

            Major = (NewVersion/10000);
            Minor = ((NewVersion/100)%100);
            Revision = (NewVersion%100) ? ((NewVersion%100) + 'a' - 1) : ' ';


            sMessage = wxString::Format(_("A new version of emIDE (V%d.%d%c) is available.\n\nIt can be downloaded at http://emide.org\n\nUpdate checks can be disabled at 'Settings -> Environment...'."), Major, Minor, Revision);
            cbMessageBox(sMessage, _("emIDE Update Notification"));
            Manager::Get()->GetConfigManager(_T("app"))->Write(_T("/environment/last_update_version"), NewVersion);
          }
        }
      }
    }
    Manager::Get()->GetConfigManager(_T("app"))->Write(_T("/environment/last_update_check"), TodayCheck);
  }
}
