#ifndef JLINKDEVICES_H
#define JLINKDEVICES_H


struct JLinkDeviceInfo
{
    wxString      Manufacturer;
    wxString      DeviceName;
    wxString      Core;
    wxArrayString ROMAreas;
    wxArrayString ROMSizes;
    wxArrayString RAMAreas;
    wxArrayString RAMSizes;
};

WX_DECLARE_OBJARRAY(JLinkDeviceInfo, JLinkDevices);

#endif
