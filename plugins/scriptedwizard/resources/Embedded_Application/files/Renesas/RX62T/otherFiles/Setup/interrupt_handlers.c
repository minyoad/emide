/************************************************************************************
 *  File:     interrupt_handlers.c
 *  Purpose:  Interrupt handlers for RX62T
 *  Date:     26 September 2013
 ************************************************************************************/

#include "interrupt_handlers.h"

// Exception(Supervisor Instruction)
void INT_Excep_SuperVisorInst(void){/* brk(); */}

// Exception(Undefined Instruction)
void INT_Excep_UndefinedInst(void){/* brk(); */}

// Exception(Floating Point)
void INT_Excep_FloatingPoint(void){/* brk(); */}

// NMI
void INT_NonMaskableInterrupt(void){/* brk(); */}

// Dummy
void Dummy(void){/* brk(); */}

// BRK
void INT_Excep_BRK(void){ /*wait(); */}

//Reserved  0 0000h
	                               
//Reserved  1 0004h 
	                                 
//Reserved  2 0008h                                   
 
//Reserved  3 000Ch                                   
 
//Reserved  4 0010h                                   
 
//Reserved  5 0014h                                   
 
//Reserved  6 0018h                                   
 
//Reserved  7 001Ch                                   
 
//Reserved  8 0020h                                   
 
//Reserved  9 0024h                                   
 
//Reserved  10 0028h                                   
 
//Reserved  11 002Ch                                   
 
//Reserved  12 0030h                                   
 
//Reserved  13 0034h                                   
 
//Reserved  14 0038h                                   
 
//Reserved  15 003Ch                                   
 

//Bus error BUSERR 16 0040h   
void  INT_Excep_BUSERR(void) { }
//Reserved  17 0044h   
 
//Reserved  18 0048h   
 
//Reserved  19 004Ch   
 
//Reserved  20 0050h   
 

//FCUIF FIFERR 21 0054h  
void  INT_Excep_FCUIF_FIFERR(void) { }

//Reserved  22 0058h    
 

//FRDYI 23 005Ch  
void  INT_Excep_FCUIF_FRDYI(void) { }
  //Reserved  24 0060h  
 
     //Reserved  25 0064h  
 
     //Reserved  26 0068h                         
 
	 
	 
//ICU SWINT 27 006Ch 
void  INT_Excep_ICU_SWINT(void) { }
//CMT0 CMI0 28 0070h 
void  INT_Excep_CMT0_CMI0(void) { }
//CMT0 CMI1 29 0074h      
void  INT_Excep_CMT0_CMI1(void) { }
//CMT1 CMI2 30 0078h 
void  INT_Excep_CMT1_CMI2(void) { }
//CMT1 CMI3 31 007Ch      
void  INT_Excep_CMT1_CMI3 (void) { }
 //Reserved      32 0080h             
 
//Reserved  33 0084h                  
 
//Reserved  34 0088h                       
 
//Reserved  35 008Ch                       
 
//Reserved  36 0090h                      
 
//Reserved  37 0094h                      
 
//Reserved  38 0098h                      
 
//Reserved  39 009Ch                     
 
//Reserved  40 00A0h                     
 
//Reserved  41 00A4h                     
 
//Reserved  42 00A8h                     
 
//Reserved  43 00ACh                 
 
//RSPIO_SPEI0 44 00B0h 
void  INT_Excep_RSPIO_SPEI0(void) { }
//RSPIO_SPRI0 45 00B4h
void  INT_Excep_RSPIO_SPRI0(void) { }	 
//RSPIO_SPTI0 46 00B8h 
void  INT_Excep_RSPIO_SPTI0(void) { }
//RSPIO_SPII0 47 00BCh 
void  INT_Excep_RSPIO_SPII0(void) { }	 
//Reserved  48 00C0h              
 
//Reserved  49 00C4h              
 
//Reserved  50 00C8h              
 
//Reserved  51 00CCh              
 
//Reserved  52 00D0h              
 
//Reserved  53 00D4h                    
 
//Reserved  54 00D8h                     
 
//Reserved  55 00DCh                     
 
	
//CAN0_ERS0 56 00E0h 
void  INT_Excep_CAN0_ERS0(void) { }
//CAN0_RXF0 57 00E4h      
void  INT_Excep_CAN0_RXF0(void) { }
//CAN0_TXF0 58 00E8h
void  INT_Excep_CAN0_TXF0(void) { }	
//CAN0_RXM0 59 00ECh
void  INT_Excep_CAN0_RXM0(void) { }	
//CAN0_TXM0 60 00F0h  
void  INT_Excep_CAN0_TXM0(void) { }




//Reserved  61 00F4h                     

//Reserved  62 00F8h                     
 
//Reserved  63 00FCh                      
 

	
//IRQ0 64 0100h  
void  INT_Excep_IRQ0(void) { }
//IRQ1 65 0104h  
void  INT_Excep_IRQ1(void) { }
//IRQ2 66 0108h  
void  INT_Excep_IRQ2(void) { }
//IRQ3 67 010Ch  
void  INT_Excep_IRQ3(void) { }
//IRQ4 68 0110h  
void  INT_Excep_IRQ4(void) { }
//IRQ5 69 0114h  
void  INT_Excep_IRQ5(void) { }
//IRQ6 70 0118h  
void  INT_Excep_IRQ6(void) { }
//IRQ7 71 011Ch  
void  INT_Excep_IRQ7(void) { }

//Reserved  72 0120h                     
 
//Reserved  73 0124h                     
 
//Reserved  74 0128h                     
 
//Reserved  75 012Ch                     
 
//Reserved  76 0130h                     
 
//Reserved  77 0134h                     
 
//Reserved  78 0138h                     
 
//Reserved  79 013Ch                     
 
//Reserved  80 0140h                     
 
//Reserved  81 0144h                     
 
//Reserved  82 0148h                     
 
//Reserved  83 014Ch                     
 
//Reserved  84 0150h                     
 
//Reserved  85 0154h                     
 
//Reserved  86 0158h               
 
//Reserved  87 015Ch                     
 

//Reserved  88 0160h                
 
//Reserved  89 0164h                
 
//Reserved  90 0168h                
 
//Reserved  91 016Ch                
 
//Reserved  92 0170h                
 
//Reserved  93 0174h                
 
//Reserved  94 0178h                
 
//Reserved  95 017Ch                
 

//WDT WOVI 96 0180h   
void  INT_Excep_WDT_WOVI(void) { }

//Reserved  97 0184h                
 

//ADA0 ADI0 98 0188h  
void  INT_Excep_ADA0_ADI0(void) { }

//Reserved  99 018Ch                
 
//Reserved  100 0190h               
 
//Reserved  101 0194h               
 

//S12ADA0 S12ADI0 102 0198h
void  INT_Excep_S12ADA0_S12ADI0(void) { }

//	S12ADA1 S12ADI1 103 019Ch
void  INT_Excep_S12ADA1_S12ADI1(void) { }

//Reserved  104 01A0h               
 
//Reserved  105 01A4h               
 


//CMPI 106 01A8h
void  INT_Excep_CMPI(void) { }

//Reserved  107 01ACh               
 
//Reserved  108 01B0h               

//Reserved  109 01B4h               

//Reserved  110 01B8h               

//Reserved  111 01BCh               

//Reserved  112 01C0h               

//Reserved  113 01C4h               


//MTU3_0 TGIA0 114 01C8h  
void  INT_Excep_MTU3_0_TGIA0(void) { }   
//MTU3_0TGIB0 115 01CCh
void  INT_Excep_MTU3_0_TGIB0(void) { }            
//MTU3_0TGIC0 116 01D0h
void  INT_Excep_MTU3_0_TGIC0(void) { }           
//MTU3_0 TGID0 117 01D4h
void  INT_Excep_MTU3_0_TGID0(void) { }           
//MTU3_0 TCIV0 118 01D8h
void  INT_Excep_MTU3_0_TCIV0(void) { }           
//MTU3_0 TGIE0 119 01DCh
void  INT_Excep_MTU3_0_TGIE0(void) { }            
//MTU3_0 TGIF0 120 01E0h 
void  INT_Excep_MTU3_0_TGIF0(void) { }  
//MTU3_1 TGIA1 121 01E4h 
void  INT_Excep_MTU3_1_TGIA1(void) { }  
//MTU3_1 TGIB1 122 01E8h
void  INT_Excep_MTU3_1_TGIB1(void) { }          
//MTU3_1_TCIV1 123 01ECh
void  INT_Excep_MTU3_1_TCIV1(void) { }     
//MTU3_1_TCIU1 124 01F0h 
void  INT_Excep_MTU3_1_TCIU1(void) { }    


//MTU3_2_TGIA2 125 01F4h 
void  INT_Excep_MTU3_2_TGIA2(void) { }  
//MTU3_2_TGIB2 126 01F8h
void  INT_Excep_MTU3_2_TGIB2(void) { }          
//MTU3_2_TCIV2 127 01FCh          
void  INT_Excep_MTU3_2_TCIV2(void) { }
//MTU3_2_TCIU2 128 0200h          
void  INT_Excep_MTU3_2_TCIU2(void) { }
//IPR56


//MTU3_3_TGIA3 129 0204h
void  INT_Excep_MTU3_3_TGIA3(void) { }   
//MTU3_3_TGIB3 130 0208h
void  INT_Excep_MTU3_3_TGIB3(void) { }

//MTU3_3_TGIC3 131 020Ch          
void  INT_Excep_MTU3_3_TGIC3(void) { }
//MTU3_3_TGID3 132 0210h          
void  INT_Excep_MTU3_3_TGID3(void) { }
//MTU3_3_TCIV3 133 0214h
void  INT_Excep_MTU3_3_TCIV3 (void) { }


//MTU3_4_TGIA4 134 0218h   
void  INT_Excep_MTU3_4_TGIA4 (void) { }
//MTU3_4_TGIB4 135 021Ch          
void  INT_Excep_MTU3_4_TGIB4 (void) { }
//MTU3_4_TGIC4 136 0220h
void  INT_Excep_MTU3_4_TGIC4 (void) { }          
//MTU3_4_TGID4 137 0224h 
void  INT_Excep_MTU3_4_TGID4(void) { }         
//MTU3_4_TCIV4 138 0228h          
void  INT_Excep_MTU3_4_TCIV4(void) { }


//MTU3_5 TGIU5 139 022Ch  
void  INT_Excep_MTU3_5_TGIU5 (void) { } 
//MTU3_5 TGIV5 140 0230h 
void  INT_Excep_MTU3_5_TGIV5(void) { }	
//MTU3_5 TGIW5 141 0234h
void  INT_Excep_MTU3_5_TGIW5(void) { }          



//MTU3_6 TGIA6 142 0238h  
void  INT_Excep_MTU3_6_TGIA6(void) { } 
//MTU3_6 TGIB6 143 023Ch
void  INT_Excep_MTU3_6_TGIB6(void) { }

//MTU3_6 TGIC6 144 0240h 
void  INT_Excep_MTU3_6_TGIC6 (void) { }         
//MTU3_6 TGID6 145 0244h
void  INT_Excep_MTU3_6_TGID6 (void) { }          
//MTU3_6 TCIV6 146 0248h
void  INT_Excep_MTU3_6_TCIV6(void) { }          


//Reserved  147 024Ch               

//Reserved  148 0250h               





//MTU3_7 TGIA7 149 0254h
void  INT_Excep_MTU3_7_TGIA7 (void) { }   
//MTU3_7 TGIB7 150 0258h
void  INT_Excep_MTU3_7_TGIB7(void) { }          
//MTU3_7 TGIC7 151 025Ch
void  INT_Excep_MTU3_7_TGIC7(void) { }          
//MTU3_7 TGID7 152 0260h
void  INT_Excep_MTU3_7_TGID7(void) { }          
//MTU3_7 TCIV7 153 0264h
void  INT_Excep_MTU3_7_TCIV7(void) { }          


//Reserved  154 0268h               

//Reserved  155 026Ch               

//Reserved  156 0270h               

//Reserved  157 0274h               

//Reserved  158 0278h               

//Reserved  159 027Ch               

//Reserved  160 0280h               

//Reserved  161 0284h               

//Reserved  162 0288h               

//Reserved  163 028Ch               

//Reserved  164 0290h               

//Reserved  165 0294h               

//Reserved  166 0298h               

//Reserved  167 029Ch               

//Reserved  168 02A0h               

//Reserved  169 02A4h               



//POE3 OE11 170 02A8h 
void  INT_Excep_POE3_OE11(void) { }     
//POE3 OE12 171 02ACh 
void  INT_Excep_POE3_OE12(void) { }          
//POE3 OE13 172 02B0h 
void  INT_Excep_POE3_OE13(void) { }          
//POE3 OE14 173 02B4h 
void  INT_Excep_POE3_OE14(void) { }          


//IPR67


//GPT0_GTCIA0 174 02B8h
void  INT_Excep_GPT0_GTCIA0(void) { }    
//GPT0_GTCIB0 175 02BCh
void  INT_Excep_GPT0_GTCIB0(void) { }         
//GPT0_GTCIC0 176 02C0h
void  INT_Excep_GPT0_GTCIC0(void) { }         
//GPT0_GTCIE0 177 02C4h
void  INT_Excep_GPT0_GTCIE0(void) { }         
//GPT0_GTCIV0 178 02C8h
void  INT_Excep_GPT0_GTCIV0(void) { }         
//GPT0_LOCO1 179 02CCh
void  INT_Excep_GPT0_LOCO1(void) { }          



//GPT1_GTCIA1 180 02D0h  
void  INT_Excep_GPT1_GTCIA1(void) { }  
//GPT1_GTCIB1 181 02D4h
void  INT_Excep_GPT1_GTCIB1(void) { }         
//GPT1_GTCIC1 182 02D8h
void  INT_Excep_GPT1_GTCIC1(void) { }         
//GPT1_GTCIE1 183 02DCh
void  INT_Excep_GPT1_GTCIE1(void) { }         
//GPT1_GTCIV1 184 02E0h
void  INT_Excep_GPT1_GTCIV1(void) { }         



//Reserved  185 02E4h       






//GPT2_GTCIA2 186 02E8h   
void  INT_Excep_GPT2_GTCIA2(void) { } 
//GPT2_GTCIB2 187 02ECh
void  INT_Excep_GPT2_GTCIB2(void) { }         
//GPT2_GTCIC2 188 02F0h
void  INT_Excep_GPT2_GTCIC2(void) { }         
//GPT2_GTCIE2 189 02F4h
void  INT_Excep_GPT2_GTCIE2(void) { }         
//GPT2_GTCIV2 190 02F8h
void  INT_Excep_GPT2_GTCIV2(void) { }         

//Reserved  191 02FCh    



//IPR6D


//GPT3_GTCIA3 192 0300h
void  INT_Excep_GPT3_GTCIA3(void) { }    
//GPT3_GTCIB3 193 0304h
void  INT_Excep_GPT3_GTCIB3(void) { }         
//GPT3_GTCIC3 194 0308h 
void  INT_Excep_GPT3_GTCIC3(void) { }        
//GPT3_GTCIE3 195 030Ch
void  INT_Excep_GPT3_GTCIE3(void) { }         
//GPT3_GTCIV3 196 0310h
void  INT_Excep_GPT3_GTCIV3(void) { }         


//Reserved  197 0314h                     

//Reserved  198 0318h                     

//Reserved  199 031Ch                     

//Reserved  200 0320h                     

//Reserved  201 0324h                     

//Reserved  202 0328h                     

//Reserved  203 032Ch                     

//Reserved  204 0330h                     

//Reserved  205 0334h                     

//Reserved  206 0338h                     

//Reserved  207 033Ch                     

//Reserved  208 0340h                     

//Reserved  209 0344h                    

//Reserved  210 0348h                    

//Reserved  211 034Ch                    

//Reserved  212 0350h                    

//Reserved  213 0354h                    



//SCI0_ERI0 214 0358h
void  INT_Excep_SCI0_ERI0(void) { }      
//SCI0_RXI0 215 035Ch 
void  INT_Excep_SCI0_RXI0(void) { }          
//SCI0_TXI0 216 0360h 
void  INT_Excep_SCI0_TXI0 (void) { }          
//SCI0_TEI0 217 0364h 
void  INT_Excep_SCI0_TEI0(void) { }          



//SCI1_ERI1 218 0368h
void  INT_Excep_SCI1_ERI1 (void) { }      
//SCI1_RXI1 219 036Ch 
void  INT_Excep_SCI1_RXI1 (void) { }          
//SCI1_TXI1 220 0370h 
void  INT_Excep_SCI1_TXI1(void) { }          
//SCI1_TEI1 221 0374h 
void  INT_Excep_SCI1_TEI1(void) { }          



//SCI2_ERI2 222 0378h 
void  INT_Excep_SCI2_ERI2(void) { }     
//SCI2_RXI2 223 037Ch 
void  INT_Excep_SCI2_RXI2(void) { }          
//SCI2_TXI2 224 0380h 
void  INT_Excep_SCI2_TXI2(void) { }          
//SCI2_TEI2 225 0384h 
void  INT_Excep_SCI2_TEI2(void) { }          

//Reserved  226 0388h                    

//Reserved  227 038Ch                    

//Reserved  228 0390h                    

//Reserved  229 0394h                    

//Reserved  230 0398h                    

//Reserved  231 039Ch                    

//Reserved  232 03A0h                    

//Reserved  233 03A4h                    

//Reserved  234 03A8h                    

//Reserved  235 03ACh                    

//Reserved  236 03B0h                    

//Reserved  237 03B4h                    

//Reserved  238 03B8h                    

//Reserved  239 03BCh                    

//Reserved  240 03C0h                    

//Reserved  241 03C4h                    

//Reserved  242 03C8h                    

//Reserved  243 03CCh                    

//Reserved  244 03D0h                    

//Reserved  245 03D4h                    


//RIIC0_ICEEI0 246 03D8h
void  INT_Excep_RIIC0_ICEEI0(void) { }   
//RIIC0_ICRXI0 247 03DCh
void  INT_Excep_RIIC0_ICRXI0(void) { }         
//RIIC0_ICTXI0 248 03E0h
void  INT_Excep_RIIC0_ICTXI0(void) { }         
//RIIC0_ICTEI0 249 03E4h
void  INT_Excep_RIIC0_ICTEI0(void) { }         



//Reserved  250 03E8h  

//Reserved  251 03ECh  

//Reserved  252 03F0h  

//Reserved  253 03F4h  



//LIN0_LIN0 254 03F8h    	
void  INT_Excep_LIN0_LIN0(void) { }
//Reserved 255 03FCh      





