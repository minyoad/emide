/************************************************************************************
 *  File:     vector_table.c
 *  Purpose:  Vector table for Hardware interrupts and relocatable interrupts for RX62T
 *  Date:     26 September 2013
 ************************************************************************************/

#include "interrupt_handlers.h"

#define FVECT_SECT          __attribute__ ((section (".fvectors")))
#define RVECT_SECT          __attribute__ ((section (".rvectors")))

typedef void (*fp) (void);

extern void Reset_Handler (void);
const fp HardwareVectors[] FVECT_SECT  = {
  (fp)0xFFFFFFFF,                // 0xffffffA0  Reserved
  (fp)0xFFFFFFFF,
  (fp)0xFFFFFFFF,
  (fp)0xFFFFFFFF,
  (fp)0,                         // 0xffffffB0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0xffffffC0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  INT_Excep_SuperVisorInst,      // 0xffffffD0  Exception(Supervisor Instruction)
  (fp)0,                         // 0xffffffd4  Reserved
  (fp)0,
  INT_Excep_UndefinedInst,       // 0xffffffdc  Exception(Undefined Instruction)
  (fp)0,                         // 0xffffffe0  Reserved
  INT_Excep_FloatingPoint,       // 0xffffffe4  Exception(Floating Point)
  (fp)0,                         // 0xffffffe8  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  INT_NonMaskableInterrupt,      // 0xfffffff8  NMI
  Reset_Handler                  // 0xfffffffc  RESET
};

const fp RelocatableVectors[] RVECT_SECT  = {
  (fp)0,                         // 0x0000  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_BUSERR,          // 0x0040  BUSERR
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_FCUIF_FIFERR,    // 0x0054  FIFERR
  (fp)0,
  (fp)INT_Excep_FCUIF_FRDYI,     // 0x005C  FRDYI
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_ICU_SWINT,       // 0x006C  ICU_SWINT
  (fp)INT_Excep_CMT0_CMI0,       // 0x0070  CMTU0_CMT0
  (fp)INT_Excep_CMT0_CMI1,       // 0x0074  CMTU0_CMT
  (fp)INT_Excep_CMT1_CMI2,       // 0x0078  CMTU1_CMT2
  (fp)INT_Excep_CMT1_CMI3,       // 0x007C  CMTU1_CMT3
  (fp)0,                         // 0x0080  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0090  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x00A0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_RSPIO_SPEI0,     // 0x00B0  RSPIO_SPEI0
  (fp)INT_Excep_RSPIO_SPRI0,     // 0x00B4  RSPIO_SPEI0
  (fp)INT_Excep_RSPIO_SPRI0,     // 0x00B8  RSPIO_SPEI0
  (fp)0,
  (fp)0,                         // 0x00C0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x00D0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_CAN0_ERS0,       // 0x00E0  CAN0_ERS0
  (fp)INT_Excep_CAN0_RXF0,       // 0x00E4  CAN0_RXF0
  (fp)INT_Excep_CAN0_TXF0,       // 0x00E8  CAN0_TXF0
  (fp)INT_Excep_CAN0_RXM0,       // 0x00EC  CAN0_RXM0
  (fp)INT_Excep_CAN0_TXM0,       // 0x00F0  CAN0_TXM0
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_IRQ0,            // 0x0100  IRQ0
  (fp)INT_Excep_IRQ1,            // 0x0104  IRQ1
  (fp)INT_Excep_IRQ2,            // 0x0108  IRQ2
  (fp)INT_Excep_IRQ3,            // 0x010C  IRQ3
  (fp)INT_Excep_IRQ4,            // 0x0110  IRQ4
  (fp)INT_Excep_IRQ5,            // 0x0114  IRQ5
  (fp)INT_Excep_IRQ6,            // 0x0118  IRQ6
  (fp)INT_Excep_IRQ7,            // 0x011C  IRQ7
  (fp)0,                         // 0x0120  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0130  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0140  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0150  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0160  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0170  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)INT_Excep_WDT_WOVI,        // 0x0180  WDT_WOVI
  (fp)0,
  (fp)INT_Excep_ADA0_ADI0,       // 0x0188  AD0_ADI0
  (fp)0,
  (fp)0,                         // 0x0190  Reserved
  (fp)0,
  (fp)INT_Excep_S12ADA0_S12ADI0, // 0x0198  S12ADA0_S12ADI0
  (fp)INT_Excep_S12ADA1_S12ADI1, // 0x019C  S12ADA1_S12ADI1
  (fp)0,                         // 0x01A0  Reserved
  (fp)0,
  (fp)INT_Excep_CMPI,            // 0x01A8  CMPI
  (fp)0,
  (fp)0,                         // 0x01B0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x01C0  Reserved
  (fp)0,
  (fp)INT_Excep_MTU3_0_TGIA0,    // 0x01C8  MTU3_0_TGIA0
  (fp)INT_Excep_MTU3_0_TGIB0,    // 0x01CC  MTU3_0_TGIB0
  (fp)INT_Excep_MTU3_0_TGIC0,    // 0x01D0  MTU3_0_TGIC0
  (fp)INT_Excep_MTU3_0_TGID0,    // 0x01D4  MTU3_0_TGID0
  (fp)INT_Excep_MTU3_0_TCIV0,    // 0x01D8  MTU3_0_TCIV0
  (fp)INT_Excep_MTU3_0_TGIE0,    // 0x01DC  MTU3_0_TGIE0
  (fp)INT_Excep_MTU3_0_TGIF0,    // 0x01E0  MTU3_0_TGIF0
  (fp)INT_Excep_MTU3_1_TGIA1,    // 0x01E4  MTU3_1_TGIA1
  (fp)INT_Excep_MTU3_1_TGIB1,    // 0x01E8  MTU3_1_TGIB1
  (fp)INT_Excep_MTU3_1_TCIV1,    // 0x01EC  MTU3_1_TCIV1
  (fp)INT_Excep_MTU3_1_TCIU1,    // 0x01F0  MTU3_1_TGIU1
  (fp)INT_Excep_MTU3_2_TGIA2,    // 0x01F4  MTU3_2_TGIA2
  (fp)INT_Excep_MTU3_2_TGIB2,    // 0x01F8  MTU3_2_TGIB2
  (fp)INT_Excep_MTU3_2_TCIV2,    // 0x01FC  MTU3_2_TCIV2
  (fp)INT_Excep_MTU3_2_TCIU2,    // 0x0200  MTU3_2_TGIU2
  (fp)INT_Excep_MTU3_3_TGIA3,    // 0x0204  MTU3_3_TGIA3
  (fp)INT_Excep_MTU3_3_TGIB3,    // 0x0208  MTU3_3_TGIB3
  (fp)INT_Excep_MTU3_3_TGIC3,    // 0x020C  MTU3_3_TGIC3
  (fp)INT_Excep_MTU3_3_TGID3,    // 0x0210  MTU3_3_TGID3
  (fp)INT_Excep_MTU3_3_TCIV3,    // 0x0214  MTU3_3_TCIV3
  (fp)INT_Excep_MTU3_4_TGIA4,    // 0x0218  MTU3_4_TGIA4
  (fp)INT_Excep_MTU3_4_TGIB4,    // 0x021C  MTU3_4_TGIB4
  (fp)INT_Excep_MTU3_4_TGIC4,    // 0x0220  MTU3_4_TGIC4
  (fp)INT_Excep_MTU3_4_TGID4,    // 0x0224  MTU3_4_TGID4
  (fp)INT_Excep_MTU3_4_TCIV4,    // 0x0228  MTU3_4_TCIV4
  (fp)INT_Excep_MTU3_5_TGIU5,    // 0x022C  MTU3_5_TGIU5
  (fp)INT_Excep_MTU3_5_TGIV5,    // 0x0230  MTU3_5_TGIV5
  (fp)INT_Excep_MTU3_5_TGIW5,    // 0x0234  MTU3_5_TGIW5
  (fp)INT_Excep_MTU3_6_TGIA6,    // 0x0238  MTU3_6_TGIA6
  (fp)INT_Excep_MTU3_6_TGIB6,    // 0x023C  MTU3_6_TGIB6
  (fp)INT_Excep_MTU3_6_TGIC6,    // 0x0240  MTU3_6_TGIC6
  (fp)INT_Excep_MTU3_6_TGID6,    // 0x0244  MTU3_6_TGID6
  (fp)INT_Excep_MTU3_6_TCIV6,    // 0x0248  MTU3_6_TCIV6
  (fp)0,
  (fp)0,                         // 0x0250  Reserved
  (fp)INT_Excep_MTU3_7_TGIA7,    // 0x0254  MTU3_7_TGIA7
  (fp)INT_Excep_MTU3_7_TGIB7,    // 0x0258  MTU3_7_TGIB7
  (fp)INT_Excep_MTU3_7_TGIC7,    // 0x025C  MTU3_7_TGIC7
  (fp)INT_Excep_MTU3_7_TGID7,    // 0x0260  MTU3_7_TGID7
  (fp)INT_Excep_MTU3_7_TCIV7,    // 0x0264  MTU3_7_TCIV7
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0270  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0280  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0290  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x02A0  Reserved
  (fp)0,
  (fp)INT_Excep_POE3_OE11,       // 0x02A8  POE3_OE11
  (fp)INT_Excep_POE3_OE12,       // 0x02AC  POE3_OE12
  (fp)INT_Excep_POE3_OE13,       // 0x02AC  POE3_OE13
  (fp)INT_Excep_POE3_OE14,       // 0x02AC  POE3_OE14

  (fp)INT_Excep_GPT0_GTCIA0,     // 0x02B8  GPT0_GTCIA0
  (fp)INT_Excep_GPT0_GTCIB0,     // 0x02BC  GPT0_GTCIB0
  (fp)INT_Excep_GPT0_GTCIC0,     // 0x02C0  GPT0_GTCIC0
  (fp)INT_Excep_GPT0_GTCIE0,     // 0x02C4  GPT0_GTCIE0
  (fp)INT_Excep_GPT0_GTCIV0,     // 0x02C8  GPT0_GTCIV0
  (fp)INT_Excep_GPT0_LOCO1,      // 0x02CC  GPT0_LOCO1
  (fp)INT_Excep_GPT1_GTCIA1,     // 0x02D0  GPT1_GTCIA1
  (fp)INT_Excep_GPT1_GTCIB1,     // 0x02D4  GPT1_GTCIB1
  (fp)INT_Excep_GPT1_GTCIC1,     // 0x02D8  GPT1_GTCIC1
  (fp)INT_Excep_GPT1_GTCIE1,     // 0x02DC  GPT1_GTCIE1
  (fp)INT_Excep_GPT1_GTCIV1,     // 0x02E0  GPT1_GTCIV1
  (fp)0,
  (fp)INT_Excep_GPT2_GTCIA2,     // 0x02E8  GPT2_GTCIA2
  (fp)INT_Excep_GPT2_GTCIB2,     // 0x02EC  GPT2_GTCIB2
  (fp)INT_Excep_GPT2_GTCIC2,     // 0x02F0  GPT2_GTCIC2
  (fp)INT_Excep_GPT2_GTCIE2,     // 0x02F4  GPT2_GTCIE2
  (fp)INT_Excep_GPT2_GTCIV2,     // 0x02F8  GPT2_GTCIV2
  (fp)0,
  (fp)INT_Excep_GPT3_GTCIA3,     // 0x0300  GPT3_GTCIA3
  (fp)INT_Excep_GPT3_GTCIB3,     // 0x0304  GPT3_GTCIB3
  (fp)INT_Excep_GPT3_GTCIC3,     // 0x0308  GPT3_GTCIC3
  (fp)INT_Excep_GPT3_GTCIE3,     // 0x030C  GPT3_GTCIE3
  (fp)INT_Excep_GPT3_GTCIV3,     // 0x0310  GPT3_GTCIV3
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0320  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0330  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0340  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0350  Reserved
  (fp)0,
  (fp)INT_Excep_SCI0_ERI0,       // 0x0358  SCI0_ERI0
  (fp)INT_Excep_SCI0_RXI0,       // 0x035C  SCI0_RXI0
  (fp)INT_Excep_SCI0_TXI0,       // 0x0360  SCI0_TXI0
  (fp)INT_Excep_SCI0_TEI0,       // 0x0364  SCI0_TEI0
  (fp)INT_Excep_SCI1_ERI1,       // 0x0368  SCI1_ERI1
  (fp)INT_Excep_SCI1_RXI1,       // 0x036C  SCI1_RXI1
  (fp)INT_Excep_SCI1_TXI1,       // 0x0370  SCI1_TXI1
  (fp)INT_Excep_SCI1_TEI1,       // 0x0374  SCI1_TEI1
  (fp)INT_Excep_SCI2_ERI2,       // 0x0378  SCI2_ERI2
  (fp)INT_Excep_SCI2_RXI2,       // 0x037C  SCI2_RXI2
  (fp)INT_Excep_SCI2_TXI2,       // 0x0380  SCI2_TXI2
  (fp)INT_Excep_SCI2_TEI2,       // 0x0384  SCI2_TEI2
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x0390  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x03A0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x03B0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x03C0  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x03D0  Reserved
  (fp)0,
  (fp)INT_Excep_RIIC0_ICEEI0,    // 0x03D8  RIIC0_ICEEI0
  (fp)INT_Excep_RIIC0_ICRXI0,    // 0x03DC  RIIC0_ICRXI0
  (fp)INT_Excep_RIIC0_ICTXI0,    // 0x03E0  RIIC0_ICTXI0
  (fp)INT_Excep_RIIC0_ICTEI0,    // 0x03E4  RIIC0_ICTEI0
  (fp)0,
  (fp)0,
  (fp)0,                         // 0x03F0  Reserved
  (fp)0,
  (fp)INT_Excep_LIN0_LIN0,       // 0x03F4  LIN0_LIN0
  (fp)0
};
