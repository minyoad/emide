/************************************************************************************
 *  File:     interrupt_handlers.c
 *  Purpose:  Interrupt handlers for RX63N
 *  Date:     26 September 2013
 ************************************************************************************/
 
#ifndef INTERRUPT_HANDLERS_H
#define INTERRUPT_HANDLERS_H
// Exception(Supervisor Instruction)
void INT_Excep_SuperVisorInst(void)__attribute__ ((interrupt));

// Exception(Undefined Instruction)
void INT_Excep_UndefinedInst(void)__attribute__ ((interrupt));

// Exception(Floating Point)
void INT_Excep_FloatingPoint(void)__attribute__ ((interrupt));

// NMI
void INT_NonMaskableInterrupt(void)__attribute__ ((interrupt));

// Dummy
void Dummy(void)__attribute__ ((interrupt));

// BRK
void INT_Excep_BRK(void)__attribute__ ((interrupt));

//Reserved  0 0000h
	                               
//Reserved  1 0004h 
	                                 
//Reserved  2 0008h                                   
 
//Reserved  3 000Ch                                   
 
//Reserved  4 0010h                                   
 
//Reserved  5 0014h                                   
 
//Reserved  6 0018h                                   
 
//Reserved  7 001Ch                                   
 
//Reserved  8 0020h                                   
 
//Reserved  9 0024h                                   
 
//Reserved  10 0028h                                   
 
//Reserved  11 002Ch                                   
 
//Reserved  12 0030h                                   
 
//Reserved  13 0034h                                   
 
//Reserved  14 0038h                                   
 
//Reserved  15 003Ch                                   
 

//Bus error BUSERR 16 0040h   
void  INT_Excep_BUSERR(void) __attribute__ ((interrupt));
//Reserved  17 0044h   
 
//Reserved  18 0048h   
 
//Reserved  19 004Ch   
 
//Reserved  20 0050h   
 

//FCU FCUERR 21 0054h  
void  INT_Excep_FCU_FCUERR(void) __attribute__ ((interrupt));

//Reserved  22 0058h    
 

//FCU FRDYI 23 005Ch  
void  INT_Excep_FCU_FRDYI(void) __attribute__ ((interrupt));
//Reserved  24 0060h  
 
//Reserved  25 0064h  
 
//Reserved  26 0068h                         
//ICU SWINT 27 006Ch 
void  INT_Excep_ICU_SWINT(void) __attribute__ ((interrupt));
//CMT0 CMI0 28 0070h 
void  INT_Excep_CMT0_CMI0(void) __attribute__ ((interrupt));
//CMT1 CMI1 29 0074h      
void  INT_Excep_CMT1_CMI1(void) __attribute__ ((interrupt));
//CMT2 CMI2 30 0078h 
void  INT_Excep_CMT2_CMI2(void) __attribute__ ((interrupt));
//CMT3 CMI3 31 007Ch      
void  INT_Excep_CMT3_CMI3 (void) __attribute__ ((interrupt));
//Reserved      32 0080h             
void INT_Excep_EINT (void) __attribute__ ((interrupt));
//USB0_D0FIFO0 33 0084h 
void  INT_Excep_USB0_D0FIFO0 (void) __attribute__ ((interrupt));
//USB0_D1FIFO0 34 0088h 
void  INT_Excep_USB0_D1FIFO0 (void) __attribute__ ((interrupt));
//USB0_USBIO 35 008Ch 
void  INT_Excep_USB0_USBI0 (void) __attribute__ ((interrupt));
//Reserved  36 0090h                      
 
//Reserved  37 0094h                      
 
//Reserved  38 0098h                      
 
//RSPI0_SPRI0 39 009Ch 
void  INT_Excep_RSPI0_SPRI0 (void) __attribute__ ((interrupt));
//RSPI0_SPTI0 40 00A0h 
void  INT_Excep_RSPI0_SPTI0 (void) __attribute__ ((interrupt));
//RSPI0_SPII0 41 00A4h 
void  INT_Excep_RSPI0_SPII0 (void) __attribute__ ((interrupt));
//RSPI1_SPRI1 42 00A8h 
void  INT_Excep_RSPI1_SPRI1 (void) __attribute__ ((interrupt));
//RSPI1_SPTI1 43 00ACh 
void  INT_Excep_RSPI1_SPTI1 (void) __attribute__ ((interrupt));
//RSPI1_SPII1 44 00B0h 
void  INT_Excep_RSPI1_SPII1 (void) __attribute__ ((interrupt));
//RSPI2_SPRI2 45 00B4h
void  INT_Excep_RSPI2_SPRI2(void) __attribute__ ((interrupt));	 
//RSPI2_SPTI2 46 00B8h 
void  INT_Excep_RSPI2_SPTI2(void) __attribute__ ((interrupt));	 
//RSPI2_SPII2 47 00BCh 
void  INT_Excep_RSPI2_SPII2(void) __attribute__ ((interrupt));	 
//CAN0_RXF0 48 00C0h              
void  INT_Excep_CAN0_RXF0(void) __attribute__ ((interrupt));	 
//CAN0_TXF0 49 00C4h              
void  INT_Excep_CAN0_TXF0(void) __attribute__ ((interrupt));	
//CAN0_RXM0 50 00C8h         
void  INT_Excep_CAN0_RXM0(void) __attribute__ ((interrupt));	
//CAN0_TXM0 51 00CCh         
void  INT_Excep_CAN0_TXM0(void) __attribute__ ((interrupt));	
//CAN1_RXF1 52 00D0h 
void  INT_Excep_CAN1_RXF1(void) __attribute__ ((interrupt));	
//CAN1_TXF1 53 00D4h 
void  INT_Excep_CAN1_TXF1(void) __attribute__ ((interrupt));	 
//CAN1_RXM1 54 00E0h 
void  INT_Excep_CAN1_RXM1(void) __attribute__ ((interrupt));	 
//CAN1_TXM1 55 00E0h 
void  INT_Excep_CAN1_TXM1(void) __attribute__ ((interrupt));	 
	
//CAN2_RXF2 56 00E0h 
void  INT_Excep_CAN2_RXF2(void) __attribute__ ((interrupt));
//CAN2_TXF2 57 00E4h      
void  INT_Excep_CAN2_TXF2(void) __attribute__ ((interrupt));
//CAN2_RXM2 58 00E8h
void  INT_Excep_CAN2_RXM2(void) __attribute__ ((interrupt));
//CAN2_TXM2  59 00ECh
void  INT_Excep_CAN2_TXM2(void) __attribute__ ((interrupt));

//Reserved  60 00F0h                     

//Reserved  61 00F4h                     

//RTC_COUNTUP  62 00F8h                     
 void  INT_Excep_RTC_COUNTUP(void) __attribute__ ((interrupt));
//Reserved  63 00FCh                      
	
//IRQ0 64 0100h  
void  INT_Excep_IRQ0(void) __attribute__ ((interrupt));
//IRQ1 65 0104h  
void  INT_Excep_IRQ1(void) __attribute__ ((interrupt));
//IRQ2 66 0108h  
void  INT_Excep_IRQ2(void) __attribute__ ((interrupt));
//IRQ3 67 010Ch  
void  INT_Excep_IRQ3(void) __attribute__ ((interrupt));
//IRQ4 68 0110h  
void  INT_Excep_IRQ4(void) __attribute__ ((interrupt));
//IRQ5 69 0114h  
void  INT_Excep_IRQ5(void) __attribute__ ((interrupt));
//IRQ6 70 0118h  
void  INT_Excep_IRQ6(void) __attribute__ ((interrupt));
//IRQ7 71 011Ch  
void  INT_Excep_IRQ7(void) __attribute__ ((interrupt));
//IRQ8  72 0120h                     
 void  INT_Excep_IRQ8(void) __attribute__ ((interrupt));
//IRQ9  73 0124h                     
 void  INT_Excep_IRQ9(void) __attribute__ ((interrupt));
//IRQ10  74 0128h                     
 void  INT_Excep_IRQ10(void) __attribute__ ((interrupt));
//IRQ11  75 012Ch                     
void  INT_Excep_IRQ11(void) __attribute__ ((interrupt)); 
//IRQ12  76 0130h                     
 void  INT_Excep_IRQ12(void) __attribute__ ((interrupt));
//IRQ13  77 0134h                     
 void  INT_Excep_IRQ13(void) __attribute__ ((interrupt));
//IRQ14  78 0138h                     
 void  INT_Excep_IRQ14(void) __attribute__ ((interrupt));
//IRQ15  79 013Ch                     
void  INT_Excep_IRQ15(void) __attribute__ ((interrupt)); 
//Reserved   80 0140h                     
 
//Reserved  81 0144h                     
 
//Reserved  82 0148h                     
 
//Reserved  83 014Ch                     
 
//Reserved  84 0150h                     
 
//Reserved  85 0154h                     
 
//Reserved  86 0158h               
 
//Reserved  87 015Ch                     
 

//Reserved  88 0160h                
 
//Reserved  89 0164h                
 
//Reserved  90 0168h                
void  INT_Excep_USB_USBR0(void) __attribute__ ((interrupt));  
//Reserved  91 016Ch                
 
//RTC_ALARM  92 0170h                
void  INT_Excep_RTC_ALARM(void) __attribute__ ((interrupt)); 
//Reserved  93 0174h                
 
//Reserved  94 0178h                
 
//Reserved  95 017Ch                
 
//Reserved 96 0180h   
//Reserved  97 0184h                
 

//AD0 ADI0 98 0188h  
void  INT_Excep_AD0_ADI0(void) __attribute__ ((interrupt));

//Reserved  99 018Ch                
 
//Reserved  100 0190h               
 
//Reserved  101 0194h               
 
//S12AD0 S12ADI0 102 0198h
void  INT_Excep_S12AD0_S12ADI0(void) __attribute__ ((interrupt));

//Reserved 103 019Ch

//Reserved  104 01A0h               
 
//Reserved  105 01A4h               
 


//GROUP_EDGE0 106 01A8h
void  INT_Excep_GROUP_EDGE0(void) __attribute__ ((interrupt));

//GROUP_EDGE1  107 01ACh               
void  INT_Excep_GROUP_EDGE1(void) __attribute__ ((interrupt));
//GROUP_EDGE2  108 01B0h               
void  INT_Excep_GROUP_EDGE2(void) __attribute__ ((interrupt));
//GROUP_EDGE3  109 01B4h               
void  INT_Excep_GROUP_EDGE3(void) __attribute__ ((interrupt));
//GROUP_EDGE4  110 01B8h               
void  INT_Excep_GROUP_EDGE4(void) __attribute__ ((interrupt));
//GROUP_EDGE5  111 01BCh               
void  INT_Excep_GROUP_EDGE5(void) __attribute__ ((interrupt));
//GROUP_EDGE6  112 01C0h               
void  INT_Excep_GROUP_EDGE6(void) __attribute__ ((interrupt));
//Reserved  113 01C4h               
 

//GROUP_EDGE_IO 114 01C8h  
void  INT_Excep_GROUP_EDGE_IO(void) __attribute__ ((interrupt));
//Reserved 115 01CCh
//Reserved 116 01D0h
//Reserved 117 01D4h
//Reserved 118 01D8h
//Reserved 119 01DCh
//Reserved 120 01E0h 
//Reserved 121 01E4h 
//SCIX_SCIX0 122 01E8h
void  INT_Excep_SCIX_SCIX0(void) __attribute__ ((interrupt));
//SCIX_SCIX1 123 01ECh
void  INT_Excep_SCIX_SCIX1(void) __attribute__ ((interrupt));     
//SCIX_SCIX2 124 01F0h 
void  INT_Excep_SCIX_SCIX2(void) __attribute__ ((interrupt));    
//SCIX_SCIX3 125 01F4h 
void  INT_Excep_SCIX_SCIX3(void) __attribute__ ((interrupt));  
//TPU0_TGIA0 126 01F8h
void  INT_Excep_TPU0_TGIA0(void) __attribute__ ((interrupt));          
//TPU0_TGIB0 127 01FCh          
void  INT_Excep_TPU0_TGIB0(void) __attribute__ ((interrupt));        
//TPU0_TGIC0 128 0200h          
void  INT_Excep_TPU0_TGIC0(void) __attribute__ ((interrupt));        
//TPU0_TGID0 129 0204h
void  INT_Excep_TPU0_TGID0(void) __attribute__ ((interrupt));   
//TPU1_TGIA1 130 0208h
void  INT_Excep_TPU1_TGIA1(void) __attribute__ ((interrupt));
//TPU1_TGIB1 131 020Ch          
void  INT_Excep_TPU1_TGIB1(void) __attribute__ ((interrupt));
//TPU2_TGIA2 132 0210h          
void  INT_Excep_TPU2_TGIA2(void) __attribute__ ((interrupt));
//TPU2_TGIB2 133 0214h
void  INT_Excep_TPU2_TGIB2(void) __attribute__ ((interrupt));
//TPU3_TGIA3 134 0218h   
void  INT_Excep_TPU3_TGIA3(void) __attribute__ ((interrupt));
//TPU3_TGIB3 135 021Ch          
void  INT_Excep_TPU3_TGIB3(void) __attribute__ ((interrupt));
//TPU3_TGIC3 136 0220h
void  INT_Excep_TPU3_TGIC3(void) __attribute__ ((interrupt));          
//TPU3_TGID3 137 0224h 
void  INT_Excep_TPU3_TGID3(void) __attribute__ ((interrupt));         
//TPU4_TGIA4 138 0228h          
void  INT_Excep_TPU4_TGIA4(void) __attribute__ ((interrupt));
//TPU4_TGIB4 139 022Ch          
void  INT_Excep_TPU4_TGIB4(void) __attribute__ ((interrupt));
//TPU5_TGIA5 140 0230h 
void  INT_Excep_TPU5_TGIA5(void) __attribute__ ((interrupt));	
//TPU5_TGIB5 141 0234h
void  INT_Excep_TPU5_TGIB5(void) __attribute__ ((interrupt));          

//MTU0_TGIA6 142 0238h  
void  INT_Excep_MTU0_TGIA6(void) __attribute__ ((interrupt)); 
//MTU0_TGIB6 143 023Ch
void  INT_Excep_MTU0_TGIB6(void) __attribute__ ((interrupt));
  
//MTU0_TGIC6 144 0240h 
void  INT_Excep_MTU0_TGIC6 (void) __attribute__ ((interrupt));         
//MTU0_TGID6 145 0244h
void  INT_Excep_MTU0_TGID6 (void) __attribute__ ((interrupt));          
//MTU0_TGIE0 146 0248h
void  INT_Excep_MTU0_TGIE0(void) __attribute__ ((interrupt));          
//MTU0_TGIF0  147 024Ch               
void  INT_Excep_MTU0_TGIF0(void) __attribute__ ((interrupt));          

//MTU1_TGIA7 148 0250h
void  INT_Excep_MTU1_TGIA7 (void) __attribute__ ((interrupt));   
//MTU1_TGIB7 149 0254h
void  INT_Excep_MTU1_TGIB7(void) __attribute__ ((interrupt));          
//MTU2_TGIA8 150 0258h
void  INT_Excep_MTU2_TGIA8(void) __attribute__ ((interrupt));          
//MTU2_7 TGIB8 151 025Ch
void  INT_Excep_MTU2_TGIB8(void) __attribute__ ((interrupt));          

//MTU3_TGIA9 152 0260h
void  INT_Excep_MTU3_TGIA9(void) __attribute__ ((interrupt));          
//MTU3_TCIB9 153 0264h
void  INT_Excep_MTU3_TCIB9(void) __attribute__ ((interrupt));          
//MTU3_TCIC9  154 0268h               
void  INT_Excep_MTU3_TCIC9(void) __attribute__ ((interrupt));    
//MTU3_TCID9  155 026Ch               
void  INT_Excep_MTU3_TCID9(void) __attribute__ ((interrupt));    
//MTU4_TCIA10  156 0270h               
void  INT_Excep_MTU4_TCIA10(void) __attribute__ ((interrupt));    
//MTU4_TCIB10  157 0274h               
void  INT_Excep_MTU4_TCIB10(void) __attribute__ ((interrupt));    
//MTU4_TCIC4 158 0278h               
void  INT_Excep_MTU4_TCIC4(void) __attribute__ ((interrupt));     
//MTU4_TCID4  159 027Ch               
void  INT_Excep_MTU4_TCID4(void) __attribute__ ((interrupt));     
 
//MTU4_TCIV4 160 0280h               
void  INT_Excep_MTU4_TCIV4(void) __attribute__ ((interrupt));     
 
//MTU5_TGIU5  161 0284h               
void  INT_Excep_MTU5_TGIU5(void) __attribute__ ((interrupt));  
//MTU5_TGIV5  162 0288h               
void  INT_Excep_MTU5_TGIV5(void) __attribute__ ((interrupt));  
//MTU5_TGIW5 163 028Ch               
void  INT_Excep_MTU5_TGIW5(void) __attribute__ ((interrupt));  
//MTU5_TGIA11  164 0290h               
void  INT_Excep_MTU5_TGIA11(void) __attribute__ ((interrupt));  
 
//MTU5_TGIB11  165 0294h               
void  INT_Excep_MTU5_TGIB11(void) __attribute__ ((interrupt));  
 
//POE2_OEI1 166 0298h               
void  INT_Excep_POE2_OEI1(void) __attribute__ ((interrupt));   
//POE2_OEI2  167 029Ch               
void  INT_Excep_POE2_OEI2(void) __attribute__ ((interrupt));   
//Reserved  168 02A0h               
 
//Reserved  169 02A4h               
 


//TMR0_CMIA0 170 02A8h 
void  INT_Excep_TMR0_CMIA0(void) __attribute__ ((interrupt));     
//TMR0_CMIB0 171 02ACh 
void  INT_Excep_TMR0_CMIB0(void) __attribute__ ((interrupt));          
//TMR0_OVI0 172 02B0h 
void  INT_Excep_TMR0_OVI0(void) __attribute__ ((interrupt));          
//TMR1_CMIA1 173 02B4h 
void  INT_Excep_TMR1_CMIA1(void) __attribute__ ((interrupt));          


//IPR67


//TMR1_CMIB1 174 02B8h
void  INT_Excep_TMR1_CMIB1(void) __attribute__ ((interrupt));    
//TMR1_OVI1 175 02BCh
void  INT_Excep_TMR1_OVI1(void) __attribute__ ((interrupt));         
//TMR2_CMIA2 176 02C0h
void  INT_Excep_TMR2_CMIA2(void) __attribute__ ((interrupt));         
//TMR2_CMIB2 177 02C4h
void  INT_Excep_TMR2_CMIB2(void) __attribute__ ((interrupt));         
//TMR2_OVI2 178 02C8h
void  INT_Excep_TMR2_OVI2(void) __attribute__ ((interrupt));         
//TMR3_CMIA3 179 02CCh
void  INT_Excep_TMR3_CMIA3(void) __attribute__ ((interrupt));          



//TMR3_CMIB3 180 02D0h  
void  INT_Excep_TMR3_CMIB3(void) __attribute__ ((interrupt));  
//TMR3_OVI3 181 02D4h
void  INT_Excep_TMR3_OVI3(void) __attribute__ ((interrupt));         
//RIIC0_EEI0 182 02D8h
void  INT_Excep_RIIC0_EEI0(void) __attribute__ ((interrupt));         
//RIIC0_RXI0 183 02DCh
void  INT_Excep_RIIC0_RXI0(void) __attribute__ ((interrupt));         
//RIIC0_GPT1_GTCIV1 184 02E0h
void  INT_Excep_RIIC0_TXI0(void) __attribute__ ((interrupt));         



//RIIC0_TEI0  185 02E4h       
void  INT_Excep_RIIC0_TEI0(void) __attribute__ ((interrupt));
 





//RIIC1_EEI1 186 02E8h   
void  INT_Excep_RIIC1_EEI1(void) __attribute__ ((interrupt)); 
//RIIC1_RXI1 187 02ECh
void  INT_Excep_RIIC1_RXI1(void) __attribute__ ((interrupt));         
//RIIC1_TXI1 188 02F0h
void  INT_Excep_RIIC1_TXI1(void) __attribute__ ((interrupt));         
//RIIC1_TEI1 189 02F4h
void  INT_Excep_RIIC1_TEI1(void) __attribute__ ((interrupt));         
//RIIC2_EEI2 190 02F8h
void  INT_Excep_RIIC2_EEI2(void) __attribute__ ((interrupt));         

//RIIC2_RXI2  191 02FCh    
void  INT_Excep_RIIC2_RXI2(void) __attribute__ ((interrupt));
 


//RIIC2_TXI2 192 0300h
void  INT_Excep_RIIC2_TXI2(void) __attribute__ ((interrupt));    
//RIIC2_TEI2 193 0304h
void  INT_Excep_RIIC2_TEI2(void) __attribute__ ((interrupt));         
//RIIC3_EEI3 194 0308h 
void  INT_Excep_RIIC3_EEI3(void) __attribute__ ((interrupt));        
//RIIC3_RXI3 195 030Ch
void  INT_Excep_RIIC3_RXI3(void) __attribute__ ((interrupt));         
//RIIC3_TXI3 196 0310h
void  INT_Excep_RIIC3_TXI3(void) __attribute__ ((interrupt));         


//RIIC3_TEI3  197 0314h                     
void  INT_Excep_RIIC3_TXI3(void) __attribute__ ((interrupt));  
//DMACA_DMAC0I  198 0318h  
void  INT_Excep_DMACA_DMAC0I(void) __attribute__ ((interrupt));                   
 
//DMACA_DMAC1I  199 031Ch 
void  INT_Excep_DMACA_DMAC1I(void) __attribute__ ((interrupt));                     
 
//DMACA_DMAC2I  200 0320h                     
void  INT_Excep_DMACA_DMAC2I(void) __attribute__ ((interrupt)); 
//DMACA_DMAC3I  201 0324h
void  INT_Excep_DMACA_DMAC3I(void) __attribute__ ((interrupt)); 
 
//Reserved  202 0328h                     
 
//Reserved  203 032Ch                     
 
//Reserved  204 0330h                     
 
//Reserved  205 0334h                     
 
//Reserved  206 0338h                     
 
//Reserved  207 033Ch                     
 
//Reserved  208 0340h                     
 
//Reserved  209 0344h                    
 
//Reserved  210 0348h                    
 
//Reserved  211 034Ch                    
 
//Reserved  212 0350h                    
 
//Reserved  213 0354h                    
 


//SCI0_RXI0 214 0358h
void  INT_Excep_SCI0_RXI0(void) __attribute__ ((interrupt));      
//SCI0_TXI0 215 035Ch 
void  INT_Excep_SCI0_TXI0(void) __attribute__ ((interrupt));          
//SCI0_TEI0 216 0360h 
void  INT_Excep_SCI0_TEI0 (void) __attribute__ ((interrupt));          
//SCI1_RXI1 217 0364h 
void  INT_Excep_SCI1_RXI1(void) __attribute__ ((interrupt));          



//SCI1_TXI1 218 0368h
void  INT_Excep_SCI1_TXI1 (void) __attribute__ ((interrupt));      
//SCI1_TEI1 219 036Ch 
void  INT_Excep_SCI1_TEI1 (void) __attribute__ ((interrupt));          
//SCI2_RXI2 220 0370h 
void  INT_Excep_SCI2_RXI2(void) __attribute__ ((interrupt));          
//SCI2_TXI2 221 0374h 
void  INT_Excep_SCI2_TXI2(void) __attribute__ ((interrupt));          



//SCI2_TEI2 222 0378h 
void  INT_Excep_SCI2_TEI2(void) __attribute__ ((interrupt));     
//RXI3 223 037Ch 
void  INT_Excep_SCI3_RXI3(void) __attribute__ ((interrupt));          
//TXI3 224 0380h 
void  INT_Excep_SCI3_TXI3(void) __attribute__ ((interrupt));          
//SCI3_TEI3 225 0384h 
void  INT_Excep_SCI3_TEI3(void) __attribute__ ((interrupt));          

//SCI4_RXI4  226 0388h                    
void  INT_Excep_SCI4_RXI4(void) __attribute__ ((interrupt));
//SCI4_TXI4  227 038Ch                    
void  INT_Excep_SCI4_TXI4(void) __attribute__ ((interrupt)); 
//SCI4_TEI4  228 0390h                    
void  INT_Excep_SCI4_TEI4(void) __attribute__ ((interrupt)); 
//SCI5_RXI5  229 0394h                    
void  INT_Excep_SCI5_RXI5(void) __attribute__ ((interrupt)); 
 
//SCI5_TXI5  230 0398h                    
void  INT_Excep_SCI5_TXI5(void) __attribute__ ((interrupt)); 
//SCI5_TEI5  231 039Ch                    
void  INT_Excep_SCI5_TEI5(void) __attribute__ ((interrupt)); 
//SCI6_RXI6  232 03A0h                    
void  INT_Excep_SCI6_RXI6(void) __attribute__ ((interrupt)); 
//SCI6_TXI6  233 03A4h                    
void  INT_Excep_SCI6_TXI6(void) __attribute__ ((interrupt)); 
//SCI6_TEI6  234 03A8h                    
void  INT_Excep_SCI6_TEI6(void) __attribute__ ((interrupt)); 
//SCI7_RXI7  235 03ACh                    
void  INT_Excep_SCI7_RXI7(void) __attribute__ ((interrupt)); 
//SCI7_TXI7  236 03A4h                    
void  INT_Excep_SCI7_TXI7(void) __attribute__ ((interrupt)); 
//SCI7_TEI7  237 03A8h                    
void  INT_Excep_SCI7_TEI7(void) __attribute__ ((interrupt)); 
 
//SCI8_RXI8  238 03B8h                    
void  INT_Excep_SCI8_RXI8(void) __attribute__ ((interrupt)); 
//SCI8_TXI8  239 03A4h                    
void  INT_Excep_SCI8_TXI8(void) __attribute__ ((interrupt)); 
//SCI8_TEI8  240 03A8h                    
void  INT_Excep_SCI8_TEI8(void) __attribute__ ((interrupt));  
//SCI9_RXI9  241 03C4h                    
void  INT_Excep_SCI9_RXI9(void) __attribute__ ((interrupt)); 
//SCI9_TXI9  242 03A4h                    
void  INT_Excep_SCI9_TXI9(void) __attribute__ ((interrupt)); 
//SCI9_TEI9  243 03A8h                    
void  INT_Excep_SCI9_TEI9(void) __attribute__ ((interrupt));                 
 
//SCI10_RXI10  244 03D0h                    
void  INT_Excep_SCI10_RXI10(void) __attribute__ ((interrupt)); 
//SCI10_TXI10  245 03A4h                    
void  INT_Excep_SCI10_TXI10(void) __attribute__ ((interrupt)); 
//SCI10_TEI10  246 03A8h                    
void  INT_Excep_SCI10_TEI10(void) __attribute__ ((interrupt));  

//SCI11_RXI11 247 03DCh
void  INT_Excep_SCI11_RXI11(void) __attribute__ ((interrupt)); 
//SCI11_TXI11  248 03A4h                    
void  INT_Excep_SCI11_TXI11(void) __attribute__ ((interrupt)); 
//SCI11_TEI11  249 03A8h                    
void  INT_Excep_SCI11_TEI11(void) __attribute__ ((interrupt));  
         



//SCI12_RXI12  250 03E8h  
void  INT_Excep_SCI12_RXI12(void) __attribute__ ((interrupt)); 
//SCI12_TXI12  248 03A4h                    
void  INT_Excep_SCI12_TXI12(void) __attribute__ ((interrupt)); 
//SCI12_TEI12  249 03A8h                    
void  INT_Excep_SCI12_TEI12(void) __attribute__ ((interrupt));
 
//IEBUS_IEBINT  253 03F4h  
void  INT_Excep_IEBUS_IEBINT(void) __attribute__ ((interrupt));

//Reserved 254 03F8h    	

//Reserved 255 03FCh                                                                                                             
//;<<VECTOR DATA END (POWER ON RESET)>>

#endif