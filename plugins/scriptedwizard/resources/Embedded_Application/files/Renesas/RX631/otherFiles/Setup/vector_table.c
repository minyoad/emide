/************************************************************************************
 *  File:     vector_table.c
 *  Purpose:  Option section and 
 *            Vector table for Hardware interrupts and relocatable interrupts for RX630
 *  Date:     26 September 2013
 ************************************************************************************/

#include "interrupt_handlers.h"

#define FVECT_SECT          __attribute__ ((section (".fvectors")))
#define RVECT_SECT          __attribute__ ((section (".rvectors")))
#define OPTIONS_SECT        __attribute__ ((section (".options")))

typedef void (*fp) (void);

extern void Reset_Handler (void);

#ifdef __RX_BIG_ENDIAN__                                                            // MDE register (Single Chip Mode)
  const unsigned long _MDEreg __attribute__ ((section (".options"))) = 0xFFFFFFF8;  // Big endian
#else
  const unsigned long _MDEreg __attribute__ ((section (".options"))) = 0xFFFFFFFF;  // Little endian
#endif
const unsigned long _Dummy    __attribute__ ((section (".options"))) = 0xFFFFFFFF;  // Dummy value
const unsigned long _OFS1     __attribute__ ((section (".options"))) = 0xFFFFFFFF;  // Option function select register 1 (OFS1)
const unsigned long _OFS2     __attribute__ ((section (".options"))) = 0xFFFFFFFF;  // Option function select register 2 (OFS2)

const unsigned long _IDCode0  __attribute__ ((section (".idcode")))  = 0xFFFFFFFF;  // Control code, ID Code 1-3
const unsigned long _IDCode1  __attribute__ ((section (".idcode")))  = 0xFFFFFFFF;  // ID Code 4 - 7
const unsigned long _IDCode2  __attribute__ ((section (".idcode")))  = 0xFFFFFFFF;  // ID Code 8 - 11
const unsigned long _IDCode3  __attribute__ ((section (".idcode")))  = 0xFFFFFFFF;  // ID Code 12- 15

const void *HardwareVectors[] FVECT_SECT  = {
  INT_Excep_SuperVisorInst,      // 0xffffffD0  Exception(Supervisor Instruction)
  (fp)0,                         // 0xffffffd4  Reserved
  (fp)0,
  INT_Excep_UndefinedInst,       // 0xffffffdc  Exception(Undefined Instruction)
  (fp)0,                         // 0xffffffe0  Reserved
  INT_Excep_FloatingPoint,       // 0xffffffe4  Exception(Floating Point)
  (fp)0,                         // 0xffffffe8  Reserved
  (fp)0,
  (fp)0,
  (fp)0,
  INT_NonMaskableInterrupt,      // 0xfffffff8  NMI
  Reset_Handler                  // 0xfffffffc  RESET
};

const fp RelocatableVectors[] RVECT_SECT  = {
//Reserved  0 0000h
(fp)INT_Excep_BRK,
//Reserved  1 0004h 
(fp)0,   	                                 
//Reserved  2 0008h                                   
(fp)0,    
//Reserved  3 000Ch                                   
(fp)0,    
//Reserved  4 0010h                                   
(fp)0,    
//Reserved  5 0014h                                   
(fp)0,    
//Reserved  6 0018h                                   
(fp)0,    
//Reserved  7 001Ch                                   
(fp)0,    
//Reserved  8 0020h                                   
(fp)0,    
//Reserved  9 0024h                                   
(fp)0,    
//Reserved  10 0028h                                   
(fp)0,    
//Reserved  11 002Ch                                   
(fp)0,    
//Reserved  12 0030h                                   
(fp)0,    
//Reserved  13 0034h                                   
(fp)0,    
//Reserved  14 0038h                                   
(fp)0,    
//Reserved  15 003Ch                                   
(fp)0,    
//Bus error BUSERR 16 0040h   
(fp)INT_Excep_BUSERR,
//Reserved  17 0044h   
(fp)0,    
//Reserved  18 0048h   
(fp)0,    
//Reserved  19 004Ch   
(fp)0,    
//Reserved  20 0050h   
(fp)0,    
//FCU FCUERR 21 0054h  
(fp)INT_Excep_FCU_FCUERR,
//Reserved  22 0058h    
(fp)0,    
//FCU FRDYI 23 005Ch  
(fp)INT_Excep_FCU_FRDYI,
//Reserved  24 0060h  
(fp)0,    
//Reserved  25 0064h  
(fp)0,    
//Reserved  26 0068h
(fp)0,                            
//ICU SWINT 27 006Ch 
(fp)INT_Excep_ICU_SWINT,
//CMT0 CMI0 28 0070h 
(fp)INT_Excep_CMT0_CMI0,
//CMT1 CMI1 29 0074h      
(fp)INT_Excep_CMT1_CMI1,
//CMT2 CMI2 30 0078h 
(fp)INT_Excep_CMT2_CMI2,
//CMT3 CMI3 31 007Ch      
(fp)INT_Excep_CMT3_CMI3 ,
//Reserved      32 0080h             
(fp)INT_Excep_EINT,  
//USB0_D0FIFO0 33 0084h 
(fp)INT_Excep_USB0_D0FIFO0 ,
//USB0_D1FIFO0 34 0088h 
(fp)INT_Excep_USB0_D1FIFO0 ,
//USB0_USBIO 35 008Ch 
(fp)INT_Excep_USB0_USBI0 ,
//Reserved  36 0090h                      
(fp)0,  
//Reserved  37 0094h                      
(fp)0,  
//Reserved  38 0098h                      
(fp)0,  
//RSPI0_SPRI0 39 009Ch 
(fp)INT_Excep_RSPI0_SPRI0 ,
//RSPI0_SPTI0 40 00A0h 
(fp)INT_Excep_RSPI0_SPTI0 ,
//RSPI0_SPII0 41 00A4h 
(fp)INT_Excep_RSPI0_SPII0 ,
//RSPI1_SPRI1 42 00A8h 
(fp)INT_Excep_RSPI1_SPRI1 ,
//RSPI1_SPTI1 43 00ACh 
(fp)INT_Excep_RSPI1_SPTI1 ,
//RSPI1_SPII1 44 00B0h 
(fp)INT_Excep_RSPI1_SPII1 ,
//RSPI2_SPRI2 45 00B4h
(fp)INT_Excep_RSPI2_SPRI2,	 
//RSPI2_SPTI2 46 00B8h 
(fp)INT_Excep_RSPI2_SPTI2,	 
//RSPI2_SPII2 47 00BCh 
(fp)INT_Excep_RSPI2_SPII2,	 
//CAN0_RXF0 48 00C0h              
(fp)INT_Excep_CAN0_RXF0,	 
//CAN0_TXF0 49 00C4h              
(fp)INT_Excep_CAN0_TXF0,	
//CAN0_RXM0 50 00C8h         
(fp)INT_Excep_CAN0_RXM0,	
//CAN0_TXM0 51 00CCh         
(fp)INT_Excep_CAN0_TXM0,	
//CAN1_RXF1 52 00D0h 
(fp)INT_Excep_CAN1_RXF1,	
//CAN1_TXF1 53 00D4h 
(fp)INT_Excep_CAN1_TXF1,	 
//CAN1_RXM1 54 00E0h 
(fp)INT_Excep_CAN1_RXM1, 
//CAN1_TXM1 55 00E0h 
(fp)INT_Excep_CAN1_TXM1,
//CAN2_RXF2 56 00E0h 
(fp)INT_Excep_CAN2_RXF2,
//CAN2_TXF2 57 00E4h      
(fp)INT_Excep_CAN2_TXF2,
//CAN2_RXM2 58 00E8h
(fp)INT_Excep_CAN2_RXM2,
//CAN2_TXM2  59 00ECh
(fp)INT_Excep_CAN2_TXM2,
//Reserved  60 00F0h                     
(fp)0, 
//Reserved  61 00F4h                     
(fp)0, 
//RTC_COUNTUP  62 00F8h                     
 (fp)INT_Excep_RTC_COUNTUP,
//Reserved  63 00FCh                      
(fp)0, 	
//IRQ0 64 0100h  
(fp)INT_Excep_IRQ0,
//IRQ1 65 0104h  
(fp)INT_Excep_IRQ1,
//IRQ2 66 0108h  
(fp)INT_Excep_IRQ2,
//IRQ3 67 010Ch  
(fp)INT_Excep_IRQ3,
//IRQ4 68 0110h  
(fp)INT_Excep_IRQ4,
//IRQ5 69 0114h  
(fp)INT_Excep_IRQ5,
//IRQ6 70 0118h  
(fp)INT_Excep_IRQ6,
//IRQ7 71 011Ch  
(fp)INT_Excep_IRQ7,
//IRQ8  72 0120h                     
(fp)INT_Excep_IRQ8,
//IRQ9  73 0124h                     
(fp)INT_Excep_IRQ9,
//IRQ10  74 0128h                     
(fp)INT_Excep_IRQ10,
//IRQ11  75 012Ch                     
(fp)INT_Excep_IRQ11, 
//IRQ12  76 0130h                     
(fp)INT_Excep_IRQ12,
//IRQ13  77 0134h                     
(fp)INT_Excep_IRQ13,
//IRQ14  78 0138h                     
(fp)INT_Excep_IRQ14,
//IRQ15  79 013Ch                     
(fp)INT_Excep_IRQ15, 
//Reserved   80 0140h                     
(fp)0,  
//Reserved  81 0144h                     
(fp)0,  
//Reserved  82 0148h                     
(fp)0,  
//Reserved  83 014Ch                     
(fp)0,  
//Reserved  84 0150h                     
(fp)0,  
//Reserved  85 0154h                     
(fp)0,  
//Reserved  86 0158h               
(fp)0,  
//Reserved  87 015Ch                     
(fp)0,  
//Reserved  88 0160h                
(fp)0,  
//Reserved  89 0164h                
(fp)0,  
//Reserved  90 0168h                
(fp)INT_Excep_USB_USBR0,  
//Reserved  91 016Ch                
(fp)0, 
//RTC_ALARM  92 0170h                
(fp)INT_Excep_RTC_ALARM, 
//Reserved  93 0174h                
(fp)0,   
//Reserved  94 0178h                
(fp)0,   
//Reserved  95 017Ch                
(fp)0,   
//Reserved 96 0180h   
(fp)0,  
//Reserved  97 0184h                
(fp)0,   
//AD0 ADI0 98 0188h  
(fp)INT_Excep_AD0_ADI0,
//Reserved  99 018Ch                
(fp)0,   
//Reserved  100 0190h               
(fp)0,   
//Reserved  101 0194h               
(fp)0,   
//S12AD0 S12ADI0 102 0198h
(fp)INT_Excep_S12AD0_S12ADI0,
//Reserved 103 019Ch
(fp)0,  
//Reserved  104 01A0h               
(fp)0,   
//Reserved  105 01A4h               
(fp)0,   
//GROUP_EDGE0 106 01A8h
(fp)INT_Excep_GROUP_EDGE0,
//GROUP_EDGE1  107 01ACh               
(fp)INT_Excep_GROUP_EDGE1,
//GROUP_EDGE2  108 01B0h               
(fp)INT_Excep_GROUP_EDGE2,
//GROUP_EDGE3  109 01B4h               
(fp)INT_Excep_GROUP_EDGE3,
//GROUP_EDGE4  110 01B8h               
(fp)INT_Excep_GROUP_EDGE4,
//GROUP_EDGE5  111 01BCh               
(fp)INT_Excep_GROUP_EDGE5,
//GROUP_EDGE6  112 01C0h               
(fp)INT_Excep_GROUP_EDGE6,
//Reserved  113 01C4h               
(fp)0,   
//GROUP_EDGE_IO 114 01C8h  
(fp)INT_Excep_GROUP_EDGE_IO,
//Reserved 115 01CCh
(fp)0,  
//Reserved 116 01D0h
(fp)0,  
//Reserved 117 01D4h
(fp)0,  
//Reserved 118 01D8h
(fp)0,  
//Reserved 119 01DCh
(fp)0,  
//Reserved 120 01E0h
(fp)0,   
//Reserved 121 01E4h
(fp)0,   
//SCIX_SCIX0 122 01E8h
(fp)INT_Excep_SCIX_SCIX0,
//SCIX_SCIX1 123 01ECh
(fp)INT_Excep_SCIX_SCIX1,     
//SCIX_SCIX2 124 01F0h 
(fp)INT_Excep_SCIX_SCIX2,    
//SCIX_SCIX3 125 01F4h 
(fp)INT_Excep_SCIX_SCIX3,  
//TPU0_TGIA0 126 01F8h
(fp)INT_Excep_TPU0_TGIA0,          
//TPU0_TGIB0 127 01FCh          
(fp)INT_Excep_TPU0_TGIB0,        
//TPU0_TGIC0 128 0200h          
(fp)INT_Excep_TPU0_TGIC0,        
//TPU0_TGID0 129 0204h
(fp)INT_Excep_TPU0_TGID0,   
//TPU1_TGIA1 130 0208h
(fp)INT_Excep_TPU1_TGIA1,
//TPU1_TGIB1 131 020Ch          
(fp)INT_Excep_TPU1_TGIB1,
//TPU2_TGIA2 132 0210h          
(fp)INT_Excep_TPU2_TGIA2,
//TPU2_TGIB2 133 0214h
(fp)INT_Excep_TPU2_TGIB2,
//TPU3_TGIA3 134 0218h   
(fp)INT_Excep_TPU3_TGIA3,
//TPU3_TGIB3 135 021Ch          
(fp)INT_Excep_TPU3_TGIB3,
//TPU3_TGIC3 136 0220h
(fp)INT_Excep_TPU3_TGIC3,          
//TPU3_TGID3 137 0224h 
(fp)INT_Excep_TPU3_TGID3,         
//TPU4_TGIA4 138 0228h          
(fp)INT_Excep_TPU4_TGIA4,
//TPU4_TGIB4 139 022Ch          
(fp)INT_Excep_TPU4_TGIB4,
//TPU5_TGIA5 140 0230h 
(fp)INT_Excep_TPU5_TGIA5,	
//TPU5_TGIB5 141 0234h
(fp)INT_Excep_TPU5_TGIB5,          
//MTU0_TGIA6 142 0238h  
(fp)INT_Excep_MTU0_TGIA6, 
//MTU0_TGIB6 143 023Ch
(fp)INT_Excep_MTU0_TGIB6,
//MTU0_TGIC6 144 0240h 
(fp)INT_Excep_MTU0_TGIC6 ,         
//MTU0_TGID6 145 0244h
(fp)INT_Excep_MTU0_TGID6 ,          
//MTU0_TGIE0 146 0248h
(fp)INT_Excep_MTU0_TGIE0,          
//MTU0_TGIF0  147 024Ch               
(fp)INT_Excep_MTU0_TGIF0,          
//MTU1_TGIA7 148 0250h
(fp)INT_Excep_MTU1_TGIA7 ,   
//MTU1_TGIB7 149 0254h
(fp)INT_Excep_MTU1_TGIB7,          
//MTU2_TGIA8 150 0258h
(fp)INT_Excep_MTU2_TGIA8,          
//MTU2_7 TGIB8 151 025Ch
(fp)INT_Excep_MTU2_TGIB8,          
//MTU3_TGIA9 152 0260h
(fp)INT_Excep_MTU3_TGIA9,          
//MTU3_TCIB9 153 0264h
(fp)INT_Excep_MTU3_TCIB9,          
//MTU3_TCIC9  154 0268h               
(fp)INT_Excep_MTU3_TCIC9,    
//MTU3_TCID9  155 026Ch               
(fp)INT_Excep_MTU3_TCID9,    
//MTU4_TCIA10  156 0270h               
(fp)INT_Excep_MTU4_TCIA10,    
//MTU4_TCIB10  157 0274h               
(fp)INT_Excep_MTU4_TCIB10,    
//MTU4_TCIC4 158 0278h               
(fp)INT_Excep_MTU4_TCIC4,     
//MTU4_TCID4  159 027Ch               
(fp)INT_Excep_MTU4_TCID4,     
//MTU4_TCIV4 160 0280h               
(fp)INT_Excep_MTU4_TCIV4,     
//MTU5_TGIU5  161 0284h               
(fp)INT_Excep_MTU5_TGIU5,  
//MTU5_TGIV5  162 0288h               
(fp)INT_Excep_MTU5_TGIV5,  
//MTU5_TGIW5 163 028Ch               
(fp)INT_Excep_MTU5_TGIW5,  
//MTU5_TGIA11  164 0290h               
(fp)INT_Excep_MTU5_TGIA11,  
//MTU5_TGIB11  165 0294h               
(fp)INT_Excep_MTU5_TGIB11,  
//POE2_OEI1 166 0298h               
(fp)INT_Excep_POE2_OEI1,   
//POE2_OEI2  167 029Ch               
(fp)INT_Excep_POE2_OEI2,   
//Reserved  168 02A0h               
(fp)0,   
//Reserved  169 02A4h               
(fp)0,   
//TMR0_CMIA0 170 02A8h 
(fp)INT_Excep_TMR0_CMIA0,     
//TMR0_CMIB0 171 02ACh 
(fp)INT_Excep_TMR0_CMIB0,          
//TMR0_OVI0 172 02B0h 
(fp)INT_Excep_TMR0_OVI0,          
//TMR1_CMIA1 173 02B4h 
(fp)INT_Excep_TMR1_CMIA1,
//IPR67

//TMR1_CMIB1 174 02B8h
(fp)INT_Excep_TMR1_CMIB1,    
//TMR1_OVI1 175 02BCh
(fp)INT_Excep_TMR1_OVI1,         
//TMR2_CMIA2 176 02C0h
(fp)INT_Excep_TMR2_CMIA2,         
//TMR2_CMIB2 177 02C4h
(fp)INT_Excep_TMR2_CMIB2,         
//TMR2_OVI2 178 02C8h
(fp)INT_Excep_TMR2_OVI2,         
//TMR3_CMIA3 179 02CCh
(fp)INT_Excep_TMR3_CMIA3,          
//TMR3_CMIB3 180 02D0h  
(fp)INT_Excep_TMR3_CMIB3,  
//TMR3_OVI3 181 02D4h
(fp)INT_Excep_TMR3_OVI3,         
//RIIC0_EEI0 182 02D8h
(fp)INT_Excep_RIIC0_EEI0,         
//RIIC0_RXI0 183 02DCh
(fp)INT_Excep_RIIC0_RXI0,         
//RIIC0_GPT1_GTCIV1 184 02E0h
(fp)INT_Excep_RIIC0_TXI0,         
//RIIC0_TEI0  185 02E4h       
(fp)INT_Excep_RIIC0_TEI0,
//RIIC1_EEI1 186 02E8h   
(fp)INT_Excep_RIIC1_EEI1, 
//RIIC1_RXI1 187 02ECh
(fp)INT_Excep_RIIC1_RXI1,         
//RIIC1_TXI1 188 02F0h
(fp)INT_Excep_RIIC1_TXI1,         
//RIIC1_TEI1 189 02F4h
(fp)INT_Excep_RIIC1_TEI1,         
//RIIC2_EEI2 190 02F8h
(fp)INT_Excep_RIIC2_EEI2,         
//RIIC2_RXI2  191 02FCh    
(fp)INT_Excep_RIIC2_RXI2,
//RIIC2_TXI2 192 0300h
(fp)INT_Excep_RIIC2_TXI2,    
//RIIC2_TEI2 193 0304h
(fp)INT_Excep_RIIC2_TEI2,         
//RIIC3_EEI3 194 0308h 
(fp)INT_Excep_RIIC3_EEI3,        
//RIIC3_RXI3 195 030Ch
(fp)INT_Excep_RIIC3_RXI3,         
//RIIC3_TXI3 196 0310h
(fp)INT_Excep_RIIC3_TXI3,         
//RIIC3_TEI3  197 0314h                     
(fp)INT_Excep_RIIC3_TXI3,  
//DMACA_DMAC0I  198 0318h  
(fp)INT_Excep_DMACA_DMAC0I,                   
//DMACA_DMAC1I  199 031Ch 
(fp)INT_Excep_DMACA_DMAC1I,                     
//DMACA_DMAC2I  200 0320h                     
(fp)INT_Excep_DMACA_DMAC2I, 
//DMACA_DMAC3I  201 0324h
(fp)INT_Excep_DMACA_DMAC3I, 
//Reserved  202 0328h                     
(fp)0,   
//Reserved  203 032Ch                     
(fp)0,   
//Reserved  204 0330h                     
(fp)0,   
//Reserved  205 0334h                     
(fp)0,   
//Reserved  206 0338h                     
(fp)0,   
//Reserved  207 033Ch                     
(fp)0,   
//Reserved  208 0340h                     
(fp)0,   
//Reserved  209 0344h                    
(fp)0,   
//Reserved  210 0348h                    
(fp)0,   
//Reserved  211 034Ch                    
(fp)0,   
//Reserved  212 0350h                    
(fp)0,   
//Reserved  213 0354h                    
(fp)0,   
//SCI0_RXI0 214 0358h
(fp)INT_Excep_SCI0_RXI0,      
//SCI0_TXI0 215 035Ch 
(fp)INT_Excep_SCI0_TXI0,          
//SCI0_TEI0 216 0360h 
(fp)INT_Excep_SCI0_TEI0 ,          
//SCI1_RXI1 217 0364h 
(fp)INT_Excep_SCI1_RXI1,          
//SCI1_TXI1 218 0368h
(fp)INT_Excep_SCI1_TXI1 ,      
//SCI1_TEI1 219 036Ch 
(fp)INT_Excep_SCI1_TEI1 ,          
//SCI2_RXI2 220 0370h 
(fp)INT_Excep_SCI2_RXI2,          
//SCI2_TXI2 221 0374h 
(fp)INT_Excep_SCI2_TXI2,          
//SCI2_TEI2 222 0378h 
(fp)INT_Excep_SCI2_TEI2,     
//RXI3 223 037Ch 
(fp)INT_Excep_SCI3_RXI3,          
//TXI3 224 0380h 
(fp)INT_Excep_SCI3_TXI3,          
//SCI3_TEI3 225 0384h 
(fp)INT_Excep_SCI3_TEI3,          
//SCI4_RXI4  226 0388h                    
(fp)INT_Excep_SCI4_RXI4,
//SCI4_TXI4  227 038Ch                    
(fp)INT_Excep_SCI4_TXI4, 
//SCI4_TEI4  228 0390h                    
(fp)INT_Excep_SCI4_TEI4, 
//SCI5_RXI5  229 0394h                    
(fp)INT_Excep_SCI5_RXI5, 
//SCI5_TXI5  230 0398h                    
(fp)INT_Excep_SCI5_TXI5, 
//SCI5_TEI5  231 039Ch                    
(fp)INT_Excep_SCI5_TEI5, 
//SCI6_RXI6  232 03A0h                    
(fp)INT_Excep_SCI6_RXI6, 
//SCI6_TXI6  233 03A4h                    
(fp)INT_Excep_SCI6_TXI6, 
//SCI6_TEI6  234 03A8h                    
(fp)INT_Excep_SCI6_TEI6, 
//SCI7_RXI7  235 03ACh                    
(fp)INT_Excep_SCI7_RXI7, 
//SCI7_TXI7  236 03A4h                    
(fp)INT_Excep_SCI7_TXI7, 
//SCI7_TEI7  237 03A8h                    
(fp)INT_Excep_SCI7_TEI7, 
//SCI8_RXI8  238 03B8h                    
(fp)INT_Excep_SCI8_RXI8, 
//SCI8_TXI8  239 03A4h                    
(fp)INT_Excep_SCI8_TXI8, 
//SCI8_TEI8  240 03A8h                    
(fp)INT_Excep_SCI8_TEI8,  
//SCI9_RXI9  241 03C4h                    
(fp)INT_Excep_SCI9_RXI9, 
//SCI9_TXI9  242 03A4h                    
(fp)INT_Excep_SCI9_TXI9, 
//SCI9_TEI9  243 03A8h                    
(fp)INT_Excep_SCI9_TEI9,                 
//SCI10_RXI10  244 03D0h                    
(fp)INT_Excep_SCI10_RXI10, 
//SCI10_TXI10  245 03A4h                    
(fp)INT_Excep_SCI10_TXI10, 
//SCI10_TEI10  246 03A8h                    
(fp)INT_Excep_SCI10_TEI10,  
//SCI11_RXI11 247 03DCh
(fp)INT_Excep_SCI11_RXI11, 
//SCI11_TXI11  248 03A4h                    
(fp)INT_Excep_SCI11_TXI11, 
//SCI11_TEI11  249 03A8h                    
(fp)INT_Excep_SCI11_TEI11,  
//SCI12_RXI12  250 03E8h  
(fp)INT_Excep_SCI12_RXI12, 
//SCI12_TXI12  248 03A4h                    
(fp)INT_Excep_SCI12_TXI12, 
//SCI12_TEI12  249 03A8h                    
(fp)INT_Excep_SCI12_TEI12,
//IEBUS_IEBINT  253 03F4h  
(fp)INT_Excep_IEBUS_IEBINT,
//Reserved 254 03F8h    

//Reserved 255 03FCh                                                                                                             
(fp)0,  
};