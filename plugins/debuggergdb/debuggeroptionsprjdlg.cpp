/********************************************************************
**  This file is part of emIDE
**  Contains changes to the original C::B source
**  Made by [JL]
********************************************************************/
/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 *
 * $Revision$
 * $Id$
 * $HeadURL$
 */

#include <sdk.h>
#include "debuggeroptionsprjdlg.h"
#include <wx/intl.h>
#include <wx/xrc/xmlres.h>
#include <wx/listbox.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/checkbox.h>
#include <wx/hyperlink.h>
#include <wx/filepicker.h>
#include <cbproject.h>
#include <editpathdlg.h>
#include <manager.h>
#include <globals.h>

#include "debuggergdb.h"

BEGIN_EVENT_TABLE(DebuggerOptionsProjectDlg, wxPanel)
    EVT_UPDATE_UI(-1,                             DebuggerOptionsProjectDlg::OnUpdateUI)
    //EVT_BUTTON(   XRCID("btnAdd"),                DebuggerOptionsProjectDlg::OnAdd)
    //EVT_BUTTON(   XRCID("btnEdit"),               DebuggerOptionsProjectDlg::OnEdit)
    //EVT_BUTTON(   XRCID("btnDelete"),             DebuggerOptionsProjectDlg::OnDelete)
    EVT_BUTTON(   XRCID("btnBrowseRegisterFile"), DebuggerOptionsProjectDlg::OnBrowseRegisterFile)
    EVT_LISTBOX(  XRCID("lstTargets"),            DebuggerOptionsProjectDlg::OnTargetSel)
END_EVENT_TABLE()

DebuggerOptionsProjectDlg::DebuggerOptionsProjectDlg(wxWindow* parent, DebuggerGDB* debugger, cbProject* project)
    : m_pDBG(debugger),
    m_pProject(project),
    m_LastTargetSel(-1)
{
    if (!wxXmlResource::Get()->LoadPanel(this, parent, _T("pnlDebuggerProjectOptions")))
        return;

    m_OldPaths = m_pDBG->GetSearchDirs(project);
    m_CurrentRemoteDebugging = m_pDBG->GetRemoteDebuggingMap(project);

    wxListBox* control = XRCCTRL(*this, "lstTargets", wxListBox);
    control->Clear();
    for (int i = 0; i < project->GetBuildTargetsCount(); ++i)
    {
        control->Append(project->GetBuildTarget(i)->GetTitle());
    }
    control->SetSelection(0);

    AddPluginDebuggerPanels();

    LoadCurrentRemoteDebuggingRecord();
    Manager::Get()->RegisterEventSink(cbEVT_BUILDTARGET_REMOVED, new cbEventFunctor<DebuggerOptionsProjectDlg, CodeBlocksEvent>(this, &DebuggerOptionsProjectDlg::OnBuildTargetRemoved));
    Manager::Get()->RegisterEventSink(cbEVT_BUILDTARGET_ADDED, new cbEventFunctor<DebuggerOptionsProjectDlg, CodeBlocksEvent>(this, &DebuggerOptionsProjectDlg::OnBuildTargetAdded));
    Manager::Get()->RegisterEventSink(cbEVT_BUILDTARGET_RENAMED, new cbEventFunctor<DebuggerOptionsProjectDlg, CodeBlocksEvent>(this, &DebuggerOptionsProjectDlg::OnBuildTargetRenamed));
}

DebuggerOptionsProjectDlg::~DebuggerOptionsProjectDlg()
{
    Manager::Get()->RemoveAllEventSinksFor(this);
}

void DebuggerOptionsProjectDlg::AddPluginDebuggerPanels() {
  //
  // Add other debugger plugin panels to notebook
  //
  wxNotebook* nb = XRCCTRL(*this, "nbDebugger", wxNotebook);

  Manager::Get()->GetPluginManager()->GetProjectConfigurationDebuggerPanels(nb, m_pProject, m_DebuggerPanels);

  for (size_t i = 0; i < m_DebuggerPanels.GetCount(); ++i)
  {
      cbDebuggerConfigurationPanel* panel = m_DebuggerPanels[i];
      panel->SetParentDialog(this);
      nb->AddPage(panel, panel->GetTitle());
  }
}

void DebuggerOptionsProjectDlg::OnBuildTargetRemoved(CodeBlocksEvent& event)
{
    cbProject* project = event.GetProject();
    if(project != m_pProject)
    {
        return;
    }
    wxString theTarget = event.GetBuildTargetName();
    for (RemoteDebuggingMap::iterator it = m_CurrentRemoteDebugging.begin(); it != m_CurrentRemoteDebugging.end(); ++it)
    {
        // find our target
        if ( !it->first || it->first->GetTitle() != theTarget)
            continue;

        m_CurrentRemoteDebugging.erase(it);
        // if we erased it, just break, there can only be one map per target
        break;
    }
    wxListBox* lstBox = XRCCTRL(*this, "lstTargets", wxListBox);
    int idx = lstBox->FindString(theTarget);
    if (idx > 0)
    {
        lstBox->Delete(idx);
    }
    if((size_t)idx >= lstBox->GetCount())
    {
        idx--;
    }
    lstBox->SetSelection(idx);
    LoadCurrentRemoteDebuggingRecord();
}

void DebuggerOptionsProjectDlg::OnBuildTargetAdded(CodeBlocksEvent& event)
{
    cbProject* project = event.GetProject();
    if(project != m_pProject)
    {
        return;
    }
    wxString newTarget = event.GetBuildTargetName();
    wxString oldTarget = event.GetOldBuildTargetName();
    if(!oldTarget.IsEmpty())
    {
        for (RemoteDebuggingMap::iterator it = m_CurrentRemoteDebugging.begin(); it != m_CurrentRemoteDebugging.end(); ++it)
        {
            // find our target
            if ( !it->first || it->first->GetTitle() != oldTarget)
                continue;
            ProjectBuildTarget* bt = m_pProject->GetBuildTarget(newTarget);
            if(bt)
                m_CurrentRemoteDebugging.insert(m_CurrentRemoteDebugging.end(), std::make_pair(bt, it->second));
            // if we inserted it, just break, there can only be one map per target
            break;
        }
    }
    wxListBox* lstBox = XRCCTRL(*this, "lstTargets", wxListBox);
    int idx = lstBox->FindString(newTarget);
    if (idx == wxNOT_FOUND)
    {
        idx = lstBox->Append(newTarget);
    }
    lstBox->SetSelection(idx);
    LoadCurrentRemoteDebuggingRecord();
}

void DebuggerOptionsProjectDlg::OnBuildTargetRenamed(CodeBlocksEvent& event)
{
    cbProject* project = event.GetProject();
    if(project != m_pProject)
    {
        return;
    }
    wxString newTarget = event.GetBuildTargetName();
    wxString oldTarget = event.GetOldBuildTargetName();
    for (RemoteDebuggingMap::iterator it = m_CurrentRemoteDebugging.begin(); it != m_CurrentRemoteDebugging.end(); ++it)
    {
        // find our target
        if ( !it->first || it->first->GetTitle() != oldTarget)
            continue;
        it->first->SetTitle(newTarget);
        // if we renamed it, just break, there can only be one map per target
        break;
    }

    wxListBox* lstBox = XRCCTRL(*this, "lstTargets", wxListBox);
    int idx = lstBox->FindString(oldTarget);
    if (idx == wxNOT_FOUND)
    {
        return;
    }
    lstBox->SetString(idx, newTarget);
    lstBox->SetSelection(idx);
    LoadCurrentRemoteDebuggingRecord();
}

void DebuggerOptionsProjectDlg::LoadCurrentRemoteDebuggingRecord()
{
    ProjectBuildTarget* bt;

    m_LastTargetSel = XRCCTRL(*this, "lstTargets", wxListBox)->GetSelection();
    bt = m_pProject->GetBuildTarget(m_LastTargetSel);

    if (m_CurrentRemoteDebugging.find(bt) != m_CurrentRemoteDebugging.end())
    {
      //
      // Remote debugging record found
      //
      RemoteDebugging& rd = m_CurrentRemoteDebugging[bt];

      //
      // Server Connection Settings
      //
      int sel = XRCCTRL(*this, "cmbRemoteServers", wxChoice)->FindString(rd.RemoteServer);
      if (sel != wxNOT_FOUND) {
        XRCCTRL(*this, "cmbRemoteServers", wxChoice)->SetSelection(sel);
      } else {
        XRCCTRL(*this, "cmbRemoteServers", wxChoice)->SetSelection(-1);
      }
      XRCCTRL(*this, "txtIP",                 wxTextCtrl)->SetValue(rd.ip);
      XRCCTRL(*this, "txtPort",               wxTextCtrl)->SetValue(rd.ipPort);
      XRCCTRL(*this, "txtTargetDevice",       wxTextCtrl)->SetValue(rd.TargetDevice);
      //
      // GDB Settings
      //
      XRCCTRL(*this, "txtRegisterFile",       wxTextCtrl)->SetValue(rd.RegisterFile);
      XRCCTRL(*this, "chkBreakAtMain",        wxCheckBox)->SetValue(rd.breakAtMain);
      XRCCTRL(*this, "txtBreakAtMainSymbol",  wxTextCtrl)->SetValue(rd.breakAtMainSymbol);
      XRCCTRL(*this, "txtCmds",               wxTextCtrl)->SetValue(rd.additionalCmds);
      XRCCTRL(*this, "txtCmdsBefore",         wxTextCtrl)->SetValue(rd.additionalCmdsBefore);

    }
    else
    {
      //
      // No debugging record found, set standard values
      //
      //
      // Server Connection Settings
      //
#if 0
      int sel = XRCCTRL(*this, "cmbRemoteServers", wxChoice)->FindString(_("J-Link GDB Server"));
      if (sel != wxNOT_FOUND) {
        XRCCTRL(*this, "cmbRemoteServers", wxChoice)->SetSelection(sel);
      } else {
        XRCCTRL(*this, "cmbRemoteServers", wxChoice)->SetSelection(-1);
      }
#else
      XRCCTRL(*this, "cmbRemoteServers", wxChoice)->SetSelection(0);
#endif
      XRCCTRL(*this, "txtIP",                 wxTextCtrl)->SetValue(_("localhost"));
      XRCCTRL(*this, "txtPort",               wxTextCtrl)->SetValue(_("2331"));
      XRCCTRL(*this, "txtTargetDevice",       wxTextCtrl)->SetValue(wxEmptyString);
      //
      // GDB Settings
      //
      XRCCTRL(*this, "txtRegisterFile",       wxTextCtrl)->SetValue(wxEmptyString);
      XRCCTRL(*this, "chkBreakAtMain",        wxCheckBox)->SetValue(true);
      XRCCTRL(*this, "txtBreakAtMainSymbol",  wxTextCtrl)->SetValue(_("main"));
      XRCCTRL(*this, "txtCmds",               wxTextCtrl)->SetValue(_("monitor reset\nload"));
      XRCCTRL(*this, "txtCmdsBefore",         wxTextCtrl)->SetValue(wxEmptyString);
    }
}

void DebuggerOptionsProjectDlg::SaveCurrentRemoteDebuggingRecord()
{
    ProjectBuildTarget* bt = m_pProject->GetBuildTarget(m_LastTargetSel);

    RemoteDebuggingMap::iterator it = m_CurrentRemoteDebugging.find(bt);
    if (it == m_CurrentRemoteDebugging.end())
        it = m_CurrentRemoteDebugging.insert(m_CurrentRemoteDebugging.end(), std::make_pair(bt, RemoteDebugging()));

    RemoteDebugging& rd = it->second;
    //
    // Set fixed settings
    //
    rd.connType                   = (RemoteDebugging::ConnectionType)0;
    rd.serialPort                 = wxEmptyString;
    rd.serialBaud                 = wxT("115200");
    rd.skipLDpath                 = false;
    rd.additionalShellCmdsAfter   = wxEmptyString;
    rd.additionalShellCmdsBefore  = wxEmptyString;
    //
    // Server Connection Settings
    //
    rd.ip =                         XRCCTRL(*this, "txtIP",                 wxTextCtrl)->GetValue();
    rd.ipPort =                     XRCCTRL(*this, "txtPort",               wxTextCtrl)->GetValue();
    rd.TargetDevice =               XRCCTRL(*this, "txtTargetDevice",       wxTextCtrl)->GetValue();
    rd.RemoteServer =               XRCCTRL(*this, "cmbRemoteServers",      wxChoice  )->GetStringSelection();
    //
    // Get GDB Settings
    //
    rd.RegisterFile =               XRCCTRL(*this, "txtRegisterFile",       wxTextCtrl)->GetValue();
    rd.breakAtMain =                XRCCTRL(*this, "chkBreakAtMain",        wxCheckBox)->GetValue();
    rd.breakAtMainSymbol =          XRCCTRL(*this, "txtBreakAtMainSymbol",  wxTextCtrl)->GetValue();
    rd.additionalCmds =             XRCCTRL(*this, "txtCmds",               wxTextCtrl)->GetValue();
    rd.additionalCmdsBefore =       XRCCTRL(*this, "txtCmdsBefore",         wxTextCtrl)->GetValue();
}

RemoteDebugging& DebuggerOptionsProjectDlg::GetRemoteDebuggingRecord(int Target) {
  ProjectBuildTarget* bt = m_pProject->GetBuildTarget(Target);

  RemoteDebuggingMap::iterator it = m_CurrentRemoteDebugging.find(bt);
  if (it == m_CurrentRemoteDebugging.end()) {
    //return NULL;
  }
  RemoteDebugging& rd = it->second;
  return rd;
}

void DebuggerOptionsProjectDlg::OnTargetSel(wxCommandEvent& WXUNUSED(event))
{
    //
    // Get current remote server identifier
    //
    wxString RemoteServer = XRCCTRL(*this, "cmbRemoteServers",      wxChoice  )->GetStringSelection();
    // update remote debugging controls
    SaveCurrentRemoteDebuggingRecord();
    LoadCurrentRemoteDebuggingRecord();
    //
    // Inform all registered debugger panels about the selection
    //
    for (size_t i = 0; i < m_DebuggerPanels.GetCount(); ++i) {
      cbDebuggerConfigurationPanel* panel = m_DebuggerPanels[i];
      panel->OnTargetSel(RemoteServer);
    }
}

void DebuggerOptionsProjectDlg::OnAdd(wxCommandEvent& WXUNUSED(event))
{
    //wxListBox* control = XRCCTRL(*this, "lstSearchDirs", wxListBox);

    //EditPathDlg dlg(this,
    //        m_pProject ? m_pProject->GetBasePath() : _T(""),
    //        m_pProject ? m_pProject->GetBasePath() : _T(""),
    //        _("Add directory"));

    //PlaceWindow(&dlg);
    //if (dlg.ShowModal() == wxID_OK)
    //{
    //    wxString path = dlg.GetPath();
    //    control->Append(path);
    //}
}

void DebuggerOptionsProjectDlg::OnEdit(wxCommandEvent& WXUNUSED(event))
{
    //wxListBox* control = XRCCTRL(*this, "lstSearchDirs", wxListBox);
    //int sel = control->GetSelection();
    //if (sel < 0)
    //    return;

    //EditPathDlg dlg(this,
    //        control->GetString(sel),
    //        m_pProject ? m_pProject->GetBasePath() : _T(""),
    //        _("Edit directory"));

    //PlaceWindow(&dlg);
    //if (dlg.ShowModal() == wxID_OK)
    //{
    //    wxString path = dlg.GetPath();
    //    control->SetString(sel, path);
    //}
}

void DebuggerOptionsProjectDlg::OnDelete(wxCommandEvent& WXUNUSED(event))
{
    //wxListBox* control = XRCCTRL(*this, "lstSearchDirs", wxListBox);
    //int sel = control->GetSelection();
//    if (sel < 0)
//        return;
//
//    control->Delete(sel);
}
/* emIDE change [JL] - MMR Window register definition file*/
void DebuggerOptionsProjectDlg::OnBrowseRegisterFile(cb_unused wxCommandEvent& event) {
  wxFileName fname;
  fname.Assign(XRCCTRL(*this, "txtRegisterFile", wxTextCtrl)->GetValue());
  fname.Normalize(wxPATH_NORM_ALL & ~wxPATH_NORM_CASE);
  wxFileDialog dlg(this, _("Select register description file"), fname.GetPath(), fname.GetFullName(), _("CMSIS-SVD files (*.xml;*.svd)|*.xml;*.svd|IAR ddf files (*.ddf)|*.ddf|All files (*.*)|*.*"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);/**/
  PlaceWindow(&dlg);
  if (dlg.ShowModal() != wxID_OK)
      return;
  fname.Assign(dlg.GetPath());
  //fname.MakeRelativeTo(Manager::Get()->GetProjectManager()->GetActiveProject()->GetBasePath());
  XRCCTRL(*this, "txtRegisterFile", wxTextCtrl)->SetValue(fname.GetFullPath());
}

void DebuggerOptionsProjectDlg::OnUpdateUI(wxUpdateUIEvent& WXUNUSED(event)) {
  bool bEnabled;

  bEnabled = XRCCTRL(*this, "lstTargets", wxListBox)->GetSelection() != wxNOT_FOUND;
  //
  // Server Connection Settings
  //
  XRCCTRL(*this, "cmbRemoteServers",      wxChoice        )->Enable(bEnabled);
  XRCCTRL(*this, "txtIP",                 wxTextCtrl      )->Enable(bEnabled);
  XRCCTRL(*this, "txtPort",               wxTextCtrl      )->Enable(bEnabled);
  //
  // Set up GDB Settings
  //
  XRCCTRL(*this, "txtRegisterFile",       wxTextCtrl      )->Enable(bEnabled);
  XRCCTRL(*this, "chkBreakAtMain",        wxCheckBox      )->Enable(bEnabled);
  XRCCTRL(*this, "txtBreakAtMainSymbol",  wxTextCtrl      )->Enable(bEnabled);
  XRCCTRL(*this, "txtCmds",               wxTextCtrl      )->Enable(bEnabled);
  XRCCTRL(*this, "txtCmdsBefore",         wxTextCtrl      )->Enable(bEnabled);

}

void DebuggerOptionsProjectDlg::OnApply()
{
    m_OldPaths.Clear();

    SaveCurrentRemoteDebuggingRecord();

    m_pDBG->GetSearchDirs(m_pProject) = m_OldPaths;
    m_pDBG->GetRemoteDebuggingMap(m_pProject) = m_CurrentRemoteDebugging;
    //
    // Inform all registered debugger panels about the apply
    //
    for (size_t i = 0; i < m_DebuggerPanels.GetCount(); ++i) {
      cbDebuggerConfigurationPanel* panel = m_DebuggerPanels[i];
      panel->OnApply();
    }
}

void DebuggerOptionsProjectDlg::OnCancel() {
  //
  // Inform all registered debugger panels about the cancel
  //
  for (size_t i = 0; i < m_DebuggerPanels.GetCount(); ++i) {
    cbDebuggerConfigurationPanel* panel = m_DebuggerPanels[i];
    panel->OnCancel();
  }
}
