/********************************************************************
**  This file is part of emIDE by [JL]
**
**  File: compilerKPITRX.h
**
********************************************************************/

#ifndef COMPILER_KPITRX_H
#define COMPILER_KPITRX_H

#include "compiler.h"

class CompilerKPITRX : public Compiler
{
    public:
        CompilerKPITRX();
        virtual ~CompilerKPITRX();
        virtual void Reset();
        virtual void LoadDefaultRegExArray();
        virtual AutoDetectResult AutoDetectInstallationDir();
    protected:
        virtual Compiler* CreateCopy();
    private:
};

#endif // COMPILER_KPITRX_H
