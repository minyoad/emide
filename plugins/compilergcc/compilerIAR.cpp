/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 *
 * $Revision: 9263 $
 * $Id: compilerIAR.cpp 9263 2013-08-17 09:20:28Z mortenmacfly $
 * $HeadURL: http://svn.code.sf.net/p/codeblocks/code/branches/release-xx.yy/src/plugins/compilergcc/compilerIAR.cpp $
 */

#include <sdk.h>
#include <prep.h>
#include "compilerIAR.h"
#include <wx/dir.h>
#include <wx/intl.h>
#include <wx/regex.h>
#include <wx/config.h>
#include <wx/fileconf.h>
#include <wx/msgdlg.h>

#ifdef __WXMSW__
    #include <wx/msw/registry.h>
#endif

CompilerIAR::CompilerIAR(wxString arch)
    : Compiler(_("IAR ") + arch + _(" Compiler"), _T("iar") + arch)
{
    m_Arch = arch;
    Reset();
}

CompilerIAR::~CompilerIAR()
{
    //dtor
}

Compiler * CompilerIAR::CreateCopy()
{
    Compiler* c = new CompilerIAR(*this);
    c->SetExtraPaths(m_ExtraPaths); // wxArrayString doesn't seem to be copied with the default copy ctor...
    return c;
}

void CompilerIAR::Reset()
{
    if (platform::windows) {
        m_Programs.C          = _T("iccarm.exe");
        m_Programs.CPP        = _T("iccarm.exe");
        m_Programs.LD         = _T("ilinkarm.exe");
        m_Programs.DBG        = _T("");
        m_Programs.DBGconfig  = _T("gdb_debugger:ARM");
        m_Programs.LIB        = _T("iarchive.exe");
        m_Programs.WINDRES    = _T("");
        m_Programs.MAKE       = _T("IarBuild.exe");
        m_Programs.OBJCOPY    = _T("");
    } else {
        m_Programs.C          = _T("iccarm");
        m_Programs.CPP        = _T("iccarm");
        m_Programs.LD         = _T("ilinkarm");
        m_Programs.DBG        = _T("");
        m_Programs.DBGconfig  = _T("gdb_debugger:ARM");
        m_Programs.LIB        = _T("iarchive");
        m_Programs.WINDRES    = _T("");
        m_Programs.MAKE       = _T("IarBuild");
        m_Programs.OBJCOPY    = _T("");
    }
    m_Switches.includeDirs              = _T("-I");
    m_Switches.libDirs                  = _T("-I");
    m_Switches.linkLibs                 = _T("");
    m_Switches.defines                  = _T("-D");
    m_Switches.genericSwitch            = _T("-");
    m_Switches.objectExtension          = _T("o");
    m_Switches.needDependencies         = true;
    m_Switches.forceCompilerUseQuotes   = false;
    m_Switches.forceLinkerUseQuotes     = false;
    m_Switches.logging                  = CompilerSwitches::defaultLogging;
    m_Switches.libPrefix                = _T("lib");
    m_Switches.libExtension             = _T("a");
    m_Switches.linkerNeedsLibPrefix     = true;
    m_Switches.linkerNeedsLibExtension  = true;
    m_Switches.UseFullSourcePaths       = true;     // use the GDB workaround !!!!!!!!

    // Summary of GCC options: http://gcc.gnu.org/onlinedocs/gcc/Option-Summary.html

    m_Options.ClearOptions();
wxString category;

m_Options.AddOption(_("Produce debugging symbols"),
                    _("--debug"),
                    _("Debugging"),
                    _(""),
                    true,
                    _("-Om -Oh -Ohs -Ohz"),
                    _("You have optimizations enabled. This is Not A Good Thing(tm) when producing debugging symbols..."));

m_Options.AddOption(_("aeabi compliant code"),
                    _("--aeabi"));
m_Options.AddOption(_("Enable Remarks"),
                    _("--remarks"));
m_Options.AddOption(_("Disable Warnings"),
                    _("--no-warnings"));
m_Options.AddOption(_("Require proper function prototypes"),
                    _("--require_prototypes"));

m_Options.AddOption(_("Enforce strict language rules compliance"),
                    _("--strict"),
                    _("General"),
                    _(""),
                    true,
                    _("-e"),
                    _("--strict and -e are mutually exclusive"));

m_Options.AddOption(_("Treat Warnings as Errors"),
                    _("--warnings_are_errors"));
m_Options.AddOption(_("Discard unused public functions and variables"),
                    _("--discard_unused_publics"));
m_Options.AddOption(_("Insert debug info in object file"),
                    _("-r"));
m_Options.AddOption(_("All enum is 4 bytes"),
                    _("--enum_is_int"));
m_Options.AddOption(_("Generate interworking code"),
                    _("--interwork"));
m_Options.AddOption(_("Legacy RVCT3.0"),
                    _("--legacy RVCT3.0"));
m_Options.AddOption(_("Separate initialized and uninitialized variables,when using variable clustering"),
                    _("--separate_cluster_for_initialized_variables"));
m_Options.AddOption(_("Place generated generic code in separate section"),
                    _("--shared_helper_section"));

category = _("Diagnostic");
m_Options.AddOption(_("Adds include file context to diagnostics"),
                    _("--header_context"), category);
m_Options.AddOption(_("Use positions inside macros in diagnostics"),
                    _("--macro_positions_in_diagnostics"), category);

category = _("System Library");
m_Options.AddOption(_("Use DLIB without configuration"),
                    _("--dlib --dlib_config none"), category);
m_Options.AddOption(_("Use DLIB tiny configuration"),
                    _("--dlib --dlib_config tiny"), category);
m_Options.AddOption(_("Use DLIB normal configuration"),
                    _("--dlib --dlib_config normal"), category);
m_Options.AddOption(_("Use DLIB full configuration"),
                    _("--dlib --dlib_config full"), category);

category = _("Code");
m_Options.AddOption(_("Generate read-only position independent code"),
                    _("--ropi"), category);
m_Options.AddOption(_("Generate read-write position independent code"),
                    _("--rwpi"), category);

category = _("Optimization");
m_Options.AddOption(_("Optimization Level None (best debug support)"),
                    _("-On"), category);
m_Options.AddOption(_("Optimization Level Low"),
                    _("-Ol"), category);
m_Options.AddOption(_("Optimization Level Medium"),
                    _("-Om"), category);
m_Options.AddOption(_("Optimization Level High, Balanced"),
                    _("-Oh"), category);
m_Options.AddOption(_("Optimization Level High, favoring Speed"),
                    _("-Ohs"), category);
m_Options.AddOption(_("Optimization Level High, favoring Size"),
                    _("-Ohz"), category);


category = _("Optimization");
m_Options.AddOption(_("Disable Code Motion"),
                    _("--no_code_motion"), category);
m_Options.AddOption(_("Disable Common Subexpression Elimination"),
                    _("--no_cse"), category);
m_Options.AddOption(_("Disable Function Inlining"),
                    _("--no_inline"), category);
m_Options.AddOption(_("Do not include file path in __FILE__ and __BASE_FILE__ macros"),
                    _("--no_path_in_file_macros"), category);
m_Options.AddOption(_("Do not emit destructors for C++ static variables"),
                    _("--no_static_destruction"), category);
m_Options.AddOption(_("Disable automatic search for system include files"),
                    _("--no_system_include"), category);
m_Options.AddOption(_("Disable Type-Based Alias Analysis"),
                    _("--no_tbaa"), category);
m_Options.AddOption(_("Do not use typedefs in diagnostic messages"),
                    _("--no_typedefs_in_diagnostics"), category);
m_Options.AddOption(_("Disable Loop Unrolling"),
                    _("--no_unroll"), category);
m_Options.AddOption(_("Disable alignment reduction of simple thumb functions"),
                    _("--no_alignment_reduction"), category);
m_Options.AddOption(_("Disable static clustering for static and global variables"),
                    _("--no_clustering"), category);
m_Options.AddOption(_("Turn off the alignment optimization for constants"),
                    _("--no_const_align"), category);
m_Options.AddOption(_("Suppress Dwarf 3 Call Frame Information instructions"),
                    _("--no_dwarf3_cfi"), category);
m_Options.AddOption(_("Disable C++ exception support"),
                    _("--no_exceptions"), category);
m_Options.AddOption(_("Do not generate section fragments"),
                    _("--no_fragments"), category);
m_Options.AddOption(_("Generate code that does not issue read request to .text"),
                    _("--no_literal_pool"), category);
m_Options.AddOption(_("Disable alignment of labels in loops (Thumb2)"),
                    _("--no_loop_align"), category);
m_Options.AddOption(_("Disable idiom recognition for memcpy/memset/memclr"),
                    _("--no_mem_idioms"), category);
m_Options.AddOption(_("Disable C++ runtime type information support"),
                    _("--no_rtti"), category);
m_Options.AddOption(_("Don't allow C-object to be initialized at runtime"),
                    _("--no_rw_dynamic_init"), category);
m_Options.AddOption(_("Disable instruction scheduling"),
                    _("--no_scheduling"), category);
m_Options.AddOption(_("Remove limits for code expansion"),
                    _("--no_size_constraints"), category);
m_Options.AddOption(_("Don't generate unaligned accesses"),
                    _("--no_unaligned_access"), category);
m_Options.AddOption(_("Use stdout only (no console output on stderr)"),
                    _("--only_stdout"), category);

category = _("C Dialect");
m_Options.AddOption(_("C89 Dialect (default is C99)"),
                    _("--c89"),
                    category,
                    _(""),
                    true,
                    _("--vla"),
                    _("C89 and Variable Length Arrays are incompatible"));

m_Options.AddOption(_("C++"),
                    _("--c++"), category);
m_Options.AddOption(_("Embedded C++"),
                    _("--ec++"), category);
m_Options.AddOption(_("Extended Embedded C++"),
                    _("--eec++"), category);


category = _("C Dialect: Char signedness");
m_Options.AddOption(_("Char is Signed"),
                    _("--char_is_signed"), category);
m_Options.AddOption(_("Char is Unsigned"),
                    _("--char_is_unsigned"), category);


category = _("C Dialect");
m_Options.AddOption(_("Enable Multibyte characters in C or C++ source code"),
                    _("--enable_multibytes"), category);
m_Options.AddOption(_("Use Guards for function static variable initialization"),
                    _("--guard_calls"), category);
m_Options.AddOption(_("Enable C++ inline semantics even in C mode"),
                    _("--use_c++_inline"), category);
m_Options.AddOption(_("Enable C99 Variable Length Arrays"),
                    _("--vla"),
                    category,
                    _(""),
                    true,
                    _("--c89"),
                    _("C89 and Variable Length Arrays are incompatible"));
m_Options.AddOption(_("Enable Language Extensions"),
                    _("-e"),
                    category,
                    _(""),
                    true,
                    _("--strict"),
                    _("--strict and -e are mutually exclusive"));


category = _("Calling convention");
m_Options.AddOption(_("FPA (Standard)"),
                    _("--aapcs std"), category);
m_Options.AddOption(_("VFP (scalar mode)"),
                    _("--aapcs vfp"), category);


category = _("CPU Core");
m_Options.AddOption(_("Cortex-A15"),
                    _("--cpu=Cortex-A15"), category);
m_Options.AddOption(_("Cortex-A9"),
                    _("--cpu=Cortex-A9"), category);
m_Options.AddOption(_("Cortex-A8"),
                    _("--cpu=Cortex-A8"), category);
m_Options.AddOption(_("Cortex-A7"),
                    _("--cpu=Cortex-A7"), category);
m_Options.AddOption(_("Cortex-A5"),
                    _("--cpu=Cortex-A5"), category);
m_Options.AddOption(_("Cortex-R7"),
                    _("--cpu=Cortex-R7"), category);
m_Options.AddOption(_("Cortex-R5"),
                    _("--cpu=Cortex-R5"), category);
m_Options.AddOption(_("Cortex-R4"),
                    _("--cpu=Cortex-R4"), category);
m_Options.AddOption(_("Cortex-M4"),
                    _("--cpu=Cortex-M4"), category);
m_Options.AddOption(_("Cortex-M3"),
                    _("--cpu=Cortex-M3"), category);
m_Options.AddOption(_("Cortex-M1"),
                    _("--cpu=Cortex-M1"), category);
m_Options.AddOption(_("Cortex-M0+"),
                    _("--cpu=Cortex-M0+"), category);
m_Options.AddOption(_("Cortex-M0"),
                    _("--cpu=Cortex-M0"), category);
m_Options.AddOption(_("ARM946E-S"),
                    _("--cpu=ARM946E-S"), category);
m_Options.AddOption(_("ARM966E-S"),
                    _("--cpu=ARM966E-S"), category);
m_Options.AddOption(_("ARM968E-S"),
                    _("--cpu=ARM968E-S"), category);
m_Options.AddOption(_("ARM926EJ-S"),
                    _("--cpu=ARM926EJ-S"), category);
m_Options.AddOption(_("ARM940T"),
                    _("--cpu=ARM940T"), category);
m_Options.AddOption(_("ARM920T"),
                    _("--cpu=ARM920T"), category);
m_Options.AddOption(_("ARM922T"),
                    _("--cpu=ARM922T"), category);
m_Options.AddOption(_("ARM922T"),
                    _("--cpu=ARM922T"), category);
m_Options.AddOption(_("ARM9TDMI"),
                    _("--cpu=ARM9TDMI"), category);
m_Options.AddOption(_("ARM720T"),
                    _("--cpu=ARM720T"), category);
m_Options.AddOption(_("ARM7TDMI"),
                    _("--cpu=ARM7TDMI"), category);
m_Options.AddOption(_("ARM7TDMI-S"),
                    _("--cpu=ARM7TDMI-S"), category);
m_Options.AddOption(_("ARM9E"),
                    _("--cpu=ARM9E"), category);
m_Options.AddOption(_("ARM7EJ-S"),
                    _("--cpu=ARM7EJ-S"), category);
m_Options.AddOption(_("XScale"),
                    _("--cpu=XScale"), category);

category = _("FPU type");
m_Options.AddOption(_("Software floating point"),
                    _("--fpu none"), category);
m_Options.AddOption(_("VFPv2"),
                    _("--fpu VFPv2"), category);
m_Options.AddOption(_("VFPv3"),
                    _("--fpu VFPv3"), category);
m_Options.AddOption(_("VFPv3_D16"),
                    _("--fpu VFPv3_D16"), category);
m_Options.AddOption(_("VFPv3_D16_FP16"),
                    _("--fpu VFPv3_D16_FP16"), category);
m_Options.AddOption(_("VFPv4"),
                    _("--fpu VFPv4"), category);
m_Options.AddOption(_("VFPv4_sp"),
                    _("--fpu VFPv4_sp"), category);
m_Options.AddOption(_("VFP9-S"),
                    _("--fpu VFP9-S"), category);


category = _("Byte order");
m_Options.AddOption(_("Little endian"),
                    _("--endian little"), category);
m_Options.AddOption(_("Big endian"),
                    _("--endian big"), category);

category = _("CPU Mode");
m_Options.AddOption(_("Arm"),
                    _("--cpu_mode=a"), category);
m_Options.AddOption(_("Thumb"),
                    _("--cpu_mode=t"), category);

    m_OptionLinkerScript = _("--config ");

    m_Commands[(int)ctCompileObjectCmd].push_back(CompilerTool(_T("$compiler $options $includes $file -o $object")));
    m_Commands[(int)ctGenDependenciesCmd].push_back(CompilerTool(_T("$compiler $options $includes $file -o $object --dependencies=m $dep_object")));
    m_Commands[(int)ctCompileResourceCmd].push_back(CompilerTool(_T("$rescomp -i $file -J rc -o $resource_output -O coff $res_includes")));
    m_Commands[(int)ctLinkConsoleExeCmd].push_back(CompilerTool(_T("$linker $libdirs -o $exe_output -f $link_objects $link_resobjects $link_options $libs")));
    m_Commands[(int)ctLinkExeCmd].push_back(CompilerTool(_T("$linker $libdirs $link_options $libs -f $link_objects -o $exe_output")));
    m_Commands[(int)ctLinkDynamicCmd].push_back(CompilerTool(_T("")));

    m_Commands[(int)ctLinkStaticCmd].push_back(CompilerTool(_T("$lib_linker $static_output -f $link_objects")));
    m_Commands[(int)ctLinkNativeCmd] = m_Commands[(int)ctLinkConsoleExeCmd]; // unsupported currently

    LoadDefaultRegExArray();

    m_CompilerOptions.Clear();
    m_LinkerOptions.Clear();
    m_LinkLibs.Clear();
    m_CmdsBefore.Clear();
    m_CmdsAfter.Clear();
} // end of Reset

void CompilerIAR::LoadDefaultRegExArray()
{
    m_RegExes.Clear();
    m_RegExes.Add(RegExStruct(_("Fatal error"), cltError, _T("FATAL:[ \t]*(.*)"), 1));

//
//  "C:\temp\embOS_CortexM_IAR_V6_Obj_V388c\Start\BoardSupport\ST\STM32F40G_Eval\Application\Start_LEDBlink.c",52  Error[Pe065]: expected a ";"
//  "C:\temp\embOS_CortexM_IAR_V6_Obj_V388c\Start\BoardSupport\ST\STM32F40G_Eval\Application\Start_LEDBlink.c",38  Warning[Pe1105]: #warning directive: This is a Warning
//  "C:\temp\embOS_CortexM_IAR_V6_Obj_V388c\Start\BoardSupport\ST\STM32F40G_Eval\Application\Start_LEDBlink.c",39  Fatal error[Pe035]: #error directive: This is an Error
//
//  Fatal error detected, aborting.
//  Errors: 2
//  Warnings: 1
//
    m_RegExes.Add(RegExStruct(_("Compiler warning"),      cltWarning, _("\"([][{} \\(\\)\t\\.#%$~_/:&[:alnum:]\\-]+)\",([0-9]+)[ \\t]+([Ww]arning\\[[[:alnum:]]+\\]:.*)"), 3, 1, 2 ));
    m_RegExes.Add(RegExStruct(_("Compiler error"),        cltError,   _("\"([][{} \\(\\)\t\\.#%$~_/:&[:alnum:]\\-]+)\",([0-9]+)[ \\t]+([Ee]rror\\[[[:alnum:]]+\\]:.*)"), 3, 1, 2 ));
    m_RegExes.Add(RegExStruct(_("Compiler remark"),       cltWarning, _("\"([][{} \\(\\)\t\\.#%$~_/:&[:alnum:]\\-]+)\",([0-9]+)[ \\t]+([Rr]emark\\[[[:alnum:]]+\\]:.*)"), 3, 1, 2 ));
    m_RegExes.Add(RegExStruct(_("Compiler fatal error"),  cltError,   _("\"([][{} \\(\\)\t\\.#%$~_/:&[:alnum:]\\-]+)\",([0-9]+)[ \\t]+([Ff]atal [Ee]rror\\[[[:alnum:]]+\\]:.*)"), 3, 1, 2 ));

    m_RegExes.Add(RegExStruct(_("Compiler fatal error"),  cltInfo,    _("([Ff]atal error detected, aborting.)"), 1));
    m_RegExes.Add(RegExStruct(_("Compiler error"),        cltInfo,    _("([Ee]rror[s]*:[ \t][1-9][0-9]*)"), 1));
    m_RegExes.Add(RegExStruct(_("Compiler warning"),      cltInfo,    _("([Ww]arning[s]*:[ \t][1-9][0-9]*)"), 1));

    m_RegExes.Add(RegExStruct(_("Command line error"),    cltError,   _("(Command line error: Unexpected command line argument[s*])"), 1));

    m_RegExes.Add(RegExStruct(_("Internal error"),        cltError,   _T("[Ii]nternal [Ee]rror:[ \t]*(.*)"),1));
    m_RegExes.Add(RegExStruct(_("Compiler remark"),       cltWarning, _T("([][{}() \t#%$~[:alnum:]&_:+/\\.-]+),([0-9]+)[ \t]+([Rr]emark\\[[0-9A-Za-z]*\\]:.*)"), 3, 1, 2));
    m_RegExes.Add(RegExStruct(_("Compiler warning"),      cltWarning, _T("([][{}() \t#%$~[:alnum:]&_:+/\\.-]+),([0-9]+)[ \t]+([Ww]arning\\[[0-9A-Za-z]*\\]:.*)"), 3, 1, 2));
    m_RegExes.Add(RegExStruct(_("Compiler error"),        cltError,   _T("([][{}() \t#%$~[:alnum:]&_:+/\\.-]+),([0-9]+)[ \t]+([Ee]rror\\[[0-9A-Za-z]*\\]:.*)"), 3, 1, 2));
    m_RegExes.Add(RegExStruct(_("Compiler fatal error"),  cltError,   _T("([][{}() \t#%$~[:alnum:]&_:+/\\.-]+),([0-9]+)[ \t]+([Ff]atal[ \t]+[Ee]rror\\[[0-9A-Za-z]*\\]:.*)"), 3, 1, 2));
    m_RegExes.Add(RegExStruct(_("Linker warning"),        cltWarning, _T("([Ww]arning\\[[0-9A-Za-z]*\\]:.*)"), 1));
    m_RegExes.Add(RegExStruct(_("Linker error"),          cltError,   _T("([Ee]rror\\[[0-9A-Za-z]*\\]:.*)"), 1));
    m_RegExes.Add(RegExStruct(_("Linker fatal"),          cltError,   _T("([Ff]atal[ \t]+[Ee]rror\\[[0-9A-Za-z]*\\]:.*)"), 1));
}

AutoDetectResult CompilerIAR::AutoDetectInstallationDir()
{
    if (platform::windows)
    {
        m_MasterPath.Clear();
        //
        // Try own search at first
        // All versions are listed at
        // HKLM/Software/IAR Systems/Embedded Workbench/$A/Locations/Location$n/
        //   Where $A is 4.0 or 5.0, maybe lower, maybe higher values follow with IAR EW 7
        // Installation Path in key "InstallPath"
        // Product Family gotten from ProductFamilies\$B\ as $B
        //
        // e.g. HKLM/Software/IAR Systems/Embedded Workbench/5.0/Locations/Location8/InstallPath
        // and  HKLM/Software/IAR Systems/Embedded Workbench/5.0/Locations/Location8/Product Families/ARM/
        //
        wxRegKey  KeyPath;
        wxRegKey  KeySub;
        wxRegKey  KeyLocation;
        wxRegKey  KeyLocations;
        wxString  sSubKey;
        wxString  sLocKey;
        wxString  sPath;
        wxArrayString asInstallationPaths;
        long      iSub;
        long      iLocation;

        KeyPath.SetName(_("HKEY_LOCAL_MACHINE\\Software\\IAR Systems\\Embedded Workbench"));
        if (KeyPath.Exists() && KeyPath.Open(wxRegKey::Read)) {
          //
          // Enumerate through sub keys which might be e.g. "4.0", "5.0"
          //
          if (KeyPath.HasSubkeys()) {
            KeyPath.GetFirstKey(sSubKey, iSub);
            do {
              KeySub.SetName(KeyPath, sSubKey);
              Manager::Get()->GetLogManager()->DebugLog(_("KeySub.SetName: ") + KeyPath.GetName() + _(" + ") + sSubKey);
              if (!KeySub.Exists() || !KeySub.Open(wxRegKey::Read)) {
                continue;
              }
              if (KeySub.HasSubKey(_("Locations"))) {
                KeyLocations.SetName(KeySub, _("Locations"));
                if (!KeyLocations.Exists() || !KeyLocations.Open(wxRegKey::Read)) {
                    continue;
                }
                if (KeyLocations.HasSubkeys()) {
                  KeyLocations.GetFirstKey(sLocKey, iLocation);
                  do {
                    KeyLocation.SetName(KeyLocations, sLocKey);
                    Manager::Get()->GetLogManager()->DebugLog(_("KeyLocation.SetName: ") + KeyLocations.GetName() + _(" + ") + sLocKey);
                    if (!KeyLocation.Exists() || !KeyLocation.Open(wxRegKey::Read)) {
                      continue;
                    }
                    if (KeyLocation.HasValue(_("InstallPath"))) {
                      KeyLocation.QueryValue(_("InstallPath"), sPath);
                      if (!sPath.IsEmpty()) {
                        Manager::Get()->GetLogManager()->DebugLog(_("Found IAR Compiler: ") + sPath);
                        if (wxFileExists(sPath + wxFILE_SEP_PATH + _("ARM") + wxFILE_SEP_PATH + wxT("bin") + wxFILE_SEP_PATH + m_Programs.C)) {
                          asInstallationPaths.Add(sPath);
                        }
                      }
                    }
                  } while (KeyLocations.GetNextKey(sLocKey, iLocation));
                }
              }
            } while (KeyPath.GetNextKey(sSubKey, iSub));
          }
        }

        if (asInstallationPaths.GetCount() == 1) {
          m_MasterPath = asInstallationPaths.Item(0) + wxFILE_SEP_PATH + _("ARM");
        } else if (asInstallationPaths.GetCount() > 1) {
          //
          // Show selection dialog to let the user choose which version to use
          //
          wxSingleChoiceDialog dlg(0,
                             _("Select the IAR compiler you want to use for emIDE"),
                             _("IAR compiler selection"),
                             asInstallationPaths);
          dlg.SetSelection(asInstallationPaths.GetCount()-1);
          PlaceWindow(&dlg);
          if (dlg.ShowModal() == wxID_OK) {
            m_MasterPath = asInstallationPaths.Item(dlg.GetSelection()) + wxFILE_SEP_PATH + _("ARM");
          }
        }

#ifdef __WXMSW__ // for wxRegKey
        if (m_MasterPath.IsEmpty())
        {
          wxRegKey key;   // defaults to HKCR
          key.SetName(wxT("HKEY_LOCAL_MACHINE\\Software\\IAR Systems\\Installed Products"));
          if (key.Exists() && key.Open(wxRegKey::Read))
          {
              wxString subkeyname;
              long idx;
              if (key.GetFirstKey(subkeyname, idx))
              {
                  do
                  {
                      wxRegKey keys;
                      keys.SetName(key.GetName() + wxFILE_SEP_PATH + subkeyname);
                      if (!keys.Exists() || !keys.Open(wxRegKey::Read))
                          continue;
                      keys.QueryValue(wxT("TargetDir"), m_MasterPath);
                      if (!m_MasterPath.IsEmpty())
                      {
                          if (wxFileExists(m_MasterPath + wxFILE_SEP_PATH + wxT("bin") + wxFILE_SEP_PATH + m_Programs.C))
                              break;
                          m_MasterPath.Clear();
                      }
                  } while (key.GetNextKey(subkeyname, idx));
              }
          }
        }
#endif // __WXMSW__
        wxString env_path = wxGetenv(_T("ProgramFiles(x86)"));
        if (m_MasterPath.IsEmpty())
        {
            wxDir dir(env_path + wxT("\\IAR Systems"));
            if (wxDirExists(dir.GetName()) && dir.IsOpened())
            {
                wxString filename;
                bool cont = dir.GetFirst(&filename, wxEmptyString, wxDIR_DIRS);
                while (cont)
                {
                    if ( filename.StartsWith(wxT("Embedded Workbench")) )
                    {
                        wxFileName fn(dir.GetName() + wxFILE_SEP_PATH + filename + wxFILE_SEP_PATH +
                                      m_Arch + wxFILE_SEP_PATH + wxT("bin") + wxFILE_SEP_PATH + m_Programs.C);
                        if (   wxFileName::IsFileExecutable(fn.GetFullPath())
                            && (m_MasterPath.IsEmpty() || fn.GetPath() > m_MasterPath) )
                        {
                            m_MasterPath = dir.GetName() + wxFILE_SEP_PATH + filename + wxFILE_SEP_PATH + m_Arch;
                        }
                    }
                    cont = dir.GetNext(&filename);
                }
            }
        }
        if (m_MasterPath.IsEmpty())
        {
            // just a guess; the default installation dir
            m_MasterPath = env_path + wxT("\\IAR Systems\\Embedded Workbench\\" + m_Arch);
        }

        if ( wxDirExists(m_MasterPath) )
        {
            AddIncludeDir(m_MasterPath + wxFILE_SEP_PATH + wxT("include"));
            m_ExtraPaths.Add(m_MasterPath + wxFILE_SEP_PATH + wxT("bin"));
        }
        wxString sMakePath = m_MasterPath;
        sMakePath.RemoveLast(3);
        if (wxDirExists(sMakePath + _("common") + wxFILE_SEP_PATH + _("bin"))) {
          m_ExtraPaths.Add(sMakePath + _("common") + wxFILE_SEP_PATH + _("bin"));
        }
    }
    else
    {
        m_MasterPath=_T("/usr/local"); // default
    }
    if (m_Arch == wxT("8051"))
    {
        AddLinkerOption(wxT("-f \"") + m_MasterPath + wxFILE_SEP_PATH + wxT("config") + wxFILE_SEP_PATH +
                        wxT("devices") + wxFILE_SEP_PATH + wxT("_generic") + wxFILE_SEP_PATH +
                        wxT("lnk51ew_plain.xcl\""));
    }
    else // IAR
    {
        AddCompilerOption(wxT("--no_wrap_diagnostics"));
    }
    return wxFileExists(m_MasterPath + wxFILE_SEP_PATH + wxT("bin") + wxFILE_SEP_PATH + m_Programs.C) ? adrDetected : adrGuessed;
}
